﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content;
using Xamarin.Forms;
using AppUrdaira.Droid.Services;

namespace AppUrdaira.Droid
{
    [Activity(
        Label = "Urdaira", 
        Icon = "@drawable/Logo", 
        Theme = "@style/MainTheme", 
        MainLauncher = true, 
        ConfigurationChanges = 
            ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | 
            ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize,
        ScreenOrientation =ScreenOrientation.Portrait
    )]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        #region Properties
        #region Permission
        const int RequestId = 0;
        private readonly string[] Permission =
        {
            Android.Manifest.Permission.WriteExternalStorage,
            Android.Manifest.Permission.ReadExternalStorage,
            Android.Manifest.Permission.AccessNetworkState,
            Android.Manifest.Permission.Bluetooth,
            Android.Manifest.Permission.BluetoothAdmin,
            Android.Manifest.Permission.AccessFineLocation,
            Android.Manifest.Permission.AccessCoarseLocation,
        };
        #endregion

        public static int BG_TASKS_INTERVAL_SECONDS = 30;
        #endregion
        protected override void OnCreate(Bundle savedInstanceState)
        {
            try
            {
                base.OnCreate(savedInstanceState);

                
                TabLayoutResource = Resource.Layout.Tabbar;
                ToolbarResource = Resource.Layout.Toolbar;

                //Permisos de la app
                RequestPermissions(Permission, RequestId);

                //Xamarin essentials
                Xamarin.Essentials.Platform.Init(this, savedInstanceState);

                //Xamarin forms
                global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

                //cargar app
                LoadApplication(new App());

                //arrancar alarma para proceso en segundo plano
                StartAlarmReceiver();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[MainActivity:OnCreate]Error:" + ex.Message);
            }

        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            try
            {
                Shiny.AndroidShinyHost.OnRequestPermissionsResult(requestCode, permissions, grantResults);

                Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[MainActivity:OnRequestPermissionsResult]Error:" + ex.Message);
            }

        }

        private void StartAlarmReceiver()
        {
            try
            {
                // para dispositivos Oreo o superiores (nivel de api >= 26)
                if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
                {
                    var alarmIntent = new Intent(this, typeof(AlarmReceiver));
                    var pending = PendingIntent.GetBroadcast(this, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);
                    var alarmManager = GetSystemService(AlarmService).JavaCast<AlarmManager>();
                    SetAlarm(pending, alarmManager);
                    // evento para reprogramar alarma
                    MessagingCenter.Subscribe<AlarmReceiver>(this, "FireAlarm", (sender) =>
                    {
                        SetAlarm(pending, alarmManager);
                    });
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[MainActivity:StartAlarmReceiver]Error:" + ex.Message);
            }
        }

        private static void SetAlarm(PendingIntent pending, AlarmManager alarmManager)
        {
            alarmManager.SetAndAllowWhileIdle(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + BG_TASKS_INTERVAL_SECONDS * 1000, pending);
        }

        protected override void OnDestroy()
        {
            try
            {
                base.OnDestroy();

                // Borrar las preferencias al desinstalar la aplicación
                //Xamarin.Essentials.Preferences.Clear();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[MainActivity:OnDestroy]Error:" + ex.Message);
            }    
        }
    }
}