﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppUrdaira.Droid.Services
{
    [BroadcastReceiver]
    public class AlarmReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            try
            {
                var resultIntent = new Intent(context, typeof(MainActivity));
                resultIntent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);

                if (Preferences.Get("SyncingAvailable", true))
                {
                    System.Threading.Tasks.Task.Run(async () =>
                    {
                        try
                        {
                            await AppConfig.Instance.SyncService.SendPendingDeliveryNotes().ConfigureAwait(false);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine($"[AlarmReceiver.OnReceive] Exception syncing: {ex.Message}");
                        }
                    });
                }

                // volver a programar la alarma
                MessagingCenter.Send(this, "FireAlarm");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"[AlarmReceiver.OnReceive] Exception: {ex.Message}");
            }
        }


    }
}