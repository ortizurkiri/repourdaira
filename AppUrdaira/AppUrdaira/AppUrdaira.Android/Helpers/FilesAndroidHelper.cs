﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppUrdaira.Droid.Helpers;
using AppUrdaira.Helpers;
using AppUrdaira.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(FilesAndroidHelper))]
namespace AppUrdaira.Droid.Helpers
{
    public class FilesAndroidHelper : IFilesHelper
    {
        public string Copy(string pathFile, string file)
        {
            try
            {
                string rutaArchivo = pathFile;

                string rutaNuevoDirectorio = Path.Combine(
                    Android.OS.Environment.ExternalStorageDirectory.AbsolutePath,
                    Android.OS.Environment.DirectoryDownloads,
                    "UrdairaBD");

                string rutaPublica = Path.Combine(rutaNuevoDirectorio,file);

                // crer directorio si no existe
                if(!Directory.Exists(rutaNuevoDirectorio))
                    Directory.CreateDirectory(rutaNuevoDirectorio);

                // copiar archivo
                File.Copy(rutaArchivo, rutaPublica, overwrite: true);

                // devolver la ruta pública
                return rutaPublica;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[FileHelper:Copy]Error:" + ex.Message);
                return null;
            }
        }
    }
}