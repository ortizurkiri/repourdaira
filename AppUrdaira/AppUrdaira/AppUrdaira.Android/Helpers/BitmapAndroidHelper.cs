﻿using Android.Graphics;
using AppUrdaira.Droid.Helpers;
using AppUrdaira.Interfaces;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(BitmapAndroidHelper))]
namespace AppUrdaira.Droid.Helpers
{
    public class BitmapAndroidHelper : IBitmapHelper
    {

        /// <summary>
        /// Obtener logo en string
        /// </summary>
        /// <returns></returns>
        public string GetLogo(string base64)
        {
            try
            {
                return string.Empty;
                //return GetLogo(GetMLogo(base64));
            }
            catch (Exception ex)
            {
                System.Diagnostics. Debug.WriteLine("[BitmapAndroidHelper:GetLogo]Error: " + ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Obtener logo en bytes
        /// </summary>
        /// <returns></returns>
        public byte[] GetBLogo(string base64)
        {
            try
            {
                byte[] byteBLogo = null;
                byte[] imageBytes = !string.IsNullOrEmpty(base64) ? Convert.FromBase64String(base64) : null;

        


                if (imageBytes != null)
                    byteBLogo = imageBytes;
                else
                {
                    // Decodificar los datos de la imagen y crear un objeto Bitmap
                    MemoryStream streamBmp = new MemoryStream();
                    Bitmap bmp = BitmapFactory.DecodeResource(Android.App.Application.Context.Resources, Resource.Drawable.icon_about);

                    if (bmp != null)
                    {
                        bmp.Compress(Bitmap.CompressFormat.Png, 100, streamBmp);
                        byteBLogo = streamBmp.ToArray();
                    }
                }

                // load the PNG image from file
                var bitmap = SKBitmap.Decode(byteBLogo);

                // convert the bitmap to a byte array in ESC/POS format
                var width = bitmap.Width;
                var height = bitmap.Height;
                var data = new List<byte>();

                // send the print command to the printer
                data.AddRange(new byte[] { 0x1B, 0x33, 0x00 });

                // send the image data to the printer
                for (int y = 0; y < height; y += 8)
                {
                    for (int x = 0; x < width; x++)
                    {
                        byte b = 0;
                        for (int i = 0; i < 8; i++)
                        {
                            int yy = y + i;
                            if (yy < height)
                            {
                                SKColor color = bitmap.GetPixel(x, yy);
                                if (color.Alpha != 0)
                                {
                                    b |= (byte)(1 << i);
                                }
                            }
                        }
                        data.Add(b);
                    }
                }

                // send the print command to the printer
                data.AddRange(new byte[] { 0x1B, 0x32 });


                return data.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[BitmapAndroidHelper:GetBLogo]Error: " + ex.Message);
                return null;
            }
        }
    }
}