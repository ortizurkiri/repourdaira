﻿using AppUrdaira.Droid.Helpers;
using AppUrdaira.Interfaces;
using System;

[assembly: Xamarin.Forms.Dependency(typeof(TestAndroidHelper))]
namespace AppUrdaira.Droid.Helpers
{
    public class TestAndroidHelper : ITestHelper
    {
        public object Test()
        {
            try
            {
                //TestInvokeXamarin.Test test = new TestInvokeXamarin.Test();
                //var ini = test.Inicializar();
                return null;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[TestAndroidHelper:Test]Error:" + ex.Message);
                return null;
            }
        }
    }
}