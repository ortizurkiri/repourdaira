﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppUrdaira.Droid.Helpers;
using AppUrdaira.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(DialogsAndroidHelper))]
namespace AppUrdaira.Droid.Helpers
{
    public class DialogsAndroidHelper : IDialogsHelper
    {
        public void ShowToast(string message, bool showLongTime = true)
        {
            try
            {
                Toast.MakeText(Android.App.Application.Context, message, showLongTime ? ToastLength.Long : ToastLength.Short).Show();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[DialogsAndroidHelper:ShowToast]Error: " + ex.Message);
            }
        }
    }
}