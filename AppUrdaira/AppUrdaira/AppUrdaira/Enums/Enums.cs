﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppUrdaira.Enums
{

    public class Enums
    {
        public enum BluetoothDevicesEnum
        {
            Printer,
            Scanner
        }

        public enum FontSizeTextPrinterEnum
        {
            Default,
            SmallFont,
            Bold,
            DoubleHeight,
            DoubleWidth,
        }

        public enum JustifyTextPrinterEnum
        {
            Start,
            Center,
            End
        }

        public enum NavigateDeliveryNoteEnum
        {
            First,
            Before,
            Next,
            Last,
            LastSave
        }
    }
}
