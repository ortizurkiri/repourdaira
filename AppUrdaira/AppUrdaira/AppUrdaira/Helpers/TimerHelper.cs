﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AppUrdaira.Helpers
{
    internal delegate void TimerCallback(object state);
    internal sealed class TimerHelper : CancellationTokenSource, IDisposable
    {
        private TimerCallback _callback;
        private object _state;
        private int _dueTime;
        private int _period;
        private bool _stop;


        public TimerHelper(TimerCallback callback, object state, int interval)
        {
            _callback = callback;
            _state = state;
            _dueTime = interval;
            _period = interval;
        }

        public new void Dispose()
        {
            base.Cancel();
        }

        public void Stop()
        {
            _stop = true;
        }

        public void Restart()
        {
            Stop();
            Start();
        }
        /// <summary>
        /// Start again or restart to changes take effect
        /// </summary>
        /// <param name="value"></param>
        public void Interval(int value)
        {
            _dueTime = value;
            _period = value;
        }


        private async Task<bool> TimerInterval(object t, object s)
        {
            var tuple = (Tuple<TimerCallback, object>)s;

            while (!_stop)
            {
                if (IsCancellationRequested)
                    break;
                await Task.Run(() => tuple.Item1(tuple.Item2));
                await Task.Delay(_period);
            }
            return true;
        }


        public void Start()
        {
            _stop = false;

            Task.Delay(_dueTime, Token).ContinueWith(async (t, s) =>
            {
                await TimerInterval(t, s);

            }, Tuple.Create(_callback, _state), CancellationToken.None,
                TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Default);
        }
    }
}
