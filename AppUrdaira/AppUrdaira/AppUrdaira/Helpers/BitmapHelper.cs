﻿using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace AppUrdaira.Helpers
{
    public class BitmapHelper : Interfaces.IBitmapHelper
    {
        public string GetLogo(string base64)
        {
            try
            {
                return DependencyService.Get<Interfaces.IBitmapHelper>()?.GetLogo(base64);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[BitmapHelper:GetLogo]Error: " + ex.Message);
                return string.Empty;
            }
        }

        public byte [] GetBLogo(string base64)
        {
            try
            {
                return DependencyService.Get<Interfaces.IBitmapHelper>()?.GetBLogo(base64);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[BitmapHelper:GetBLogo]Error: " + ex.Message);
                return null;
            }
        }
    }
}
