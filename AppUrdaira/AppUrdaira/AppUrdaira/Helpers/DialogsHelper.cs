﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AppUrdaira.Helpers
{
    public class DialogsHelper : Interfaces.IDialogsHelper
    {
        public void ShowToast(string message, bool showLongTime = true)
        {
            try
            {
                DependencyService.Get<Interfaces.IDialogsHelper>()?.ShowToast(message, showLongTime);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[DialogsHelper:ShowToast]Error: " + ex.Message);
            }
        }
    }
}
