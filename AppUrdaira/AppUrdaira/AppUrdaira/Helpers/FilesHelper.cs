﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AppUrdaira.Helpers
{
    public class FilesHelper : Interfaces.IFilesHelper
    {
        public string Copy(string pathFile, string file)
        {
            return DependencyService.Get<Interfaces.IFilesHelper>()?.Copy(pathFile, file);
        }
    }
}
