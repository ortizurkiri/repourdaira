﻿using AppUrdaira.Models;
using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeliveryNoteLinePage : ContentPage
    {
        DeliveryNoteLineViewModel ViewModel;

        /// <summary>
        /// Constructor
        /// </summary>
        public DeliveryNoteLinePage(GenericModel gm)
        {
            try
            {
                InitializeComponent();
                ViewModel = new DeliveryNoteLineViewModel(gm);
                BindingContext = ViewModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLinePage:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento cuando aparece la página
        /// </summary>
        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                // si estamos inicializando, lo desactivamos
                ViewModel.DisableInitialize();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLinePage:OnAppearing]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al salir del entry de cantidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryQuantity_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(xEntryQuantity.Text))
                    ViewModel.QuantityStr = "0";

                ViewModel.RecalculateAmountCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLinePage:xEntryQuantity_Unfocused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al salir del entry de precio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryPrice_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(xEntryQuantity.Text))
                    ViewModel.PriceStr = "0";

                ViewModel.RecalculateAmountCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLinePage:xEntryQuantity_Unfocused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al hacer foco sobre el campo cantidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryQuantity_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                Device.InvokeOnMainThreadAsync(() =>
                {
                    try
                    {
                        xEntryQuantity.CursorPosition = 0;
                        xEntryQuantity.SelectionLength = xEntryQuantity.Text?.Length ?? 0;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[DeliveryNoteLinePage:xEntryQuantity_Focused:d]Error:" + ex.Message);
                    }
                });

            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLinePage:xEntryQuantity_Focused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al hacer foco sobre el campo precio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryPrice_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                Device.InvokeOnMainThreadAsync(() =>
                {
                    try
                    {
                        xEntryPrice.CursorPosition = 0;
                        xEntryPrice.SelectionLength = xEntryPrice.Text?.Length ?? 0;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[DeliveryNoteLinePage:xEntryPrice_Focused:d]Error:" + ex.Message);
                    }
                });

            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLinePage:xEntryPrice_Focused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al salir del entry de rappel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void xEntryRappel_Unfocused(object sender, FocusEventArgs e)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(xEntryRappel.Text))
        //            ViewModel.Rappel = 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("[DeliveryNoteLinePage:xEntryRappel_Unfocused]Error:" + ex.Message);
        //    }
        //}

        /// <summary>
        /// Evento al salir del entry de recargo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void xEntryRecargo_Unfocused(object sender, FocusEventArgs e)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(xEntryRecargo.Text))
        //            ViewModel.Recargo = 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("[DeliveryNoteLinePage:xEntryRecargo_Unfocused]Error:" + ex.Message);
        //    }
        //}
    }
}