﻿using AppUrdaira.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace AppUrdaira.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}