﻿using AppUrdaira.Models;
using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InvoicePage : ContentPage
    {
        public InvoiceViewModel ViewModel;

        /// <summary>
        /// Constructor
        /// </summary>
        public InvoicePage(GenericModel genericModel)
        {
            try
            {        
                InitializeComponent();
                InitializeQR();
                BindingContext = ViewModel = new InvoiceViewModel(genericModel);
             
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InvoicePage:()]Error:" + ex.Message);
            }
        }

        private void InitializeQR()
        {
            try
            {
                xQR.BarcodeFormat = ZXing.BarcodeFormat.QR_CODE;
                xQR.HeightRequest = 200;
                xQR.WidthRequest = 200;
                xQR.BarcodeOptions = new ZXing.Common.EncodingOptions()
                {
                    Height = 200,
                    Width = 200
                };
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InvoicePage:InitializeQR]Error:" + ex.Message);
            }
        }
    }
}