﻿using AppUrdaira.Models;
using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectedPrintPage : ContentPage
    {
        SelectedPrintViewModel ViewModel;
        public SelectedPrintPage(List<TextPrinterModel> listTextToPrint = null, bool autoLoadPrinter = false)
        {
            try
            {
                InitializeComponent();
                ViewModel = new SelectedPrintViewModel(listTextToPrint, autoLoadPrinter);
                BindingContext = ViewModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintPage:()]" + ex.Message);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintPage:OnAppearing]" + ex.Message);
            }
        }
        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintPage:OnDisappearing]" + ex.Message);
            }
        }
    }
}