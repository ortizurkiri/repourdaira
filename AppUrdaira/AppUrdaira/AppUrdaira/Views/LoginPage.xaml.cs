﻿using AppUrdaira.Models;
using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        LoginViewModel ViewModel;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public LoginPage()
        {
            try
            {
                InitializeComponent();
                BindingContext = ViewModel = new LoginViewModel();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginPage:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento cuando va a aparecer la página
        /// </summary>
        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                ViewModel.Initialize();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginPage:OnAppearing]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento cuando va a desaparecer la página
        /// </summary>
        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginPage:OnDisappearing]Error:" + ex.Message);
            }
        }
    }
}