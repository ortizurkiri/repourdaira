﻿using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeliveryNotePage : ContentPage
    {
        public DeliveryNoteViewModel ViewModel;

        /// <summary>
        /// Constructor
        /// </summary>
        public DeliveryNotePage()
        {
            try
            {        
                InitializeComponent();
                BindingContext = ViewModel = new DeliveryNoteViewModel();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento cuando va a aparecer la página
        /// </summary>
        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                ViewModel.RefreshCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:OnAppearing]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al desaparecer la página
        /// </summary>
        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();
                ViewModel.SaveGenericModelInBBDDAsyncCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:OnDisappearing]Error:" + ex.Message);
            }
          
        }

        /// <summary>
        /// Evento al salir del entry de rappel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryRappel_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(xEntryRappel.Text))
                    ViewModel.Rappel = 0;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:xEntryRappel_Unfocused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al pulsar el botón de búsqueda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSearchBarCustomer_Completed(object sender, EventArgs e)
        {
            try
            {
                //si no se visualiza el listado de clientes, mostrarlo
                if (ViewModel.ListCustomer?.Count > 0 && !ViewModel.IsVisibleListCustomers)
                    ViewModel.IsVisibleListCustomers = true;
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:xSearchBarCustomer_Completed]Error:" + ex.Message);
            }
        }


        /// <summary>
        /// Evento al cambiar texto del filtrado de clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSearchBarCustomer_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                ViewModel.ChangeSelectedCustomerCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:xSearchBarCustomer_TextChanged]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Seleccionar un cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xListCustomer_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                Models.ClientesModel customer = e.SelectedItem as Models.ClientesModel;

                if (customer != null)
                {
                    ViewModel.SelectedCustomer = customer;
                    ViewModel.IsVisibleListCustomers = false;
                }

                xListCustomer.SelectedItem = null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:xListCustomer_ItemSelected]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al hacer foco sobre el entry para clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSearchBarCustomer_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                // indicamos que ya no es el primer acceso a clientes
                if (ViewModel.IsFirstAccessCustomers)
                    ViewModel.IsFirstAccessCustomers = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNotePage:xSearchBarCustomer_Focused]Error:" + ex.Message);
            }
        }
    }
}