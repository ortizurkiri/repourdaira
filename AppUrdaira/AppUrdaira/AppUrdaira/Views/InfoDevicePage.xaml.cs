﻿using AppUrdaira.Utilities;
using AppUrdaira.ViewModels;
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoDevicePage : ContentPage
    {
        #region Propiedades
        InfoDeviceViewModel ViewModel;
        #endregion

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public InfoDevicePage()
        {
            try
            {
                InitializeComponent();
                BindingContext = ViewModel = new InfoDeviceViewModel();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:()]Error:" + ex.Message);
            }
 
        }

        /// <summary>
        /// Evento al aparecer la página
        /// </summary>
        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                ViewModel.InitializeFieldsCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:OnAppearing]Error:" + ex.Message);
            }    
        }

        /// <summary>
        /// Evento al salir del foco del nombre del dispositivo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryDevice_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(xEntryDevice.Text))
                    ViewModel.IdDevice = string.Empty;

                ViewModel.SetIdDeviceCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:xEntryDevice_Unfocused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al salir del foco del host
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryHost_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(xEntryHost.Text))
                    ViewModel.Host = AppConfig.Instance.Globals.Host;

                ViewModel.SetHostCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:xEntryHost_Unfocused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al salir del foco de la BBDD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntryBBDD_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {            
                ViewModel.SetBBDDCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:xEntryBBDD_Unfocused]Error:" + ex.Message);
            }
        }


        /// <summary>
        /// Evento al cambiar texto del filtrado de clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSearchBarCustomer_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                ViewModel.ChangeSelectedCustomerCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:xSearchBarCustomer_TextChanged]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al hacer foco sobre el entry para clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSearchBarCustomer_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                // indicamos que ya no es el primer acceso a clientes
                if (ViewModel.IsFirstAccessCustomers)
                    ViewModel.IsFirstAccessCustomers = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:xSearchBarCustomer_Focused]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Evento al pulsar el botón de búsqueda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSearchBarCustomer_Completed(object sender, EventArgs e)
        {
            try
            {
                //si no se visualiza el listado de clientes, mostrarlo
                if (ViewModel.ListCustomer?.Count > 0 && !ViewModel.IsVisibleListCustomers)
                    ViewModel.IsVisibleListCustomers = true;

            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:xSearchBarCustomer_Completed]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Seleccionar un cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xListCustomer_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                Models.ClientesModel customer = e.SelectedItem as Models.ClientesModel;

                if (customer != null)
                {
                    ViewModel.SelectedCustomer = customer;
                    ViewModel.IsVisibleListCustomers = false;
                }

                xListCustomer.SelectedItem = null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDevicePage:xListCustomer_ItemSelected]Error:" + ex.Message);
            }
        }
    }
}