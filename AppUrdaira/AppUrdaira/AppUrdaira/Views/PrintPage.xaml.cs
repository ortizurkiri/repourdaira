﻿using AppUrdaira.Models;
using AppUrdaira.ViewModels;
using Newtonsoft.Json;
using Shiny.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class PrintPage : ContentPage
    {
        PrintViewModel ViewModel;
        /// <summary>
        /// Constructor
        /// </summary>
        public PrintPage(List<TextPrinterModel> listTextToPrint)
        {
            try
            {
                InitializeComponent();
                BindingContext = ViewModel = new PrintViewModel(listTextToPrint);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPage:()]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Evento al aparecer la página
        /// </summary>
        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPage:OnAppearing]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Evento al desaparecer la página
        /// </summary>
        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();
                ViewModel.DisconnectDeviceCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPage:OnDisappearing]Error: " + ex.Message);
            }
        }
    }
}