﻿using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LotsPage : ContentPage
    {
        #region Propiedades
        LotsViewModel ViewModel;
        #endregion

        public LotsPage()
        {
            try
            {
                InitializeComponent();
                ViewModel = new LotsViewModel();
                BindingContext = ViewModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LotsPage:()]Error:" + ex.Message);
            }
      
        }

        /// <summary>
        /// Evento al salir del entry de temporadas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xEntrySeasons_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (xEntrySeasons?.Text == null)
                    ViewModel.Seasons = string.Empty;

                ViewModel.SetSeasonsCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LotsPage:xEntrySeasons_Unfocused]Error:" + ex.Message);
            }
        }
    }
}