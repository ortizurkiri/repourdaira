﻿using AppUrdaira.Models;
using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StockLotsPage : ContentPage
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public StockLotsPage(List<LoteLnExtendedModel> listLots)
        {
            try
            {
                InitializeComponent();
                BindingContext = new StockLotsViewModel(listLots);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[StockLotsPage:()]Error:" + ex.Message);
            }
        }
    }
}