﻿using AppUrdaira.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        #region Propiedades
        HomeViewModel ViewModel;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public HomePage()
        {
            try
            {
                InitializeComponent();
                ViewModel = new HomeViewModel();
                BindingContext = ViewModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[HomePage:()]Error:" + ex.Message);
            }
        }
    }
}