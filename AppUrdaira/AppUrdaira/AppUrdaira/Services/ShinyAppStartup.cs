﻿using Microsoft.Extensions.DependencyInjection;
using Shiny;

namespace AppUrdaira.Services
{
    public class ShinyAppStartup : Shiny.ShinyStartup
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.UseBleClient();
            services.UseBleHosting();
        }
    }
}
