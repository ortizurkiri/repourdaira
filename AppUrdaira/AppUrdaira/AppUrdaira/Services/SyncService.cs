﻿using AppUrdaira.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Services
{
    public class SyncService
    {
        public const int NUN_REGISTROS_SINCRO = 10000;

        static SyncService()
        {

        }

        public async Task SendPendingDeliveryNotes()
        {
            try
            {
                // si los WS no están disponibles, no entramos
                if (!await IsAvailableWS().ConfigureAwait(false))
                    return;

                //inicializar variables
                List<AlbaranCbModel> listDeliveryNote = null;
                List<AlbaranLnModel> listDeliveryNoteLine = null;
                ParametrosInvoiceCreateOutModel parametrosInvoiceCreateOut = null;

                await Task.Delay(100).ConfigureAwait(false);

                //obtener listado de albaranes pendientes
                listDeliveryNote = await AlbaranCbModel.GetListDeliveryNoteByTagEnviado0BBDD().ConfigureAwait(false);

                //obtener listado de líneas de albaranes pendientes
                listDeliveryNoteLine = await AlbaranLnModel.GetListDeliveryNoteLineByTagEnviado0BBDD().ConfigureAwait(false);

                //enviar y actualizar albaranes
                if(listDeliveryNote?.Count > 0)
                {
                    foreach (AlbaranCbModel deliveryNote in listDeliveryNote)
                    {
                        //actualizar tag reenvio
                        deliveryNote.TagReenvio = Utilities.Generators.TagGenerator();

                        // insertar líneas de albarán
                        deliveryNote.LineasAlbaran = deliveryNote.GetListDeliveryNoteLineByGeneralListToSend(listDeliveryNoteLine) ?? new List<AlbaranLnModel>();

                        //enviar a ws
                        parametrosInvoiceCreateOut = await AlbaranCbModel.InsertAlbaranYLineasWS(deliveryNote);

                        //si enviado, actualizar y guardar en BBDD
                        if (parametrosInvoiceCreateOut?.ResultadoOK ?? false)
                            _ = deliveryNote.SaveToBBDD(updateTagEnviado: true);      
                    }
                }      
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:SendDeliveryNotes]Error: " + ex.Message);
            }
        }

        public static bool EditTableColumnSQLite(string nombreSqlite, string nombreTabla, Dictionary<string, string> diccNombreTipoColumna)
        {
            try
            {
                // Validar parámetros.
                if (string.IsNullOrEmpty(nombreSqlite) || string.IsNullOrEmpty(nombreTabla) || diccNombreTipoColumna == null || diccNombreTipoColumna.Count == 0)
                {
                    Debug.WriteLine($"No se ha podido modificar la entidad, algún parámetro está vacío.");
                    return false;
                }

                string fullFileName = Utilities.Constants.DatabasePath;
                using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(fullFileName))
                {
                    // Comprobar que existe la tabla.
                    var selectTableQuery = $"SELECT count(*) FROM {nombreTabla}";
                    var selectTableCommand = connection.CreateCommand(selectTableQuery);
                    try
                    {
                        var selectTableResult = selectTableCommand.ExecuteScalar<int>();
                    }
                    catch (Exception stex)
                    {
                        Debug.WriteLine($"Query: {selectTableQuery}");
                        Debug.WriteLine($"Exception: {stex.Message}");
                        throw new Exception($"Tabla {nombreTabla} no encontrada en {nombreSqlite}.");
                    }
                    foreach (KeyValuePair<string, string> nombreTipoCol in diccNombreTipoColumna)
                    {
                        // Comprobar si existe la columna.
                        var selectColumnQuery = $"SELECT {nombreTipoCol.Key} FROM {nombreTabla} LIMIT 1";
                        var selectColumnCommand = connection.CreateCommand(selectColumnQuery);
                        try
                        {
                            var selectColumnResult = selectColumnCommand.ExecuteScalar<int>();
                            continue;
                        }
                        catch (Exception scex)
                        {
                            Debug.WriteLine($"Query: {selectColumnQuery}");
                            Debug.WriteLine($"Exception: {scex.Message}");
                        }
                        // Si no existe la columna llega hasta aquí, e intenta modificar la tabla.
                        var alterTableQuery = $"ALTER TABLE {nombreTabla} ADD {nombreTipoCol.Key} {nombreTipoCol.Value}";
                        var alterTableCommand = connection.CreateCommand(alterTableQuery);
                        try
                        {
                            var alterTableResult = alterTableCommand.ExecuteScalar<int>();
                            Debug.WriteLine($"Se añadió la columna {nombreTipoCol.Key} a la tabla {nombreTabla}.");
                        }
                        catch (Exception atex)
                        {
                            Debug.WriteLine($"Query: {alterTableQuery}");
                            Debug.WriteLine($"Exception: {atex.Message}");
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:ModificarColumnaTablaSQLite]Error: " + ex.Message);
                return false;
            }
        }

        public async Task SyncInitialList(bool showMsg = false)
        {
            try
            {
                // si los WS no están disponibles, no entramos
                if (!await IsAvailableWS().ConfigureAwait(false))
                    return;

                //inicializar variables
                List<UserModel> listaUsuarios = null;
                List<ClientesModel> listaClientes = null;
                List<FormasPagoModel> listaFormasPago = null;
                List<EntidadesModel> listaEntidades = null;
                List<ProductosModel> listaProductos = null;
                List<LoteLnModel> listaLotes = null;
                List<UnidadesMedidaModel> listaUnidades = null;
                List<TipoIvaModel> listaTipoIVA = null;
                List<RecargoEquivalenciaModel> listaRecargo = null;
                List<AlbaranCbModel> listaCabeceraAlbaranes = null;
                List<AlbaranLnModel> listaLineaAlbaranes = null;
                List<AlbaranCbModel> listaCabeceraAlbaranesOriginales = null;
                List<AlbaranLnModel> listaLineaAlbaranesOriginales = null;
                await AppConfig.Instance.Database.InitializeAsync();

                //mostrar mensaje
                if (showMsg)
                    AppConfig.Instance.Dialogs.DisplayToast(message: "Sincronizando usuarios...", showLongTime: false);
                //asignar usuarios
                listaUsuarios = await GetSincroUsuarios().ConfigureAwait(false);
                //guardar en bbdd
                await UserModel.InsertOrUpdateListUserBBDD(listaUsuarios);

                //mostrar mensaje
                if (showMsg)
                    AppConfig.Instance.Dialogs.DisplayToast(message: "Sincronizando clientes, artículos, lotes...", showLongTime: false);
                //asignar clientes
                listaClientes = await GetSincroClientes().ConfigureAwait(false);
                //guardar en bbdd
                await ClientesModel.InsertOrUpdateListCustomerBBDD(listaClientes);

                //asignar formas de pago
                listaFormasPago = await GetSincroFormasPago().ConfigureAwait(false);
                //guardar en bbdd
                await FormasPagoModel.InsertOrUpdateListWayToPayBBDD(listaFormasPago);

                //asignar entidades
                listaEntidades = await GetSincroEntidades().ConfigureAwait(false);
                //guardar en bbdd
                await EntidadesModel.InsertOrUpdateListEntityBBDD(listaEntidades);

                //asignar productos
                listaProductos = await GetSincroProductos().ConfigureAwait(false);
                //guardar en bbdd
                await ProductosModel.InsertOrUpdateListArticleBBDD(listaProductos);

                //asignar lotes
                listaLotes = await GetSincroLotes().ConfigureAwait(false);
                //guardar en bbdd
                await LoteLnModel.InsertOrUpdateListLotBBDD(listaLotes);

                //asignar unidades
                listaUnidades = await GetSincroUnidadesMedida().ConfigureAwait(false);
                //guardar en bbdd
                await UnidadesMedidaModel.InsertOrUpdateListUnitBBDD(listaUnidades);

                //asignar tipo iva
                listaTipoIVA = await GetSincroTipoIva().ConfigureAwait(false);
                //guardar en bbdd
                await TipoIvaModel.InsertOrUpdateListTipoIvaBBDD(listaTipoIVA);

                //asignar recargo
                listaRecargo = await GetSincroRecargoEquivalencia().ConfigureAwait(false);
                //guardar en bbdd
                await RecargoEquivalenciaModel.InsertOrUpdateListSurchargeBBDD(listaRecargo);

                //mostrar mensaje
                if (showMsg)
                    AppConfig.Instance.Dialogs.DisplayToast(message: "Sincronizando albaranes...", showLongTime: false);

                //asignar cabecera de albaranes
                listaCabeceraAlbaranes = await GetSincroAlbaranesCb();
                //guardar en bbdd
                await AlbaranCbModel.InsertOrUpdateListAlbaranCbModelBBDD(listaCabeceraAlbaranes);

                //asignar línea de albaranes
                listaLineaAlbaranes = await GetSincroAlbaranesLn();
                //guardar en bbdd
                await AlbaranLnModel.InsertOrUpdateListaAlbaranLnModelBBDD(listaLineaAlbaranes);

                //asignar cabecera de albaranes originales
                listaCabeceraAlbaranesOriginales = await GetSincroAlbaranesCbOriginal();
                //guardar en bbdd
                await AlbaranCbModel.InsertOrUpdateListAlbaranCbModelBBDD(listdeliveryNote: listaCabeceraAlbaranesOriginales, tableName: AlbaranCbOriginalModel.TableName);

                //asignar línea de albaranes originales
                listaLineaAlbaranesOriginales = await GetSincroAlbaranesLnOriginal();
                //guardar en bbdd
                await AlbaranLnModel.InsertOrUpdateListaAlbaranLnModelBBDD(listDeliveryNoteLine: listaLineaAlbaranesOriginales, tableName: AlbaranLnOriginalModel.TableName);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:SendDeliveryNotes]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener sincro de cabecera de albaranes
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranCbModel>> GetSincroAlbaranesCb()
        {
            try
            {
                //inicializar variables
                List<AlbaranCbModel> listaAlbaranesCb = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await AlbaranCbModel.GetLastTagReenvioDeliveryNoteBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSAlbaranes.SincroContadorAlbaranesCb( 
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                if(contador > 1)
                {
                    //consultar líneas
                    listaAlbaranesCb = await WSModel.Get<List<AlbaranCbModel>>(
                        url: WSAlbaranes.SincroListAlbaranesCb(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);

                    //si hemos recibido correctamente el listado de albaranes
                    if (listaAlbaranesCb?.Count > 0)
                    {
                        //actualizar es sincro
                        listaAlbaranesCb = listaAlbaranesCb.Select(x =>
                        {
                            x.EsSincro = true;
                            return x;
                        }).ToList();
                    }
                }

                //devolver listado
                return listaAlbaranesCb;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroAlbaranesCb]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de líneas de albaranes
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranLnModel>> GetSincroAlbaranesLn()
        {
            try
            {
                //inicializar variables
                List<AlbaranLnModel> listaAlbaranesLn = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await AlbaranLnModel.GetLastTagReenvioDeliveryNoteLineBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSAlbaranes.SincroContadorAlbaranesLn(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                if(contador > 1)
                {
                    //consultar líneas
                    listaAlbaranesLn = await WSModel.Get<List<AlbaranLnModel>>(
                        url: WSAlbaranes.SincroListAlbaranesLn(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);

                    //si hemos recibido correctamente el listado de albaranes
                    if (listaAlbaranesLn?.Count > 0)
                    {
                        //actualizar es sincro
                        listaAlbaranesLn = listaAlbaranesLn.Select(x =>
                        {
                            x.EsSincro = true;
                            return x;
                        }).ToList();
                    }
                }

                //devolver listado
                return listaAlbaranesLn;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroAlbaranesLn]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de usuarios
        /// </summary>
        /// <returns></returns>
        public static async Task<List<UserModel>> GetSincroUsuarios()
        {
            try
            {
                //inicializar variables
                List<UserModel> listaUsuarios = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await UserModel.GetLastTagReenvioUserBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSUsuario.SincroContadorUsuarios(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if(contador > 1)
                {
                    listaUsuarios = await WSModel.Get<List<UserModel>>(
                        url: WSUsuario.SincroListUsuarios(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaUsuarios;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroUsuarios]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de clientes
        /// </summary>
        /// <returns></returns>
        public static async Task<List<ClientesModel>> GetSincroClientes()
        {
            try
            {
                //inicializar variables
                List<ClientesModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await ClientesModel.GetLastTagReenvioClienteBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSCliente.SincroContadorClientes(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if(contador > 1)
                {
                    listaItems = await WSModel.Get<List<ClientesModel>>(
                        url: WSCliente.SincroListClientes(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroClientes]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de formas de pago
        /// </summary>
        /// <returns></returns>
        public static async Task<List<FormasPagoModel>> GetSincroFormasPago()
        {
            try
            {
                //inicializar variables
                List<FormasPagoModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await FormasPagoModel.GetLastTagReenvioWayToPayBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSFormasPago.SincroContadorFormasPago(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if (contador > 1)
                {
                    listaItems = await WSModel.Get<List<FormasPagoModel>>(
                        url: WSFormasPago.SincroListFormasPago(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroFormasPago]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de entidades
        /// </summary>
        /// <returns></returns>
        public static async Task<List<EntidadesModel>> GetSincroEntidades()
        {
            try
            {
                //inicializar variables
                List<EntidadesModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await EntidadesModel.GetLastTagReenvioEntityBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSEntidades.SincroContadorEntidades(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if (contador > 1)
                {
                    listaItems = await WSModel.Get<List<EntidadesModel>>(
                        url: WSEntidades.SincroListEntidades(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroEntidades]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de productos
        /// </summary>
        /// <returns></returns>
        public static async Task<List<ProductosModel>> GetSincroProductos()
        {
            try
            {
                //inicializar variables
                List<ProductosModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await ProductosModel.GetLastTagReenvioArticleBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSProductos.SincroContadorProductos(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if (contador > 1)
                {
                    listaItems = await WSModel.Get<List<ProductosModel>>(
                        url: WSProductos.SincroListProductos(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroProductos]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de lotes
        /// </summary>
        /// <returns></returns>
        public static async Task<List<LoteLnModel>> GetSincroLotes()
        {
            try
            {
                //inicializar variables
                List<LoteLnModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await LoteLnModel.GetLastTagReenvioLoteBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSLotes.SincroContadorLotes(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if (contador > 1)
                {
                    listaItems = await WSModel.Get<List<LoteLnModel>>(
                        url: WSLotes.SincroListLotes(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroLotes]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de unidades de medida
        /// </summary>
        /// <returns></returns>
        public static async Task<List<UnidadesMedidaModel>> GetSincroUnidadesMedida()
        {
            try
            {
                //inicializar variables
                List<UnidadesMedidaModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await UnidadesMedidaModel.GetLastTagReenvioUnidadesMedidaBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSUnidadesMedida.SincroContadorUnidadesMedida(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if (contador > 1)
                {
                    listaItems = await WSModel.Get<List<UnidadesMedidaModel>>(
                        url: WSUnidadesMedida.SincroListUnidadesMedida(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroUnidadesMedida]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de tipo de iva
        /// </summary>
        /// <returns></returns>
        public static async Task<List<TipoIvaModel>> GetSincroTipoIva()
        {
            try
            {
                //inicializar variables
                List<TipoIvaModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await TipoIvaModel.GetLastTagReenvioTipoIvaBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSTipoIva.SincroContadorTipoIva(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if (contador > 1)
                {
                    listaItems = await WSModel.Get<List<TipoIvaModel>>(
                        url: WSTipoIva.SincroListTipoIva(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroTipoIva]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de tipo de iva
        /// </summary>
        /// <returns></returns>
        public static async Task<List<RecargoEquivalenciaModel>> GetSincroRecargoEquivalencia()
        {
            try
            {
                //inicializar variables
                List<RecargoEquivalenciaModel> listaItems = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await RecargoEquivalenciaModel.GetLastTagReenvioRecargoEquivalenciaBBDD();

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSRecargoEquivalencia.SincroContadorRecargoEquivalencia(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                //consultar líneas
                if (contador > 1)
                {
                    listaItems = await WSModel.Get<List<RecargoEquivalenciaModel>>(
                        url: WSRecargoEquivalencia.SincroListRecargoEquivalencia(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);
                }

                //devolver listado
                return listaItems;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroRecargoEquivalencia]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de cabecera de albaranes originales
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranCbModel>> GetSincroAlbaranesCbOriginal()
        {
            try
            {
                //inicializar variables
                List<AlbaranCbModel> listaAlbaranesCb = null;
                List<AlbaranCbOriginalModel> listaAlbaranesCbOriginales = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await AlbaranCbModel.GetLastTagReenvioDeliveryNoteBBDD(tableName: AlbaranCbOriginalModel.TableName);

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSAlbaranes.SincroContadorAlbaranesCbOriginales(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador);

                if (contador > 1)
                {
                    //consultar líneas
                    listaAlbaranesCbOriginales = await WSModel.Get<List<AlbaranCbOriginalModel>>(
                        url: WSAlbaranes.SincroListAlbaranesCbOriginales(
                            tagSincro: lastTagReenvio.ToString(),
                            numRegistros: NUN_REGISTROS_SINCRO,
                            body: out bodyContador),
                        body: bodyContador);

                    //si hemos recibido correctamente el listado de albaranes
                    if (listaAlbaranesCbOriginales?.Count > 0)
                    {
                        //actualizar es sincro
                        listaAlbaranesCb = listaAlbaranesCbOriginales.Select(x =>
                        {
                            x.EsSincro = true;
                            return x.GetThisAlbaranCbModel();
                        }).ToList();
                    }
                }

                //devolver listado
                return listaAlbaranesCb;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroAlbaranesCbOriginal]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener sincro de líneas de albaranes originales
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranLnModel>> GetSincroAlbaranesLnOriginal()
        {
            try
            {
                //inicializar variables
                List<AlbaranLnModel> listaAlbaranesLn = null;
                List<AlbaranLnOriginalModel> listaAlbaranesLnOriginales = null;
                long lastTagReenvio = 0;
                int contador = 0;
                string bodyContador = string.Empty;

                //obtener último tag reenvío en local
                lastTagReenvio = await AlbaranLnModel.GetLastTagReenvioDeliveryNoteLineBBDD(tableName: AlbaranLnOriginalModel.TableName);

                //consultar contador a partir del tagreenvio
                contador = await WSModel.Get<int>(
                    url: WSAlbaranes.SincroContadorAlbaranesLnOriginales(
                        tagSincro: lastTagReenvio.ToString(),
                        body: out bodyContador),
                    body: bodyContador
                );

                //consultar líneas
                listaAlbaranesLnOriginales = await WSModel.Get<List<AlbaranLnOriginalModel>>(
                    url: WSAlbaranes.SincroListAlbaranesLnOriginales(
                        tagSincro: lastTagReenvio.ToString(),
                        numRegistros: NUN_REGISTROS_SINCRO,
                        body: out bodyContador),
                    body: bodyContador);

                //si hemos recibido correctamente el listado de albaranes
                if (listaAlbaranesLnOriginales?.Count > 0)
                {
                    //actualizar es sincro
                    listaAlbaranesLn = listaAlbaranesLnOriginales.Select(x =>
                    {
                        x.EsSincro = true;
                        return x.GetThisAlbaranLnModel();
                    }).ToList();
                }

                //devolver listado
                return listaAlbaranesLn;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:GetSincroAlbaranesLnOriginal]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Comprueba si los WS están disponibles
        /// </summary>
        /// <returns></returns>
        public static async Task<bool> IsAvailableWS()
        {
            try
            {
                bool isAvailableWS = false;

                if(AppConfig.Instance.Globals.LoginUser != null && !string.IsNullOrEmpty(AppConfig.Instance.Globals.LoginUser.Usuario)
                    && !string.IsNullOrEmpty(AppConfig.Instance.Globals.LoginUser.Password))
                {
                    isAvailableWS = await WSModel.Login(user: AppConfig.Instance.Globals.LoginUser.Usuario, password: AppConfig.Instance.Globals.LoginUser.Password);
                }

                return isAvailableWS;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SyncService:IsAvailableWS]Error: " + ex.Message);
                return false;
            }
        }
    }
}
