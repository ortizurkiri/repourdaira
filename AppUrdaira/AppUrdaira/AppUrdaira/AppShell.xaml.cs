﻿using AppUrdaira.ViewModels;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace AppUrdaira
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            try
            {
                InitializeComponent();

                Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
                Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
                Routing.RegisterRoute(nameof(SelectedPrintPage), typeof(SelectedPrintPage));
                Routing.RegisterRoute(nameof(PrintPage), typeof(PrintPage));
                Routing.RegisterRoute(nameof(DeliveryNotePage), typeof(DeliveryNotePage));
                Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AppShell:()]Error:" + ex.Message);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AppShell:OnAppearing]Error:" + ex.Message);
            }
        }

        //private async void xMenuItemDevice_Clicked(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        await Shell.Current.GoToAsync($"//{nameof(InfoDevicePage)}?InitialConfig=false");
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("[AppShell:xMenuItemDevice_Clicked]Error:" + ex.Message);
        //    }
        //}

        //private async void xMenuItemLogout_Clicked(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        await Shell.Current.GoToAsync($"//{nameof(LoginPage)}");
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("[AppShell:xMenuItemLogout_Clicked]Error:" + ex.Message);
        //    }
        //}
    }
}
