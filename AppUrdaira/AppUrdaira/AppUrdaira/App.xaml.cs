﻿using AppUrdaira.Services;
using AppUrdaira.Views;
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppUrdaira
{
    public partial class App : Application
    {

        public App()
        {
            try
            {
                InitializeComponent();
                DependencyService.Register<MockDataStore>();
                MainPage = new AppShell();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[App:()]Error:" + ex.Message);
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
