﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class AlbaranLnOriginalModel : AlbaranLnModel
    {
        [JsonIgnore]
        public new const string TableName = "AlbaranLnOriginal";

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AlbaranLnOriginalModel() : base()
        {

        }

        /// <summary>
        /// Convertir AlbaranLnOriginalModel a AlbaranLnModel
        /// </summary>
        /// <returns></returns>
        public AlbaranLnModel ConverterToAlbaranLnModel()
        {
            try
            {
                AlbaranLnModel albaranCb = base.GetThisAlbaranLnModel();
                //albaranCb.NPedido = !string.IsNullOrEmpty(NPedido) ? Convert.ToInt32(NPedido) : 0;

                return albaranCb;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnOriginalModel:ConverterToAlbaranLnModel]Error: " + ex.Message);
                return null;
            }
        }
    }

}
