﻿namespace AppUrdaira.Models
{
    public class NemesysJsonParamsModel
    {
        public string JsonParams { get; set; }
        public int Errores { get; set; }
        public string DerscripcionError { get; set; }
    }
}
