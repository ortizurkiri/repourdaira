﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class AlbaranCbOriginalModel : AlbaranCbModel
    {
        [JsonIgnore]
        public new const string TableName = "AlbaranCbOriginal";

        public new string NPedido { get; set; } = string.Empty;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AlbaranCbOriginalModel() : base()
        {

        }

        /// <summary>
        /// Convertir AlbaranCbOriginalModel a AlbaranCbModel
        /// </summary>
        /// <returns></returns>
        public AlbaranCbModel ConverterToAlbaranCbModel()
        {
            try
            {
                AlbaranCbModel albaranCb = base.GetThisAlbaranCbModel();
                albaranCb.NPedido = !string.IsNullOrEmpty(NPedido) ? Convert.ToInt32(NPedido) : 0;

                return albaranCb;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:ConverterToAlbaranCbModel]Error: " + ex.Message);
                return null;
            }
        }
    }
}
