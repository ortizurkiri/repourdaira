﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class ClientesModel
    {
        [JsonIgnore]
        public const string TableName = "Clientes";

        public static string ControllerNameWS => TableName;

        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [DefaultValue(0)]
        public int IdCliente { get; set; } = 0;

        [DefaultValue(0)]
        public int IdTipo { get; set; } = 0;

        [DefaultValue(0)]
        public double Recargo { get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string NombreEmpresa { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Cif { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string NombreContacto { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string CargoContacto { get; set; } = string.Empty;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Direccion { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string Poblacion { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string Provincia { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string CodPostal { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string Pais { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string Telefono1 { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string Telefono2 { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string Fax { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Email { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string PginaWeb { get; set; } = string.Empty;

        [MaxLength(10)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();   

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [MaxLength(100)]
        [DefaultValue("")]
        public string Banco { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string NCuenta { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Giro { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ClientesModel()
        {

        }

        /// <summary>
        /// Obtener listado de clientes del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListCustomerWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetClientes");
            return url;
        }

        /// <summary>
        /// Insertar o actualizar clientes
        /// </summary>
        /// <param name="listCustomer"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListCustomerBBDD(List<ClientesModel> listCustomer)
        {
            try
            {
                if(listCustomer?.Count > 0)
                {
                    foreach (ClientesModel customer in listCustomer)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(customer).ConfigureAwait(false);
                        Debug.WriteLine($"[ClientesModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ClientesModel:InsertOrUpdateListCustomerBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de clientes
        /// </summary>
        /// <returns></returns>
        public static async Task<List<ClientesModel>> GetListCustomerBBDD()
        {
            try
            {   
                return (await AppConfig.Instance.Database.GetListItemsAsync<ClientesModel>().ConfigureAwait(false))?.OrderBy(x => x.NombreEmpresa)?.ToList();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ClientesModel:GetListCustomerBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener cliente
        /// </summary>
        /// <returns></returns>
        public static async Task<ClientesModel> GetCustomerByIdBBDD(int customerId)
        {
            try
            {
                //Iniciar variables
                ClientesModel customer = null;
                string query = $"SELECT * FROM {TableName} WHERE IdCliente = {customerId}";

                //consulta
                customer = (await AppConfig.Instance.Database.ExecuteQueryListAsync<ClientesModel>(query).ConfigureAwait(false))?.FirstOrDefault();

                //devolver objeto
                return customer;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ClientesModel:GetCustomerByIdBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvío de los clientes
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioClienteBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<ClientesModel> listItems = null;
                ClientesModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<ClientesModel>(query).ConfigureAwait(false);

                //item
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ClientesModel:GetLastTagReenvioClienteBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
