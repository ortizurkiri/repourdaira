﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace AppUrdaira.Models
{
    public class GenericModel : ObservableDbObjectModel
    {
        #region Propiedades
        public static string TableName => "GenericModel";

        [DefaultValue("")]
        [PrimaryKey]
        public string PK 
        { 
            get 
            { 
                return base.RowId; 
            } 
            set { } 
        }

        public Guid GuidGenericModel { get; set; }

        public string NumDeliveryNote { get; set; } = null;

        public DateTime DateDeliveryNote { get; set; } = DateTime.Now;

        public bool Save { get; set; } = false;

        public string Dispositivo { get; set; } = string.Empty;

        public bool CanEditDelivery { get; set; } = true;

        public int NLineas { get; set; } = 0;

        [Ignore]
        public AlbaranLnModel DeliveryNoteLine { get; set; } = null;

        [Ignore]
        public List<ClientesModel> ListCustomers{ get; set; } = null;

        [Ignore]
        public AlbaranCbModel DeliveryNote { get; set; } = null;

        [Ignore]
        public List<AlbaranLnModel> ListDeliveryNoteLns { get; set; } = null;

        [Ignore]
        public List<ProductosModel> ListArticles { get; set; } = null;

        [Ignore]
        public List<EntidadesModel> ListEntities { get; set; } = null;

        [Ignore]
        public List<FormasPagoModel> ListWaysToPay { get; set; } = null;

        [Ignore]
        public List<LoteLnModel> ListLots{ get; set; } = null;

        [Ignore]
        public List<LoteLnExtendedModel> ListLotsExtended { get; set; } = null;

        [Ignore]
        public List<UnidadesMedidaModel> ListUnits{ get; set; } = null;
        [Ignore]
        public List<TipoIvaModel> ListIVA { get; set; } = null;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;
        #endregion


        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public GenericModel()
        {
            try
            {
                GuidGenericModel = Guid.NewGuid();
                Dispositivo = AppConfig.Instance.Globals.Device;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:()2]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Insertar o actualizar generic model
        /// </summary>
        /// <returns></returns>
        public static async Task InsertOrUpdateGenericModelBBDD(GenericModel genericModel)
        {
            try
            {
                //inicializar variables
                int resultGM = 0;

                if (genericModel != null)
                {
                    resultGM = await AppConfig.Instance.Database.SaveItemAsync(genericModel).ConfigureAwait(false);
                    Debug.WriteLine($"[GenericModel:SaveItemAsync] ResultGM: {resultGM}");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:InsertOrUpdateGenericModelBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener generic model completo
        /// </summary>
        /// <returns></returns>
        public static async Task<GenericModel> GetGenericModelByGuidBBDD(Guid guid)
        {
            try
            {
                //Iniciar variables
                GenericModel genericModel = null;
                string query = $"SELECT * FROM {TableName} WHERE GuidGenericModel = '{guid}'";

                //consulta
                genericModel = (await AppConfig.Instance.Database.ExecuteQueryListAsync<GenericModel>(query).ConfigureAwait(false))?.FirstOrDefault();

                if(genericModel != null)
                {
                    genericModel.DeliveryNote = await AlbaranCbModel.GetDeliveryNoteByGuidBBDD(guid).ConfigureAwait(false);
                    genericModel.ListDeliveryNoteLns = await AlbaranLnModel.GetListDeliveryNoteLineByGuidBBDD(guid).ConfigureAwait(false);
                }

                //devolver objeto
                return genericModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:GetGenericModelByGuidBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último generic model
        /// </summary>
        /// <returns></returns>
        public static async Task<GenericModel> GetGenericModelBBDD()
        {
            try
            {
                //Iniciar variables
                List<GenericModel> listGenericModel = null;
                GenericModel genericModel = null;
                string query = $"SELECT * FROM {TableName}";

                //consulta
                listGenericModel = await AppConfig.Instance.Database.ExecuteQueryListAsync<GenericModel>(query).ConfigureAwait(false);

                //primer genericModel
                genericModel = listGenericModel?.FirstOrDefault();

                //devolver objeto
                return genericModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:GetGenericModelBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Guardar generic model en BBDD y campos importantes en asíncrono
        /// </summary>
        public static void SaveGenericModelBBDDAsync(GenericModel genericModel, bool saveDeliveryNote = true, bool saveListDeliveryNoteLine = true)
        {
            try
            {
                _ = Task.Run(() =>
                {
                    try
                    {
                        if (genericModel != null)
                        {
                            //guardar generic
                            _ = InsertOrUpdateGenericModelBBDD(genericModel);

                            //guardar albarán
                            if (saveDeliveryNote)
                                _ = AlbaranCbModel.InsertOrUpdateAlbaranCbModelBBDD(genericModel.DeliveryNote);

                            //guardar líneas de albarán
                            if (saveListDeliveryNoteLine)
                                _ = AlbaranLnModel.InsertOrUpdateListaAlbaranLnModelBBDD(genericModel.ListDeliveryNoteLns);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[GenericModel:SaveGenericModelBBDDAsyncT]Error: " + ex.Message);
                    }

                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:SaveGenericModelBBDDAsync]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Guardar generic model en BBDD y campos importantes en asíncrono
        /// </summary>
        public static async Task SaveGenericModelBBDD(GenericModel genericModel, bool saveDeliveryNote = true, bool saveListDeliveryNoteLine = true)
        {
            try
            {
         
                if (genericModel != null)
                {
                    //guardar generic
                    await InsertOrUpdateGenericModelBBDD(genericModel).ConfigureAwait(false);

                    //guardar albarán
                    if (saveDeliveryNote)
                        await AlbaranCbModel.InsertOrUpdateAlbaranCbModelBBDD(genericModel.DeliveryNote).ConfigureAwait(false);

                    //guardar líneas de albarán
                    if (saveListDeliveryNoteLine)
                        await AlbaranLnModel.InsertOrUpdateListaAlbaranLnModelBBDD(genericModel.ListDeliveryNoteLns).ConfigureAwait(false);
                }
           
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:SaveGenericModelBBDDAsync]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Eliminar cabecera y líneas de GenericModel BBDD
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteHeadAndLinesGenericModelBBDDByGuid()
        {
            try
            {
                //Borrar guid de la cabecera y líneas de albarán actuales
                bool cabeceraEliminada = await AlbaranCbModel.DeleteDeliveryNoteByGuidBBDD(GuidGenericModel);
                bool lineasEliminadas = await AlbaranLnModel.DeleteDeliveryNoteLineByGuidBBDD(GuidGenericModel);

                return cabeceraEliminada && lineasEliminadas;              
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:DeleteHeadAndLinesGenericModelBBDD]Error: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Obtener siguiente nº de línea
        /// </summary>
        /// <returns></returns>
        public int GetNextNLine()
        {
            try
            {
                return NLineas + 1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GenericModel:GetNextNLine]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
