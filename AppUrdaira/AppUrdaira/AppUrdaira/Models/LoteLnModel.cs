﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class LoteLnModel
    {
        [JsonIgnore]
        public const string TableName = "LoteLn";

        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Temporada { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Lote { get; set; } = string.Empty;

        [DefaultValue(0)]
        public int IdLoteLn { get; set; } = 0;

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string FechaLote { get; set; } = "00000000";

        [MaxLength(200)]
        [DefaultValue("")]
        public string Descripcion { get; set; } = string.Empty;

        [DefaultValue(0)]
        public int CantidadBotella { get; set; } = 0;

        [DefaultValue(0)]
        public double CantidadLitros { get; set; } = 0;

        [DefaultValue(0)]
        public double CantidadTotalLitros { get; set; } = 0;

        [MaxLength(10)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        [JsonIgnore]
        public string VisualizarLote => $"{Lote} \t| {CantidadBotella} \t| {CantidadLitros}";

        [JsonIgnore]
        public string FechaLoteFormat => GetFechaLoteFormat("dd/MM/yyyy");

        /// <summary>
        /// Obtiene la fecha formateada
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        private string GetFechaLoteFormat(string format)
        {
            try
            {
                bool isParse = DateTime.TryParseExact(s: FechaLote, format: "M/d/yyyy hh:mm:ss tt", provider: CultureInfo.InvariantCulture, style: DateTimeStyles.None, result: out DateTime fecha);
                string fechaLoteFormat = isParse ? fecha.ToString(format) : FechaLote;

                return fechaLoteFormat;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoteLnModel:GetFechaLoteFormat]Error: " + ex.Message);
                return FechaLote;
            }
        }

        /// <summary>
        /// Insertar o actualizar lotes
        /// </summary>
        /// <param name="listLot"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListLotBBDD(List<LoteLnModel> listLot)
        {
            try
            {
                if (listLot?.Count > 0)
                {
                    foreach (LoteLnModel lot in listLot)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(lot).ConfigureAwait(false);
                        Debug.WriteLine($"[LoteLnModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoteLnModel:InsertOrUpdateListLotBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de lotes
        /// </summary>
        /// <returns></returns>
        public static async Task<List<LoteLnModel>> GetListLotBBDD()
        {
            try
            {
                List<LoteLnModel> listLot = null;
                string query = $@"SELECT 
                                    * 
                                  FROM 
                                    {TableName} 
                                  WHERE 
                                    TagBorrado = 0
                                    AND (CantidadBotella > 0 OR CantidadLitros > 0)
                                  ORDER BY
                                    Lote ASC
                ";

                //consulta
                listLot = await AppConfig.Instance.Database.ExecuteQueryListAsync<LoteLnModel>(query).ConfigureAwait(false);

                return listLot;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoteLnModel:GetListLotBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener listado de lotes filtrando por las temporadas elegidas en Lotes
        /// </summary>
        /// <returns></returns>
        public static async Task<List<LoteLnModel>> GetListLotBBDDFilterSeasons()
        {
            try
            {
                // inicializar variables
                List<LoteLnModel> listLot = null;
                string season = AppConfig.Instance.Globals.Seasons;
                string seasonWithQuotes = !string.IsNullOrEmpty(season) ? string.Join(",", season?.Split(',')?.Select(x => { return $"'{x}'"; })) : string.Empty;
                string queryQuantity = string.Empty;
                string querySeason = string.Empty;
                string query = string.Empty;

                //recuperar listado con alguna cantidad
                queryQuantity = $"AND (CantidadBotella > 0 OR CantidadLitros > 0)";
                //rellenar parte de la consulta para temporadas
                querySeason = !string.IsNullOrEmpty(seasonWithQuotes) ? $"AND Temporada IN ({seasonWithQuotes})" : string.Empty;

                // rellenar consulta
                query = $@"SELECT 
                                * 
                            FROM 
                                {TableName} 
                            WHERE 
                                TagBorrado = 0
                                {queryQuantity}
                                {querySeason}    
                            ORDER BY
                                Temporada ASC,
                                IdLoteLn ASC
                                --Lote ASC
                ";

                //consulta
                listLot = await AppConfig.Instance.Database.ExecuteQueryListAsync<LoteLnModel>(query).ConfigureAwait(false);

                return listLot;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoteLnModel:GetListLotBBDDFilterSeasons]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvío de los lotes
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioLoteBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<LoteLnModel> listItems = null;
                LoteLnModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<LoteLnModel>(query).ConfigureAwait(false);

                //item
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoteLnModel:GetLastTagReenvioLoteBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
