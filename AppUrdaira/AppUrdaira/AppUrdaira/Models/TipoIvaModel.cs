﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class TipoIvaModel
    {
        [JsonIgnore]
        public const string TableName = "TipoIva";

        [PrimaryKey]
        [DefaultValue(0)]
        public int TipoIva { get; set; } = 0;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public TipoIvaModel()
        {

        }

        /// <summary>
        /// Insertar o actualizar tipo de IVA
        /// </summary>
        /// <param name="listUnit"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListTipoIvaBBDD(List<TipoIvaModel> listTipoIva)
        {
            try
            {
                if (listTipoIva?.Count > 0)
                {
                    foreach (TipoIvaModel iva in listTipoIva)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(iva).ConfigureAwait(false);
                        Debug.WriteLine($"[TipoIvaModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TipoIvaModel:InsertOrUpdateListTipoIvaBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de tipo de IVA
        /// </summary>
        /// <returns></returns>
        public static async Task<List<TipoIvaModel>> GetListTipoIvaBBDD()
        {
            try
            {
                List<TipoIvaModel> listIVA = null;

                listIVA = await AppConfig.Instance.Database.GetListItemsAsync<TipoIvaModel>().ConfigureAwait(false);

                return listIVA;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TipoIvaModel:GetListTipoIvaBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvío del tipo de iva
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioTipoIvaBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<TipoIvaModel> listItems = null;
                TipoIvaModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<TipoIvaModel>(query).ConfigureAwait(false);

                //item
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TipoIvaModel:GetLastTagReenvioTipoIvaBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
