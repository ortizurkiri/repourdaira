﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AppUrdaira.Models
{
    [Table("FacturaCb")]
    public class FacturaCbModel
    {
        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [DefaultValue(0)]
        public int Numero { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Ejercicio { get; set; } = string.Empty;

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string FechaFactura { get; set; } = "00000000";

        [MaxLength(50)]
        [DefaultValue("")]
        public string Factura { get; set; } = string.Empty;

        [MaxLength(10)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [DefaultValue(0)]
        public int IdCliente { get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Cliente { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Banco { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string NCuenta { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string SiNo { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Cobrado { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;
    }
}
