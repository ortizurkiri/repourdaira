﻿using AppUrdaira.Extensions;
using AppUrdaira.Utilities;
using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    public class DatabaseModel
    {
        #region Propiedades
        public static readonly Lazy<SQLiteAsyncConnection> lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        public static SQLiteAsyncConnection Database => lazyInitializer.Value;
        public static bool initialized = false;
        #endregion
        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseModel()
        {
            try
            {
                InitializeAsync().SafeFireAndForget(false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Inicializador
        /// </summary>
        /// <returns></returns>
        public async Task InitializeAsync()
        {
            try
            {
                if (!initialized)
                {
                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(UserModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(UserModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(DeliveryNoteModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(DeliveryNoteModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranCbModel).Name))               
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(AlbaranCbModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranCbOriginalModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(AlbaranCbOriginalModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranLnModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(AlbaranLnModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranLnOriginalModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(AlbaranLnOriginalModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(ClientesModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(ClientesModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(TipoClienteModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(TipoClienteModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(FormasPagoModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(FormasPagoModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(EntidadesModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(EntidadesModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(ProductosModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(ProductosModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(LoteCbModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(LoteCbModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(LoteLnModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(LoteLnModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(UnidadesMedidaModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(UnidadesMedidaModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(TipoIvaModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(TipoIvaModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(RecargoEquivalenciaModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(RecargoEquivalenciaModel)).ConfigureAwait(false);

                    if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(GenericModel).Name))
                        await Database.CreateTablesAsync(CreateFlags.None, typeof(GenericModel)).ConfigureAwait(false);

                    initialized = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:InitializeAsync]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Borrar todos los registros
        /// </summary>
        /// <returns></returns>
        public async Task DeleteAsync()
        {
            try
            {
                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(UserModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(UserModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(DeliveryNoteModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(DeliveryNoteModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranCbModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(AlbaranCbModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranCbOriginalModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(AlbaranCbOriginalModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranLnModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(AlbaranLnModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(AlbaranLnOriginalModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(AlbaranLnOriginalModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(ClientesModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(ClientesModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(TipoClienteModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(TipoClienteModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(FormasPagoModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(FormasPagoModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(EntidadesModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(EntidadesModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(ProductosModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(ProductosModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(LoteCbModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(LoteCbModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(LoteLnModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(LoteLnModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(UnidadesMedidaModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(UnidadesMedidaModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(TipoIvaModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(TipoIvaModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(RecargoEquivalenciaModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(RecargoEquivalenciaModel).Name)).ConfigureAwait(false);

                if (Database.TableMappings.Any(m => m.MappedType.Name == typeof(GenericModel).Name))
                    await Database.DeleteAllAsync(Database.TableMappings.FirstOrDefault(m => m.MappedType.Name == typeof(GenericModel).Name)).ConfigureAwait(false);

                initialized = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:DeleteAsync]Error:" + ex.Message);
            }
        }

        public async Task<List<T>> GetListItemsAsync<T>() where T : new()
        {
            try
            {
                return await Database.Table<T>().ToListAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:GetListItemsAsync]Error:" + ex.Message);
                return null;
            }
        }

        public async Task<List<T>> ExecuteQueryListAsync<T>(string query) where T : new()
        {
            try
            {
                return await Database.QueryAsync<T>(query).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:ExecuteQueryListAsync]Error:" + ex.Message);
                return null;
            }
        }

        public async Task<int> ExecuteQueryIntAsync(string query)
        {
            try
            {
                return await Database.ExecuteScalarAsync<int>(query).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:ExecuteQueryIntAsync]Error:" + ex.Message);
                return 0;
            }
        }

        public async Task<int> SaveItemAsync<T>(T item)
        {
            try
            {
                int result = await Database.UpdateAsync(item).ConfigureAwait(false);
                if (result == 0)
                    return await Database.InsertOrReplaceAsync(item).ConfigureAwait(false);

                return result;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:SaveItemAsync]Error:" + ex.Message);
                return 0;
            }
        }

        public async Task<int> DeleteItemAsync<T>(T item)
        {
            try
            {
                return await Database.DeleteAsync(item).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DatabaseModel:DeleteItemAsync]Error:" + ex.Message);
                return 0;
            }
        }
    }
}
