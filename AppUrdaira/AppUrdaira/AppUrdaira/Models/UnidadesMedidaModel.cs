﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class UnidadesMedidaModel
    {
        [JsonIgnore]
        public const string TableName = "UnidadesMedida";

        public static string ControllerNameWS => "UnidadesMedida";

        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [DefaultValue(0)]
        public int IdMedida { get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Descripcion { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string NombreCorto { get; set; } = string.Empty;

        [MaxLength(10)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = "00000000";

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UnidadesMedidaModel()
        {

        }

        /// <summary>
        /// Obtener listado de unidades del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListUnitsWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetUnidadesMedida");

            return url;
        }

        /// <summary>
        /// Insertar o actualizar unidades de medida
        /// </summary>
        /// <param name="listUnit"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListUnitBBDD(List<UnidadesMedidaModel> listUnit)
        {
            try
            {
                if (listUnit?.Count > 0)
                {
                    foreach (UnidadesMedidaModel unit in listUnit)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(unit).ConfigureAwait(false);
                        Debug.WriteLine($"[UnidadesMedidaModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UnidadesMedidaModel:InsertOrUpdateListUnitBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de unidades de medida
        /// </summary>
        /// <returns></returns>
        public static async Task<List<UnidadesMedidaModel>> GetListUnitBBDD()
        {
            try
            {
                List<UnidadesMedidaModel> listUnit = null;

                listUnit = await AppConfig.Instance.Database.GetListItemsAsync<UnidadesMedidaModel>().ConfigureAwait(false);

                return listUnit;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UnidadesMedidaModel:GetListUnitBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvío de las unidades de medida
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioUnidadesMedidaBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<UnidadesMedidaModel> listItems = null;
                UnidadesMedidaModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<UnidadesMedidaModel>(query).ConfigureAwait(false);

                //item
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UnidadesMedidaModel:GetLastTagReenvioUnidadesMedidaBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
