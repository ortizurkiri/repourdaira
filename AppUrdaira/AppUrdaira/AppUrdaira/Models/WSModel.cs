﻿using AppUrdaira.Extensions;
using AppUrdaira.Helpers;
using AppUrdaira.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AppUrdaira.Models
{
    public class WSModel
    {
        // tiempo de espera en segundos
        public const int TIMEOUT_SECONDS = 40;
        /// <summary>
        /// Loguea al usuario
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static async Task<bool> Login(string user, string password)
        {
            try
            {
                bool isLogged = false;

                //encriptar usuario y password
                string encrypted = EncryptHelper.EncryptText($"username={user}&password={password}");

                //generar uri
                Uri uri = new Uri($"{Constants.Protocol}{AppConfig.Instance.Globals.Host}/Nemesys/login?DataX={encrypted}");

                //rellenar la petición
                HttpWebRequest request = WebRequest.CreateHttp(uri);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                request.UseDefaultCredentials = true;
                request.Credentials = CredentialCache.DefaultCredentials;
                request.Timeout = TIMEOUT_SECONDS * 1000;
                ServerCertificateHelper.ValidateServerCertificate();

                //realizar la petición
                using (HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync().ConfigureAwait(false)))
                {
                    //indica si se ha realizado o no el login
                    isLogged = response.StatusCode == HttpStatusCode.OK;
                }
                
                return isLogged;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[WSModel:Login]Error:" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Obtiene los datos solicitados
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public static async Task<T> Get<T>(string url, string body = "", bool isRecursive = false, bool isJsonParams = false, bool returnException = false)
        {    
            try
            {
                // inicializar variables
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                T result;

                //inicializar uri
                Uri uri = new Uri($"{Constants.Protocol}{AppConfig.Instance.Globals.Host}{url}");

                //inicializar handler
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    clientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                    //inicializar content
                    using (StringContent content = new StringContent(content: body, Encoding.UTF8))
                    {
                        content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded");

                        //cliente
                        using (HttpClient client = new HttpClient(handler: clientHandler))
                        {
                            client.Timeout = TimeSpan.FromSeconds(TIMEOUT_SECONDS);
                            ServerCertificateHelper.ValidateServerCertificate();

                            //realizamos la petición
                            using (HttpResponseMessage responseMessage = await client.PostAsync(requestUri: uri, content: content))
                            {
                                //si hemos recibido la respuesta correcta
                                if (responseMessage?.StatusCode == HttpStatusCode.OK)
                                {
                                    string responseString = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                                    if (!string.IsNullOrEmpty(responseString))
                                    {
                                        if (isJsonParams)
                                        {
                                            NemesysJsonReturnParamsModel nemesysJsonReturnParamsModel = JsonConvert.DeserializeObject<NemesysJsonReturnParamsModel>(responseString);
                                            string jsonParams = nemesysJsonReturnParamsModel?.DataJson?.Table1?.FirstOrDefault().JsonParams;
                                            result = !string.IsNullOrEmpty(jsonParams) ? JsonConvert.DeserializeObject<T>(jsonParams) : default;
                                        }
                                        else
                                        {
                                            NemesysJsonModel nemesysJsonModel = JsonConvert.DeserializeObject<NemesysJsonModel>(responseString);
                                            result = JsonConvert.DeserializeObject<T>(nemesysJsonModel.Data);
                                        }

                                        return result;
                                    }
                                    else
                                    {
                                        throw new Exception($"[{timestamp}] Content:{responseMessage?.Content} ResponseString:{responseString}");
                                    }
                                }
                                //si no está autorizado, reintentar una vez
                                else if (responseMessage?.StatusCode == HttpStatusCode.Unauthorized && !isRecursive)
                                {
                                    //loguearse
                                    await Login(user: AppConfig.Instance.Globals.LoginUser.Usuario, password: AppConfig.Instance.Globals.LoginUser.Password);

                                    //volver a intentar la petición
                                    return await Get<T>(url: url, body: body, isRecursive: true);
                                }
                                //si no hemos recibido la respuesta correcta
                                else
                                {
                                    throw new Exception($"[{timestamp}]StatusCode:{responseMessage?.StatusCode} Content:{responseMessage?.Content}");
                                }
                            }                          
                        }                
                    }                
                }                 
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[WSModel:Get][Url:({url}),Body:({body})]Error:" + ex.Message);

                if(returnException)
                    throw new Exception($"[WSModel.Get] Exception: {ex.Message}");
                else
                    return default;
            }
        }

        /// <summary>
        /// Envía los datos
        /// </summary>
        /// <param name="url"></param>
        /// <param name="body"></param>
        /// <param name="isRecursive"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> Post(string url, string body = "", bool isRecursive = false)
        {
            try
            {
                // inicializar variables
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

                //inicializar uri
                Uri uri = new Uri($"{Constants.Protocol}{AppConfig.Instance.Globals.Host}{url}");

                //inicializar handler
                using (HttpClientHandler clientHandler = new HttpClientHandler())
                {
                    clientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                    //inicializar content
                    using (StringContent content = new StringContent(content: body, Encoding.UTF8))
                    {
                        content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");

                        //cliente
                        using (HttpClient client = new HttpClient(handler: clientHandler))
                        {
                            client.Timeout = TimeSpan.FromSeconds(TIMEOUT_SECONDS);
                            ServerCertificateHelper.ValidateServerCertificate();

                            //realizamos la petición
                            using (HttpResponseMessage responseMessage = await client.PostAsync(requestUri: uri, content: content))
                            {
                                //si no está autorizado, reintentar una vez
                                if (responseMessage?.StatusCode == HttpStatusCode.Unauthorized && !isRecursive)
                                {
                                    //loguearse
                                    await Login(user: AppConfig.Instance.Globals.LoginUser.Usuario, password: AppConfig.Instance.Globals.LoginUser.Password);

                                    //volver a intentar la petición
                                    return await Post(url: url, body: body, isRecursive: true);
                                }
                                //si está autorizado, devolver la respuesta
                                else
                                {
                                    return responseMessage;
                                }
                            }                          
                        }                
                    }                  
                }    
            }
            catch (Exception pex)
            {
                Debug.WriteLine($"[WSModel:Post][Url:({url}),Body:({body})]Error:" + pex.Message);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Generar url
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="functionName"></param>
        /// <returns></returns>
        public static string GenerateUrl(string controllerName, string functionName)
        {
            return $"/Nemesys/file/{controllerName}/{functionName}";
        }

        /// <summary>
        /// Generar url con body
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="functionName"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public static string GenerateUrlWithBody(string controllerName, string functionName, out string body)
        {
            body = $"Controlador={controllerName}&Funcion={functionName}";
            return $"/Nemesys/file/ws";
        }

        /// <summary>
        /// Añadir parámetros al body
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string AddParamsToBody(params string[] param)
        {
            string allParams = string.Empty;
            foreach (string item in param)
            {
                if (string.IsNullOrEmpty(item))
                    continue;

                if (string.IsNullOrEmpty(allParams))
                    allParams = item;
                else
                {
                    if (!allParams.EndsWith("&"))
                        allParams += "&";
                    allParams += item;
                }
            }
            return allParams;
        }
    }

    public class WSAlbaranes
    {
        public static string ControllerNameWS => "Albaranes";

        /// <summary>
        /// Obtener listado de cabecera de albaranes del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListAlbaranCbWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetAlbaranesCb");
            return url;
        }

        /// <summary>
        /// Obtener sincro para contador de albaranesCb
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorAlbaranesCb(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorAlbaranesCb", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de albaranesCb
        /// </summary>
        /// <returns></returns>
        public static string SincroListAlbaranesCb(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListAlbaranesCb", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para contador de albaranesLn
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorAlbaranesLn(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorAlbaranesLn", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de albaranesLn
        /// </summary>
        /// <returns></returns>
        public static string SincroListAlbaranesLn(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListAlbaranesLn", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para contador de albaranesCb originales
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorAlbaranesCbOriginales(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorAlbaranesCbOriginales", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de albaranesCb originales
        /// </summary>
        /// <returns></returns>
        public static string SincroListAlbaranesCbOriginales(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListAlbaranesCbOriginales", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para contador de albaranesLn
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorAlbaranesLnOriginales(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorAlbaranesLnOriginales", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de albaranesLn
        /// </summary>
        /// <returns></returns>
        public static string SincroListAlbaranesLnOriginales(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListAlbaranesLnOriginales", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }

        /// <summary>
        /// Obtener listado de cabecera de albaranes del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListAlbaranLnWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetAlbaranesLn");
            return url;
        }

        /// <summary>
        /// Insertar cabecera albarán al WS
        /// </summary>
        /// <returns></returns>
        public static string InsertAlbaranCbWS(AlbaranCbModel albaranCb, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "InsertarAlbaranCbTest", out body);
            body = WSModel.AddParamsToBody(body, $"{JsonConvert.SerializeObject(albaranCb)}");
            return url;
        }

        /// <summary>
        /// Insertar listado de líneas albarán al WS
        /// </summary>
        /// <returns></returns>
        public static string InsertListAlbaranLnWS(List<AlbaranLnModel> listAlbaranLn, out string body)
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "InsertarAlbaranLnTest");
            body = JsonConvert.SerializeObject(listAlbaranLn);
            return url;
        }

        /// <summary>
        /// Obtener último número albarán por dispositivo y año
        /// </summary>
        /// <returns></returns>
        public static string GetUltimoNumAlbaranPorDispositivoYAnio(string dispositivo, int anio, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "GetUltimoNumAlbaranPorDispositivoYAnio", out body);
            body = WSModel.AddParamsToBody(body, $"Dispositivo={dispositivo}&Anio={anio}");
            return url;
        }
    }

    public class WSUsuario
    {
        public static string ControllerNameWS => "Usuarios";

        /// <summary>
        /// Obtener sincro para contador de albaranesLn
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorUsuarios(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorUsuarios", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de albaranesLn
        /// </summary>
        /// <returns></returns>
        public static string SincroListUsuarios(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListUsuarios", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSCliente
    {
        public static string ControllerNameWS => "Clientes";

        /// <summary>
        /// Obtener sincro para contador de clientes
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorClientes(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorClientes", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de clientes
        /// </summary>
        /// <returns></returns>
        public static string SincroListClientes(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListClientes", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSFormasPago
    {
        public static string ControllerNameWS => "FormasPago";

        /// <summary>
        /// Obtener sincro para contador de formas de pago
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorFormasPago(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorFormasPago", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de formas de pago
        /// </summary>
        /// <returns></returns>
        public static string SincroListFormasPago(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListFormasPago", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSEntidades
    {
        public static string ControllerNameWS => "Entidades";

        /// <summary>
        /// Obtener sincro para contador de entidades
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorEntidades(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorEntidades", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de entidades
        /// </summary>
        /// <returns></returns>
        public static string SincroListEntidades(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListEntidades", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSProductos
    {
        public static string ControllerNameWS => "Productos";

        /// <summary>
        /// Obtener sincro para contador de productos
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorProductos(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorProductos", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de productos
        /// </summary>
        /// <returns></returns>
        public static string SincroListProductos(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListProductos", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSLotes
    {
        public static string ControllerNameWS => "Lotes";

        /// <summary>
        /// Obtener listado de lotes del WS
        /// </summary>
        /// <returns></returns>
        public static string GetListLotsWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetLotes");

            return url;
        }

        /// <summary>
        /// Obtener sincro para contador de lotes
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorLotes(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorLotes", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de lotes
        /// </summary>
        /// <returns></returns>
        public static string SincroListLotes(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListLotes", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSUnidadesMedida
    {
        public static string ControllerNameWS => "UnidadesMedida";

        /// <summary>
        /// Obtener sincro para contador de unidades de medida
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorUnidadesMedida(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorUnidadesMedida", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de unidades de medida
        /// </summary>
        /// <returns></returns>
        public static string SincroListUnidadesMedida(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListUnidadesMedida", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSTipoIva
    {
        public static string ControllerNameWS => "TipoIva";

        /// <summary>
        /// Obtener listado de tipo de iva del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListTipoIvaWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetTipoIva");
            return url;
        }

        /// <summary>
        /// Obtener sincro para contador de tipo de iva
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorTipoIva(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorTipoIva", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de tipo de iva
        /// </summary>
        /// <returns></returns>
        public static string SincroListTipoIva(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListTipoIva", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSRecargoEquivalencia
    {
        public static string ControllerNameWS => "RecargoEquivalencia";

        /// <summary>
        /// Obtener listado de recargos del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListRecargoWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetRecargoEquivalencia");
            return url;
        }

        /// <summary>
        /// Obtener sincro para contador de recargo de equivalencia
        /// </summary>
        /// <returns></returns>
        public static string SincroContadorRecargoEquivalencia(string tagSincro, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroContadorRecargoEquivalencia", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}");
            return url;
        }

        /// <summary>
        /// Obtener sincro para listado de recargo de equivalencia
        /// </summary>
        /// <returns></returns>
        public static string SincroListRecargoEquivalencia(string tagSincro, int numRegistros, out string body)
        {
            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "SincroListRecargoEquivalencia", out body);
            body = WSModel.AddParamsToBody(body, $"TagSincro={tagSincro}&NumRegistros={numRegistros}");
            return url;
        }
    }

    public class WSFuncionesGIntegracion
    {
        public static string ControllerNameWS => "FuncionesGIntegracionUrdaira";

        /// <summary>
        /// Insertar albarán
        /// </summary>
        /// <returns></returns>
        public static string InsertAlbaranYLineas(string dispositivo, string fechaFactura, string jsonParam, bool facturar, out string body)
        {
            string facturarStr = facturar ? "1" : "0";
            string bbdd = AppConfig.Instance.Globals.BBDD;

            string url = WSModel.GenerateUrlWithBody(ControllerNameWS, "DataDB_PeticionesWS_Urdaira", out body);
            body = WSModel.AddParamsToBody(body, $"TipoPeticionWS=InsertarAlbaran&Facturar={facturarStr}&SerieFactura={dispositivo}" +
                $"&FechaFactura={fechaFactura}&TBai={facturarStr}&ODOO-DB={bbdd}&JSonParams={jsonParam}");
            return url;
        }

    }
}
