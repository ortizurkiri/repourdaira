﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class RecargoEquivalenciaModel
    {
        [JsonIgnore]
        public const string TableName = "RecargoEquivalencia";

        [PrimaryKey]
        [DefaultValue(0)]
        public double Recargo { get; set; } = 0;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public RecargoEquivalenciaModel()
        {

        }

        /// <summary>
        /// Insertar o actualizar recargo
        /// </summary>
        /// <param name="listUnit"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListSurchargeBBDD(List<RecargoEquivalenciaModel> listRecargo)
        {
            try
            {
                if (listRecargo?.Count > 0)
                {
                    foreach (RecargoEquivalenciaModel recargo in listRecargo)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(recargo).ConfigureAwait(false);
                        Debug.WriteLine($"[RecargoEquivalenciaModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[RecargoEquivalenciaModel:InsertOrUpdateListSurchargeBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de recargo
        /// </summary>
        /// <returns></returns>
        public static async Task<List<RecargoEquivalenciaModel>> GetListSurchargeBBDD()
        {
            try
            {
                List<RecargoEquivalenciaModel> listSurcharge = null;

                listSurcharge = await AppConfig.Instance.Database.GetListItemsAsync<RecargoEquivalenciaModel>().ConfigureAwait(false);

                return listSurcharge;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[RecargoEquivalenciaModel:GetListSurchargeBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvío del tipo de iva
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioRecargoEquivalenciaBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<RecargoEquivalenciaModel> listItems = null;
                RecargoEquivalenciaModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<RecargoEquivalenciaModel>(query).ConfigureAwait(false);

                //item
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[RecargoEquivalenciaModel:GetLastTagReenvioRecargoEquivalenciaBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
