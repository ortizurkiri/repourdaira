﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class FormasPagoModel
    {
        [JsonIgnore]
        public const string TableName = "FormasPago";

        public static string ControllerNameWS => "FormasPago";

        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [DefaultValue(0)]
        public int IdFPago { get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Descripcion { get; set; } = string.Empty;

        [MaxLength(10)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;


        /// <summary>
        /// Obtener listado de formas de pago del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListWayToPayWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetFormasPago");

            return url;
        }

        /// <summary>
        /// Insertar o actualizar formas de pago
        /// </summary>
        /// <param name="listWayToPay"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListWayToPayBBDD(List<FormasPagoModel> listWayToPay)
        {
            try
            {
                if (listWayToPay?.Count > 0)
                {
                    foreach (FormasPagoModel wayToPay in listWayToPay)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(wayToPay).ConfigureAwait(false);
                        Debug.WriteLine($"[FormasPagoModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[FormasPagoModel:InsertOrUpdateListWayToPayBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de formas de pago
        /// </summary>
        /// <returns></returns>
        public static async Task<List<FormasPagoModel>> GetListListWayToPayBBDD()
        {
            try
            {
                List<FormasPagoModel> listWayToPay = null;

                listWayToPay = await AppConfig.Instance.Database.GetListItemsAsync<FormasPagoModel>().ConfigureAwait(false);

                return listWayToPay;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[FormasPagoModel:GetListListWayToPayBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvío de las formas de pago
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioWayToPayBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<FormasPagoModel> listItems = null;
                FormasPagoModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<FormasPagoModel>(query).ConfigureAwait(false);

                //item
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[FormasPagoModel:GetLastTagReenvioWayToPayBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
