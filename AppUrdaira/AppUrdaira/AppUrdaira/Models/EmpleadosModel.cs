﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AppUrdaira.Models
{
    [Table("Empleados")]
    public class EmpleadosModel
    {
        [PrimaryKey]
        [DefaultValue(0)]
        public int IdEmpleado { get; set; } = 0;

        [MaxLength(100)]
        [DefaultValue("")]
        public string Apellidos { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string Nombre { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string Cargo { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string Tratamiento { get; set; } = string.Empty;

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string FechaNacimiento { get; set; } = "00000000";

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string FechaContratacin { get; set; } = "00000000";

        [MaxLength(200)]
        [DefaultValue("")]
        public string Direccin { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Ciudad { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Regin { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string CdPostal { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string Pas { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string TelDomicilio { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Extensin { get; set; } = string.Empty;

        [DefaultValue("")]
        public string Foto { get; set; } = string.Empty;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Notas { get; set; } = string.Empty;

        [DefaultValue(0)]
        public int Jefe { get; set; } = 0;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;
    }
}
