﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppUrdaira.Models
{
    public class DeviceModel
    {
        public string RowId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string DireccionMac { get; set; }
        public string DireccionIp { get; set; }
        public string PinCode { get; set; }
        public string Centro { get; set; }
        public string CodigoTpv { get; set; }
        public string TipoDispositivo { get; set; }
        public string TipoConexion { get; set; }
        public bool EnUso { get; set; }
        public long TagPropio { get; set; }
        public long TagReenvio { get; set; }
        public long TagBorrado { get; set; }
        public string VersionApp { get; set; }
        public string UUIDMovil { get; set; }
        public short Rssi { get; set; }
    }
}
