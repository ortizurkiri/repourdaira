﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace AppUrdaira.Models
{
    public class DateNumDeliveryNoteModel
    {
        #region Propiedades
        public DateTime DateDeliveryNote { get; set; } = DateTime.Now;
        public string NumDeliveryNote { get; set; } = "0";
        #endregion

        /// <summary>
        /// Contructor por defecto
        /// </summary>
        public DateNumDeliveryNoteModel()
        {

        }

        /// <summary>
        /// Contructor por parámetros
        /// </summary>
        public DateNumDeliveryNoteModel(DateTime dateDeliveryNote, string numDeliveryNote)
        {
            try
            {
                DateDeliveryNote = dateDeliveryNote;
                NumDeliveryNote = numDeliveryNote;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DateNumDeliveryNoteModel:()]Error:" + ex.Message);
            }
        }
    }
}
