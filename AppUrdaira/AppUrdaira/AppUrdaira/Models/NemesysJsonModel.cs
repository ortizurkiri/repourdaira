﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppUrdaira.Models
{
    public class NemesysJsonModel
    {
        public string Data { get; set; }
        public string DataSerializationMode { get; set; }
        public string DataTypeName { get; set; }
        public string DataMicrosoftAssemblyQualifiedName { get; set; }
    }
}
