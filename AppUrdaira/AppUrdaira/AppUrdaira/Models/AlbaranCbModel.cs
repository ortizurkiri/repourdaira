﻿using AppUrdaira.Converter;
using AppUrdaira.Utilities;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class AlbaranCbModel : ObservableDbObjectModel
    {
        [JsonIgnore]
        public const string TableName = "AlbaranCb";

        [DefaultValue("")]
        [PrimaryKey]
        public string PK 
        { 
            get { return GetPK(); }
            set { } 
        }

        public new string RowId { get; set; } = Generators.RowIdGeneratorLocal();


        [DefaultValue("0000000000")]
        public string FechaRowId { get; set; } = Generators.GetDefaultDate();


        [DefaultValue("0000000000")]
        public string FechaAlbaran { get; set; } = Generators.GetDefaultDate();

        [DefaultValue("")]
        public string NAlbaran { get; set; } = string.Empty;

        [DefaultValue(0)]
        public int IdCliente { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string FormaPago { get; set; } = string.Empty;

        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Generators.GetDefaultDate();

        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Generators.GetDefaultTime();

        [MaxLength(200)]
        [DefaultValue("")]
        public string Cliente { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string SerieFactura { get; set; } = string.Empty;

        [DefaultValue("0000000000")]
        public string FechaFactura { get; set; } = Generators.GetDefaultDate();

        [MaxLength(50)]
        [DefaultValue("")]
        public string Factura { get; set; } = string.Empty;

        [JsonConverter(typeof(BoolConverter))]
        [DefaultValue(false)]
        public bool Cerrado { get; set; } = false;

        [DefaultValue(0)]
        public int NPedido { get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Entidad { get; set; } = string.Empty;

        [JsonConverter(typeof(BoolConverter))]
        [DefaultValue(false)]
        public bool Cobrado { get; set; } = false;

        [DefaultValue(0)]
        public double Recargo { get; set; } = 0;

        [DefaultValue(0)]
        public double Rappel { get; set; } = 0;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Dispositivo { get; set; } = string.Empty;

        [JsonIgnore]
        [DefaultValue(0)]
        public long TagEnviado { get; set; } = 0;

        [DefaultValue(false)]
        public bool EsSincro { get; set; } = false;

        [Ignore]
        public List<AlbaranLnModel> LineasAlbaran { get; set; } = null;

        [JsonIgnore]
        public string TBaiUrl { get; set; } = string.Empty;

        [JsonIgnore]
        public string TBaiId { get; set; } = string.Empty;

        [JsonIgnore]
        public bool Guardado { get; set; } = false;

        [JsonIgnore]
        public Guid GuidGenericModel { get; set; }

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AlbaranCbModel()
        {

        }


        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        public AlbaranCbModel(DateTime fecha, string nAlbaran, int idCliente, string formaPago, 
            string fechaRegistro, string horaRegistro, string cliente, string serieFactura, string fechaFactura, string factura, bool cerrado, int nPedido, 
            string entidad, bool cobrado, double recargo, double rappel, long tagPropio, long tagReenvio, long tagBorrado, string dispositivo,
            long tagEnviado, Guid guidGenericModel)
        {
            try
            {
                FechaAlbaran = fecha.ToString("dd/MM/yyyy");
                FechaRowId = fecha.ToString("yyyyMMdd");
                NAlbaran = nAlbaran;
                UpdateRowId();
                IdCliente = idCliente;
                FormaPago = formaPago;
                FechaRegistro = fechaRegistro;
                HoraRegistro = horaRegistro;
                Cliente = cliente;
                SerieFactura = serieFactura;
                FechaFactura = fechaFactura;
                Factura = factura;
                Cerrado = cerrado;
                NPedido = nPedido;
                Entidad = entidad;
                Cobrado = cobrado;
                Recargo = recargo;
                Rappel = rappel;
                TagPropio = tagPropio;
                TagReenvio = tagReenvio;
                TagBorrado = tagBorrado;
                Dispositivo = dispositivo;
                TagEnviado = tagEnviado;
                GuidGenericModel = guidGenericModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:()2]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Obtener objeto actual
        /// </summary>
        /// <returns></returns>
        public AlbaranCbModel GetThisAlbaranCbModel()
        {
            try
            {
                return this;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetThisAlbaranCbModel]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener objeto json en string
        /// </summary>
        /// <returns></returns>
        public string ToJSonString()
        {
            try
            {
                string json = JsonConvert.SerializeObject(this);
                //return HttpUtility.UrlEncode(json);
                return json;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:ToJSonString]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener número de albarán
        /// </summary>
        /// <returns></returns>
        public string GetNumDeliveryNote()
        {
            string numDeliveryNote = NAlbaran;
            try
            {
                if (!string.IsNullOrEmpty(NAlbaran) && NAlbaran.Split('.')?.Length > 0)
                    numDeliveryNote = NAlbaran.Split('.').LastOrDefault();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetNumDeliveryNote]Error: " + ex.Message);
            }
            return numDeliveryNote;
        }

        /// <summary>
        /// Obtener fecha de albarán
        /// </summary>
        /// <returns></returns>
        public DateTime GetDateDeliveryNote()
        {
            DateTime dateTimeDeliveryNote = DateTime.Now;
            try
            {
                if (!string.IsNullOrEmpty(FechaAlbaran))
                    DateTime.TryParseExact(s: FechaAlbaran, format: "dd/MM/yyyy", provider: CultureInfo.InvariantCulture, style: DateTimeStyles.None, result: out dateTimeDeliveryNote);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetDateTimeDeliveryNote]Error: " + ex.Message);
            }
            return dateTimeDeliveryNote;
        }

        /// <summary>
        /// Obtener PK
        /// </summary>
        /// <returns></returns>
        public string GetPK()
        {
            try
            {
                return EsSincro ? $"{NAlbaran}.{EsSincro}" : RowId;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetPK]Error: " + ex.Message);
                return null;
            }
        }

        public void UpdateRowId()
        {
            try
            {
                RowId = Generators.RowIdGeneratorCentral(fecha: FechaRowId, numAlbaran: NAlbaran);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:UpdateRowId]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Insertar o actualizar cabecera de albarán
        /// </summary>
        /// <returns></returns>
        public static async Task InsertOrUpdateAlbaranCbModelBBDD(AlbaranCbModel deliveryNote)
        {
            try
            {
                if (deliveryNote != null)
                {
                    var result = await AppConfig.Instance.Database.SaveItemAsync(deliveryNote).ConfigureAwait(false);
                    Debug.WriteLine($"[AlbaranCbModel:SaveItemAsync] Result: {result}");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:InsertOrUpdateAlbaranCbModelBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Insertar o actualizar lista de cabecera de albarán
        /// </summary>
        /// <param name="listdeliveryNote"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListAlbaranCbModelBBDD(List<AlbaranCbModel> listdeliveryNote, string tableName = AlbaranCbModel.TableName)
        {
            try
            {
                string query = string.Empty;
                List<AlbaranCbModel> listTemp = null;
                int range = 100, rangeList = 0;

                if (listdeliveryNote?.Count > 0)
                {
                    do
                    {
                        // coger los primeros 100 registros o los registros restantes
                        rangeList = Math.Min(listdeliveryNote.Count, range);
                        listTemp = listdeliveryNote.GetRange(0, rangeList);

                        if (listTemp?.Count > 0)
                        {
                            // rellenar cabecera de insert
                            query = $@" INSERT OR REPLACE INTO {tableName} 
                            (
                                RowId, PK, FechaAlbaran, NAlbaran, IdCliente, FormaPago, FechaRegistro, HoraRegistro, 
                                Cliente, SerieFactura, FechaFactura, Factura, Cerrado, NPedido, Entidad, Cobrado, Recargo, Rappel, 
                                TagPropio, TagReenvio, TagBorrado, Dispositivo, TagEnviado, EsSincro, GuidGenericModel
                            ) 
                            VALUES ";

                            //rellenar datos de la insert
                            for (int i = 0; i < listTemp.Count; i++)
                            {
                                AlbaranCbModel deliveryNote = listTemp[i];
                                query += $@"{(i != 0 ? "," : "")}
                                    ('{deliveryNote.RowId}'
                                    ,'{deliveryNote.PK}'
                                    ,'{deliveryNote.FechaAlbaran}'
                                    ,'{deliveryNote.NAlbaran}'
                                    , {deliveryNote.IdCliente}
                                    ,'{deliveryNote.FormaPago}'
                                    ,'{deliveryNote.FechaRegistro}'
                                    ,'{deliveryNote.HoraRegistro}'

                                    ,'{deliveryNote.Cliente}'
                                    ,'{deliveryNote.SerieFactura}'                                    
                                    ,'{deliveryNote.FechaFactura}'                                    
                                    ,'{deliveryNote.Factura}'
                                    ,'{(deliveryNote.Cerrado ? 1 : 0)}'
                                    , {deliveryNote.NPedido}
                                    ,'{deliveryNote.Entidad}'
                                    ,'{(deliveryNote.Cobrado ? 1 : 0)}'
                                    , {Generators.GetDoubleStrToBBDD(deliveryNote.Recargo)}
                                    , {Generators.GetDoubleStrToBBDD(deliveryNote.Rappel)}

                                    , {deliveryNote.TagPropio}
                                    , {deliveryNote.TagReenvio}
                                    , {deliveryNote.TagBorrado}
                                    ,'{deliveryNote.Dispositivo}'
                                    , {deliveryNote.TagEnviado}
                                    ,'{(deliveryNote.EsSincro ? 1 : 0)}'
                                    ,'{deliveryNote.GuidGenericModel}'
                                    )";
                            }

                            //ejecutar insert
                            var result = await AppConfig.Instance.Database.ExecuteQueryIntAsync(query).ConfigureAwait(false);

                            // eliminar primeros 100 registros
                            listdeliveryNote.RemoveRange(0, rangeList);
                        }
                    }
                    while (listdeliveryNote?.Count > 0);
                }          
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:InsertOrUpdateListAlbaranCbModelBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener albarán
        /// </summary>
        /// <returns></returns>
        public static async Task<AlbaranCbModel> GetDeliveryNoteByGuidBBDD(Guid guid)
        {
            try
            {
                //Iniciar variables
                AlbaranCbModel deliveryNote = null;
                string query = $"SELECT * FROM {TableName} WHERE EsSincro = 0 AND GuidGenericModel = '{guid}'";

                //consulta
                deliveryNote = (await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranCbModel>(query).ConfigureAwait(false))?.FirstOrDefault();

                //devolver objeto
                return deliveryNote;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetDeliveryNoteByGuidBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener listado de albaranes con dispositivo y fecha actual
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranCbModel>> GetListDeliveryNoteThisDeviceAndActualYear()
        {
            try
            {
                //Iniciar variables
                List<AlbaranCbModel> listDeliveryNote = null;
                string device = AppConfig.Instance.Globals.Device;
                string query = $@"
                    SELECT 
                        * 
                    FROM 
                        {TableName} 
                    WHERE 
                        EsSincro = 0 
                        AND TagBorrado = 0 
                        AND Dispositivo = '{device}'
                        AND NAlbaran like '{device}.{DateTime.Now:yyyy}.%'
                    ORDER BY
                        NAlbaran ASC
                    ";

                //consulta
                listDeliveryNote = (await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranCbModel>(query).ConfigureAwait(false));

                //devolver listado
                return listDeliveryNote;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetListDeliveryNoteThisDeviceAndActualYear]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener albaranes con TagEnviado a 0
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranCbModel>> GetListDeliveryNoteByTagEnviado0BBDD()
        {
            try
            {
                //Iniciar variables
                List<AlbaranCbModel> listDeliveryNote = null;
                string query = $@"
                    SELECT 
	                    a.* 
                    FROM 
	                    {TableName} a
		                    LEFT JOIN 
	                    {GenericModel.TableName} g
			                    ON g.GuidGenericModel = a.GuidGenericModel
                    WHERE 
	                    (a.GuidGenericModel = '{Guid.Empty}' OR g.Save = 1)
	                    AND a.TagEnviado = 0
                        AND EsSincro = 0
                ";

                //consulta
                listDeliveryNote = await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranCbModel>(query).ConfigureAwait(false);

                //devolver objeto
                return listDeliveryNote;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetListDeliveryNoteByTagEnviado0BBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último nº de albarán
        /// </summary>
        /// <returns></returns>
        public static async Task<DateNumDeliveryNoteModel> GetLastNumDeliverynoteBBDD()
        {
            try
            {
                //Iniciar variables
                AlbaranCbModel deliveryNote = null;
                string numAlbaran = "0".PadLeft(5, '0');
                DateTime yearDeliveryNote = DateTime.Now;
                string dispositivo = AppConfig.Instance.Globals.Device;
                string query = $@"
                                SELECT 
                                    * 
                                FROM 
                                    {TableName} 
                                WHERE 
                                    TagBorrado = 0
                                    AND EsSincro = 0 
                                    AND Dispositivo = '{dispositivo}' 
                                ORDER BY 
                                    NAlbaran DESC 
                                LIMIT 1";
                string querySincro = $@"
                                SELECT 
                                    * 
                                FROM 
                                    {TableName} 
                                WHERE
                                    TagBorrado = 0
                                    AND EsSincro = 1 
                                    AND Dispositivo = '{dispositivo}' 
                                ORDER BY 
                                    NAlbaran DESC 
                                LIMIT 1";

                //consulta
                deliveryNote = (await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranCbModel>(query).ConfigureAwait(false))?.FirstOrDefault();

                //si no hemos recuperado ningún albarán, probamos al último de la sincro
                if(deliveryNote == null)
                    deliveryNote = (await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranCbModel>(querySincro).ConfigureAwait(false))?.FirstOrDefault();

                //si recuperamos correctamente el albarán
                if (deliveryNote != null)
                {
                    //obtenemos el año
                    yearDeliveryNote = deliveryNote.GetDateDeliveryNote();

                    //obtener número
                    numAlbaran = deliveryNote.NAlbaran ?? "0".PadLeft(5, '0');
                }

                //devolver número
                return new DateNumDeliveryNoteModel(dateDeliveryNote: yearDeliveryNote, numDeliveryNote: numAlbaran);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetLastNumDeliverynoteBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener siguiente nº de albarán
        /// </summary>
        /// <returns></returns>
        public static async Task<string> GetNextNumDeliverynoteBBDD(string numAlbaranPorDefecto)
        {
            try
            {
                //Iniciar variables
                DateNumDeliveryNoteModel fechaYnumAlbaran = null;
                DateTime fechaAlbaran;
                string numAlbaranTempStr = null;
                string numAlbaranStr = null;
                int numAlbaran = 0;

                //obtener fecha y número
                fechaYnumAlbaran = await GetLastNumDeliverynoteBBDD().ConfigureAwait(false);

                // si tenemos fecha y número albarán
                if (fechaYnumAlbaran != null)
                {
                    //fecha del último albarán
                    fechaAlbaran = fechaYnumAlbaran.DateDeliveryNote;

                    //nº albarán desde el número compuesto
                    numAlbaranTempStr = fechaYnumAlbaran.NumDeliveryNote?.Split('.')?.LastOrDefault() ?? numAlbaranPorDefecto;

                    //obtener el nº de albarán según fecha
                    numAlbaran = fechaAlbaran.Year == DateTime.Now.Year ? Convert.ToInt32(numAlbaranTempStr) : 0;
                }
                // si no tenemos, convertir a númerico lo recibido por defecto
                else
                    numAlbaran = !string.IsNullOrEmpty(numAlbaranPorDefecto) ? Convert.ToInt32(numAlbaranPorDefecto) : 0;

                //obtener número siguiente
                numAlbaranStr = (numAlbaran + 1).ToString().PadLeft(5, '0');

                //devolver número
                return numAlbaranStr;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetNextNumDeliverynoteBBDD]Error: " + ex.Message);
                return "0";
            }
        }

        /// <summary>
        /// Eliminar albarán quitándole el guid
        /// </summary>
        /// <returns></returns>
        public static async Task<bool> DeleteDeliveryNoteByGuidBBDD(Guid guid)
        {
            try
            {
                //Iniciar variables
                AlbaranCbModel deliveryNote = null;
                int deliveryNoteDeleted = 0;

                //obtener albarán
                deliveryNote = await GetDeliveryNoteByGuidBBDD(guid);

                //si tenemos albarán
                if(deliveryNote != null)
                {
                    //vaciar guid
                    deliveryNote.GuidGenericModel = Guid.Empty;

                    //actualizar
                    deliveryNoteDeleted = await AppConfig.Instance.Database.SaveItemAsync(deliveryNote).ConfigureAwait(false);
                }

                //devolver resultado
                return deliveryNoteDeleted == 1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:DeleteDeliveryNoteByGuidBBDD]Error: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Obtener texto de cabecera de albarán
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetHeadDeliveryNote()
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                int lengthStr = 25;
                string textHead = string.Empty;
                string date = string.Empty;
                string hour = string.Empty;
                string number = string.Empty;
                string customerName = !string.IsNullOrEmpty(Cliente) 
                    ? (Cliente.Trim().Length > lengthStr ? Cliente.Trim().Substring(0, lengthStr - 1) + "..." : Cliente.Trim()) 
                    : string.Empty;
                string wayToPay = FormaPago ?? string.Empty;
                string rappel = $"{Rappel}".PadRight(5) + "%";
                string recharge = $"{Recargo}".PadRight(5) + "%";
                string pagado = Cobrado ? "PAGADO" : string.Empty;
                string serieFactura = !string.IsNullOrEmpty(SerieFactura) ? $"Serie factura:   {SerieFactura}\n" : string.Empty;
                string nFactura = !string.IsNullOrEmpty(Factura) ? $"Nº factura:      {Factura}\n" : string.Empty;
                string fechaFactura = !string.IsNullOrEmpty(FechaFactura) && FechaFactura != Generators.GetDefaultDate() 
                    ? $"Fecha factura:   {Generators.GetDateTimeStringByLongString(FechaFactura)}\n" 
                    : string.Empty;

                //obtener las valores necesarios para la cabecera
                date = FechaAlbaran;
                hour = HoraRegistro;
                number = NAlbaran;

                //rellenar cabecera
                textHead = $"Nº albarán:      {number}\n" +
                           $"Fecha albarán:   {date}  {hour}\n" +
                           $"Cliente:         {customerName}\n" +
                           $"Forma pago:      {wayToPay}\n" +
                           $"Rappel:          {rappel}\n" +
                           $"Recargo:         {recharge}\n" +
                           $"{pagado}\n" +
                           $"{serieFactura}" +
                           $"{nFactura}" +
                           $"{fechaFactura}";

                //rellenar objeto
                textPrinterModel = new TextPrinterModel(text: textHead);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetHeadDeliveryNote]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener cabecera totales de albarán
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetTotalsHeaderDeliveryNote()
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textTotals = string.Empty;


                //rellenar totales
                textTotals = $"SumaImp  Rappel  SubTotal  Recargo  Iva   TOTAL\n";

                //rellenar objeto
                textPrinterModel = new TextPrinterModel(text: textTotals, fontSize: Enums.Enums.FontSizeTextPrinterEnum.Bold);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetTotalsHeaderDeliveryNote]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener totales de albarán
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetTotalsDeliveryNote(double _sumaImporte, double _rappel, double _subTotal, double _recargo, double _iva, double _totalAlbaran)
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textTotals = string.Empty;
                string sumaImporte = string.Empty;
                string rappel = string.Empty;
                string subTotal = string.Empty;
                string recargo = string.Empty;
                string iva = string.Empty;
                string totalAlbaran = string.Empty;
                int lengthStr = 6;

                //obtener las valores necesarios para los totales
                sumaImporte = string.Format("{0:N2}", _sumaImporte).PadLeft(lengthStr);
                rappel = string.Format("{0:N2}", _rappel).PadLeft(7);
                subTotal = string.Format("{0:N2}", _subTotal).PadLeft(lengthStr);
                recargo = string.Format("{0:N2}", _recargo).PadLeft(lengthStr);
                iva = string.Format("{0:N2}", _iva).PadLeft(lengthStr);
                totalAlbaran = string.Format("{0:N2}", _totalAlbaran).PadLeft(lengthStr);

                //rellenar totales
                textTotals = $"{sumaImporte} {rappel}   {subTotal}   {recargo} {iva}  {totalAlbaran}\n";

                //rellenar objeto
                textPrinterModel = new TextPrinterModel(text: textTotals);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetTotalsDeliveryNote]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto para imprimir de pagado de albarán
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetChargedDeliveryNote()
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textCharged = string.Empty;

                //rellenar pagado
                textCharged = $"{(Cobrado ? "PAGADO" : string.Empty)}\n";

                //rellenar objeto
                textPrinterModel = new TextPrinterModel(text: textCharged);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetChargedDeliveryNote]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto para imprimir el id del QR de la factura del albarán
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetIdQRTBaiDeliveryNote()
        {
            try
            {
                return new TextPrinterModel(
                    text: $"{TBaiId}\n", 
                    fontSize: Enums.Enums.FontSizeTextPrinterEnum.Default, 
                    _justifyText: Enums.Enums.JustifyTextPrinterEnum.Center
                );
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetIdQRTBaiDeliveryNote]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto para imprimir qr de la factura del albarán
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetQRTBaiDeliveryNote()
        {
            try
            {
                return new TextPrinterModel(
                    text: TBaiUrl, 
                    _justifyText: Enums.Enums.JustifyTextPrinterEnum.Center, 
                    isQR: true
                );
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetQRTBaiDeliveryNote]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener cliente de albarán
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetCustomerDeliveryNote(ClientesModel customer)
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textCustomer = string.Empty;
                string customerName = string.Empty;
                string customerCif = string.Empty;
                string customerPhone = string.Empty;

                int lengthStr = 25;

                if (!string.IsNullOrEmpty(Cliente) && customer != null)
                {
                    //obtener las valores necesarios para los datos del cliente
                    customerName = Cliente.Length > lengthStr ? Cliente.Substring(0, lengthStr - 1) + "..." : Cliente;
                    customerName = customerName.Trim();
                    customerCif = customer.Cif;
                    customerPhone = customer.Telefono1;

                    //rellenar cliente
                    textCustomer = "------------------- CLIENTE -----------------\n" +
                                    $" Nombre:   {customerName}\n" +
                                    $" Cif:      {customerCif}\n" +
                                    $" Teléfono: {customerPhone}\n"
                     ;
                }

                //rellenar objeto
                textPrinterModel = new TextPrinterModel(text: textCustomer);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetTotalsDeliveryNote]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvio de los albaranes
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioDeliveryNoteBBDD(string tableName = AlbaranCbModel.TableName)
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<AlbaranCbModel> listDeliveryNote = null;
                AlbaranCbModel deliveryNote = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {tableName} 
                    WHERE 
                        EsSincro = 1
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listDeliveryNote = await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranCbModel>(query).ConfigureAwait(false);

                //albarán
                deliveryNote = listDeliveryNote?.FirstOrDefault();

                //tagreenvio
                tagReenvio = deliveryNote != null ? deliveryNote.TagReenvio : 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetLastTagReenvioDeliveryNoteBBDD]Error: " + ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// Obtener último número albarán según sincro
        /// </summary>
        /// <returns></returns>
        public static async Task<int> GetUltimoNumAlbaranPorDispositivoYAnioSincro(string tableName = AlbaranCbModel.TableName)
        {
            try
            {
                //Iniciar variables
                int ultimoNumAlbaran = 0;
                string query = $@" 
                    SELECT COALESCE((
				      SELECT 
					       CAST(SUBSTR(NAlbaran, -5) AS Int) 
				      FROM 
					       {tableName}     
				      WHERE
					       TagBorrado = 0  
                           AND EsSincro = 1
					       AND Dispositivo = '{AppConfig.Instance.Globals.Device}'
					       AND SUBSTR(NAlbaran,-6,-4) = '{DateTime.Now.Date.Year}'    
				      ORDER BY
						    NAlbaran DESC      
				      LIMIT 1
				    ),0) AS NAlbaranInt 
                ";

                //consulta
                ultimoNumAlbaran = await AppConfig.Instance.Database.ExecuteQueryIntAsync(query).ConfigureAwait(false);

                //devolver valor
                return ultimoNumAlbaran;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetUltimoNumAlbaranPorDispositivoYAnioSincro]Error: " + ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Obtener sincro de cabecera de albaranes
        /// </summary>
        /// <returns></returns>
        public static async Task<int> GetUltimoNumAlbaranPorDispositivoYAnioWS()
        {
            try
            {
                //consultar último número albarán
                int ultimoNumAlbaran = await WSModel.Get<int>(
                    url: WSAlbaranes.GetUltimoNumAlbaranPorDispositivoYAnio(
                        dispositivo: AppConfig.Instance.Globals.Device,
                        anio: DateTime.Now.Date.Year,
                        body: out string body),
                    body: body,
                    returnException: true);

                //devolver valor
                return ultimoNumAlbaran;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetUltimoNumAlbaranPorDispositivoYAnio]Error: " + ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Comprobar nº albarán según WS o SQLite
        /// </summary>
        /// <returns></returns>
        public static async Task<string> ComprobarNumAlbaranSegunNAlbaranWSYSQLite()
        {
            try
            {
                // inicializar variable
                string mensajeError = string.Empty;

                // consultar último nº albarán en local
                int ultimoNAlbaranLocal = await GetUltimoNumAlbaranPorDispositivoYAnioSincro().ConfigureAwait(false);

                // consultar último nº albarán en remoto
                int ultimoNAlbaranRemoto = await GetUltimoNumAlbaranPorDispositivoYAnioWS().ConfigureAwait(false);

                if (ultimoNAlbaranLocal == -1)
                    mensajeError = "El número de albarán no se ha podido recuperar de local";
                else if (ultimoNAlbaranRemoto == -1)
                    mensajeError = "El número de albarán no se ha podido recuperar de remoto";
                else if (ultimoNAlbaranLocal != ultimoNAlbaranRemoto)
                    mensajeError = $"El número de albarán local ({ultimoNAlbaranLocal}) es distinto al remoto ({ultimoNAlbaranRemoto})";

                // devolver mensaje
                return mensajeError;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:ComprobarNumAlbaranSegunNAlbaranWSYSQLite]Error: " + ex.Message);
                return "Error al comprobar número de albarán en local vs remoto";
            }
        }

        /// <summary>
        /// Insertar o actualizar cabecera de albarán en ws
        /// </summary>
        /// <returns></returns>
        public static async Task<ParametrosInvoiceCreateOutModel> InsertAlbaranYLineasWS(AlbaranCbModel deliveryNote)
        {
            try
            {
                // inicializar parámetros
                ParametrosInvoiceCreateOutModel parametrosInvoiceCreateOut = null;

                if (deliveryNote != null)
                {
                    parametrosInvoiceCreateOut = await WSModel.Get<ParametrosInvoiceCreateOutModel>(
                        url: WSFuncionesGIntegracion.InsertAlbaranYLineas( 
                            dispositivo: AppConfig.Instance.Globals.Device,
                            fechaFactura: deliveryNote.FechaAlbaran,
                            jsonParam: deliveryNote.ToJSonString(),
                            facturar: deliveryNote.Cobrado,
                            body: out string body),
                        body: body,
                        isJsonParams: true
                    );

                    Debug.WriteLine($"[AlbaranCbModel:InsertAlbaranYLineasWS]parametrosInvoiceCreateOut: {parametrosInvoiceCreateOut?.ToJSonString()}");
                }

                return parametrosInvoiceCreateOut;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:InsertAlbaranYLineasWS]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtenemos listado de líneas de albarán de la cabecera de un albarán a partir de listado genérico
        /// </summary>
        /// <param name="listDeliveryNoteLine"></param>
        /// <returns></returns>
        public List<AlbaranLnModel> GetListDeliveryNoteLineByGeneralListToSend(List<AlbaranLnModel> listDeliveryNoteLine)
        {
            try
            {
                // devolvemos listado de líneas de un albarán, actualizando el TagReenvio de sus líneas
                return listDeliveryNoteLine
                    ?.Where(w => w.NAlbaran == NAlbaran)
                    ?.Select(s => { s.TagReenvio = TagReenvio; return s; })
                    ?.ToList();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetListDeliveryNoteLineByGeneralListToSend]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtenemos listado de líneas de albarán de la cabecera de un albarán para guardar cambios en BBDD
        /// </summary>
        /// <returns></returns>
        private List<AlbaranLnModel> GetListDeliveryNoteLineToSaveBBDD()
        {
            try
            {
                // devolvemos listado de líneas de un albarán, actualizando el TagReenvio de sus líneas
                return LineasAlbaran
                    ?.Select(s => 
                    {
                        s.TagEnviado = TagEnviado;
                        return s; 
                    })
                    ?.ToList();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:GetListDeliveryNoteLineToSaveBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Guardar cambios en BBDD
        /// </summary>
        /// <returns></returns>
        public async Task SaveToBBDD(bool updateTagEnviado = false)
        {
            try
            {
                // actualizar TagEnviado 
                if(updateTagEnviado)
                    TagEnviado = Generators.TagGenerator();
               
                // guardar cabecera
                await InsertOrUpdateAlbaranCbModelBBDD(this);

                // guardar líneas en BBDD
                await AlbaranLnModel.InsertOrUpdateListaAlbaranLnModelBBDD(GetListDeliveryNoteLineToSaveBBDD());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:SaveToBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Actualizar campos para enviar
        /// </summary>
        public void UpdateFieldsToSend()
        {
            try
            {
                FechaRegistro = DateTime.Now.ToString("dd/MM/yyyy");
                HoraRegistro = DateTime.Now.ToString("HH:mm:ss");
                TagEnviado = 0;
                TagReenvio = Generators.TagGenerator();
                TBaiId = TBaiUrl = Factura = SerieFactura = string.Empty;
                FechaFactura = Generators.GetDefaultDate();
                Cerrado = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:UpdateFieldsToSend]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Actualizar listado de líneas con los campos para enviar
        /// </summary>
        public void UpdateListDeliveryNoteLineToSend(List<AlbaranLnModel> listDeliveryNoteLn, bool updateDelete = false)
        {
            try
            {
                LineasAlbaran = listDeliveryNoteLn?.Select((x,i) =>
                {
                    x.FechaRegistro = FechaRegistro;
                    x.HoraRegistro = HoraRegistro;
                    x.TagEnviado = TagEnviado;
                    x.TagReenvio = TagReenvio;

                    if(updateDelete)
                        x.TagBorrado = TagBorrado;

                    return x;
                })?.ToList() ?? new List<AlbaranLnModel>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:UpdateListDeliveryNoteLineToSend]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Actualizar campos de factura
        /// </summary>
        public void UpdateFieldsByInvoice(ParametrosInvoiceCreateOutModel parametrosInvoiceCreateOut)
        {
            try
            {
                if(parametrosInvoiceCreateOut != null)
                {
                    TBaiId = parametrosInvoiceCreateOut.TBaiId;
                    TBaiUrl = parametrosInvoiceCreateOut.TBaiUrl;
                    SerieFactura = parametrosInvoiceCreateOut.Serie;
                    FechaFactura = parametrosInvoiceCreateOut.Fecha;
                    Factura = parametrosInvoiceCreateOut.NFactura;
                    Cerrado = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranCbModel:UpdateFieldsByInvoice]Error: " + ex.Message);
            }
        }
    }
}
