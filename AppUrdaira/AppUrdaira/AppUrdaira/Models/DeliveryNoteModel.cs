﻿using AppUrdaira.Helpers;
using AppUrdaira.Utilities;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    public class DeliveryNoteModel
    {
        [MaxLength(50)]
        [PrimaryKey]
        [IgnoreDataMember]
        public string RowId { get; set; }

        public string NumPedido { get; set; } = string.Empty;

        public string Dispositivo { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagProcesado { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numPedido"></param>
        /// <param name="dispositivo"></param>
        public DeliveryNoteModel()
        {
            try
            {
                RowId = Generators.RowIdGeneratorCentral();
                NumPedido = string.Empty;
                TagPropio = Generators.TagGenerator();
                Dispositivo = string.Empty;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numPedido"></param>
        /// <param name="dispositivo"></param>
        public DeliveryNoteModel(string numPedido, string dispositivo)
        {
            try
            {
                RowId = Generators.RowIdGeneratorCentral();
                NumPedido = numPedido;
                TagPropio = Generators.TagGenerator();
                Dispositivo = dispositivo;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:()]Error:" + ex.Message);
            }
        }

        public async Task<bool> InsertDeliveryNote()
        {
            try
            {
                int result = 0;
                //List<DeliveryNoteModel> listDeliveryNote = null;
                DatabaseModel databaseModel = new DatabaseModel();
                result = await databaseModel.SaveItemAsync(this);
                var prueba = await databaseModel.GetListItemsAsync<DeliveryNoteModel>();

                //AppConfig.Instance.FileHelper.Copy(Constants.DatabaseFilename);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:InsertDeliveryNote]Error:" + ex.Message);
                return false;
            }
        }
    }
}
