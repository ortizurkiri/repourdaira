﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class UserModel : ObservableDbObjectModel
    {
        [JsonIgnore]
        public const string TableName = "Usuarios";


        [DefaultValue("")]
        [PrimaryKey]
        public string PK
        {
            get { return GetPK(); }
            set { }
        }

        [MaxLength(20)]
        public string Usuario { get; set; } = string.Empty;

        [MaxLength(100)]
        public string Nombre { get; set; } = string.Empty;

        [MaxLength(20)]
        public string Password { get; set; } = string.Empty;

        [MaxLength(100)]
        public string Email { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        public UserModel()
        {
            try
            {
                Usuario = string.Empty;
                Password = string.Empty;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UserModel:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Constructor con parámetros
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        public UserModel(string user, string password)
        {
            try
            {
                Usuario = user;
                Password = password;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UserModel:()2]Error:" + ex.Message);
            }
        }

        public string GetPK()
        {
            try
            {
                return RowId;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UserModel:GetPK]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Insertar o actualizar usuarios
        /// </summary>
        /// <param name="listUser"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListUserBBDD(List<UserModel> listUser)
        {
            try
            {
                string query = string.Empty;
                List<UserModel> listTemp = null;
                int range = 100, rangeList = 0;

                if (listUser?.Count > 0)
                {
                    do
                    {
                        // coger los primeros 100 registros o los registros restantes
                        rangeList = Math.Min(listUser.Count, range);
                        listTemp = listUser.GetRange(0, rangeList);

                        if(listTemp?.Count > 0)
                        {
                            // rellenar cabecera de insert
                            query = $" INSERT OR REPLACE INTO {TableName} (RowId, PK, Usuario, Nombre, Password, Email, TagPropio, TagReenvio, TagBorrado) VALUES ";

                            //rellenar datos de la insert
                            for (int i = 0; i < listTemp.Count; i++)
                            {
                                UserModel user = listTemp[i];
                                query += $@"{(i != 0 ? "," : "")}
                                        ('{user.RowId}'
                                        ,'{user.PK}'
                                        ,'{user.Usuario}'
                                        ,'{user.Nombre}'
                                        ,'{user.Password}'
                                        ,'{user.Email}'
                                        , {user.TagPropio}
                                        , {user.TagReenvio}
                                        , {user.TagBorrado})";
                            }

                            //ejecutar insert
                            var result = await AppConfig.Instance.Database.ExecuteQueryIntAsync(query).ConfigureAwait(false);

                            // eliminar primeros 100 registros
                            listUser.RemoveRange(0, rangeList);
                        }
                    }
                    while (listUser?.Count > 0);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UserModel:InsertOrUpdateListCustomerBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener usuario
        /// </summary>
        /// <returns></returns>
        public static async Task<UserModel> GetUserByIdBBDD(string userId)
        {
            try
            {
                //Iniciar variables
                UserModel user = null;
                string query = $"SELECT * FROM {TableName} WHERE User = {userId} AND TagBorrado = 0";

                //consulta
                user = (await AppConfig.Instance.Database.ExecuteQueryListAsync<UserModel>(query).ConfigureAwait(false))?.FirstOrDefault();

                //devolver objeto
                return user;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UserModel:GetUserByIdBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvio de los usuarios
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioUserBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<UserModel> listUser = null;
                UserModel user = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listUser = await AppConfig.Instance.Database.ExecuteQueryListAsync<UserModel>(query).ConfigureAwait(false);

                //albarán
                user = listUser?.FirstOrDefault();

                //tagreenvio
                tagReenvio =  user?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UserModel:GetLastTagReenvioUserBBDD]Error: " + ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// Comprobar si el usuario es válido en local
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static async Task<bool> ValidLoginLocal(string user, string password)
        {
            try
            {
                bool validUser = false;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    WHERE
                        TagBorrado = 0
                        AND Usuario = '{user}'
                        AND Password = '{password}'
                    LIMIT 1;
                ";

                validUser = (await AppConfig.Instance.Database.ExecuteQueryListAsync<UserModel>(query).ConfigureAwait(false))?.FirstOrDefault() != null;

                return validUser;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[UserModel:ValidLoginLocal]Error: " + ex.Message);
                return false;
            }
        }
    }
}
