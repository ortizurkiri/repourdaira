﻿using AppUrdaira.Helpers;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    public class ObservableObjectModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (TargetInvocationException ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Set the property
        /// </summary>
        /// <typeparam name="T">the 1st type param</typeparam>
        /// <param name="backingStore">backing stores.</param>
        /// <param name="value">the value.</param>
        /// <param name="propertyName">property name.</param>
        /// <param name="onChanged">On changed.</param>
        /// <returns><c>true</c>, if property was set, <c>flase</c> otherwise</returns>
        protected bool SetProperty<T>(ref T backingStore, T value, [CallerMemberName] string propertyName = "", Action onChanged = null)
        {
            try
            {
                if (EqualityComparer<T>.Default.Equals(backingStore, value))
                    return false;

                backingStore = value;
                onChanged?.Invoke();
                OnPropertyChanged(propertyName);
            }
            catch (TargetInvocationException ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return true;
        }

    }
}
