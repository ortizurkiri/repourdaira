﻿using AppUrdaira.Utilities;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Shapes;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class AlbaranLnModel : ObservableDbObjectModel
    {
        [JsonIgnore]
        public const string TableName = "AlbaranLn";

        [DefaultValue("")]
        [PrimaryKey]
        public string PK
        {
            get { return GetPK(); }
            set { }
        }

        public new string RowId { get; set; } = Generators.RowIdGeneratorLocal();

        [DefaultValue("")]
        public string NAlbaran { get; set; } = string.Empty;

        [DefaultValue(0)]
        public int NLinea{ get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Concepto { get; set; } = string.Empty;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Lote { get; set; } = string.Empty;

        [DefaultValue(0)]
        public double Cantidad { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Medida { get; set; } = string.Empty;

        [DefaultValue(0)]
        public double Equivalencia { get; set; } = 0;

        [DefaultValue(0)]
        public double Rappel { get; set; } = 0;

        [DefaultValue(0)]
        public double TipoIva { get; set; } = 21;

        [DefaultValue(0)]
        public double Recargo { get; set; } = 0;

        [DefaultValue(0)]
        public double Precio { get; set; } = 0;

        [DefaultValue(0)]
        public double Importe { get; set; } = 0;

        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();

        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [DefaultValue(0)]
        public double BaseImponible { get; set; } = 0;

        [DefaultValue(0)]
        public double CuotaIva { get; set; } = 0;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        [JsonIgnore]
        [DefaultValue(0)]
        public long TagEnviado { get; set; } = 0;

        [JsonIgnore]
        [DefaultValue(0)]
        public double ImporteMenosRappel => Importe - ((Rappel * Importe) / 100);

        [JsonIgnore]
        [DefaultValue(0)]
        public double ImporteRecargo => (ImporteMenosRappel * Recargo) / 100;

        [JsonIgnore]
        [DefaultValue(0)]
        public double ImporteIva => (ImporteMenosRappel * TipoIva) / 100;

        [JsonIgnore]
        [DefaultValue(0)]
        public double ImporteTotal => ImporteMenosRappel + ImporteRecargo + ImporteIva;

        [DefaultValue(false)]
        public bool EsSincro { get; set; } = false;

        [DefaultValue("")]
        public string RowIdAlbaranCbModel { get; set; } = string.Empty;

        [JsonIgnore]
        public Guid GuidGenericModel { get; set; }
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AlbaranLnModel()
        {

        }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>

        /// <summary>
        /// Obtener objeto actual
        /// </summary>
        /// <returns></returns>
        public AlbaranLnModel GetThisAlbaranLnModel()
        {
            try
            {
                return this;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetThisAlbaranLnModel]Error: " + ex.Message);
                return null;
            }
        }

        public string GetPK()
        {
            try
            {
                string pk = string.Empty;
                string concept = Concepto?.Replace(" ", "_") ?? string.Empty;
                string lote = Lote?.Replace(" ", "_") ?? string.Empty;
                string fecha = FechaRegistro?.Replace(" ", "_")?.Replace("/", "_") ?? string.Empty;
                string hora = HoraRegistro?.Replace(" ", "_")?.Replace(":", "_") ?? string.Empty;

                //si es sincro, guardar todos los datos. Si no es sincro, guardar RowId
                pk = EsSincro ? $"{concept}.{Cantidad}.{lote}.{fecha}.{hora}.{NAlbaran}.{EsSincro}" : RowId;

                return pk;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetPK]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener CuotaIva
        /// </summary>
        /// <returns></returns>
        public double GetCuotaIva()
        {
            try
            {
                return Generators.GetCuotaIva(Importe, TipoIva);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetCuotaIva]Error: " + ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// Modificar NAlbaran
        /// </summary>
        public void SetNAlbaranAndNLinea(string fecha, string nAlbaran, int nLinea)
        {
            try
            {
                NAlbaran = nAlbaran;
                NLinea = nLinea;
                RowId = Generators.RowIdGeneratorCentral(fecha: fecha, numAlbaran: nAlbaran, numLinea: nLinea);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:SetNAlbaran]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Insertar o actualizar lista de líneas de albarán
        /// </summary>
        /// <returns></returns>
        public static async Task InsertOrUpdateListaAlbaranLnModelBBDD(List<AlbaranLnModel> listDeliveryNoteLine, string tableName = AlbaranLnModel.TableName)
        {
            try
            {
                string query = string.Empty;
                List<AlbaranLnModel> listTemp = null;
                int range = 100, rangeList = 0;

                if (listDeliveryNoteLine?.Count > 0)
                {
                    do
                    {
                        // coger los primeros 100 registros o los registros restantes
                        rangeList = Math.Min(listDeliveryNoteLine.Count, range);
                        listTemp = listDeliveryNoteLine.GetRange(0, rangeList);

                        if (listTemp?.Count > 0)
                        {
                            // rellenar cabecera de insert
                            query = $@" INSERT OR REPLACE INTO {tableName} 
                                (
                                    RowId, PK, NAlbaran, NLinea, Concepto, Lote, Cantidad, Medida, Equivalencia, Rappel,
                                    TipoIva, Recargo, Precio, Importe, FechaRegistro, HoraRegistro, BaseImponible,
                                    CuotaIva, TagPropio, TagReenvio, TagBorrado, TagEnviado, EsSincro, RowIdAlbaranCbModel, GuidGenericModel
                                ) 
                                VALUES ";

                            //rellenar datos de la insert
                            for (int i = 0; i < listTemp.Count; i++)
                            {
                                AlbaranLnModel deliveryNoteLine = listTemp[i];
                                query += $@"{(i != 0 ? "," : "")}
                                        ('{deliveryNoteLine.RowId}'
                                        ,'{deliveryNoteLine.PK}'
                                        ,'{deliveryNoteLine.NAlbaran}'                                        
                                        ,'{deliveryNoteLine.NLinea}'
                                        ,'{deliveryNoteLine.Concepto}'
                                        ,'{deliveryNoteLine.Lote}'
                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.Cantidad)}
                                        ,'{deliveryNoteLine.Medida}'
                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.Equivalencia)}
                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.Rappel)}

                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.TipoIva)}
                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.Recargo)}
                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.Precio)}
                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.Importe)}
                                        ,'{deliveryNoteLine.FechaRegistro}'
                                        ,'{deliveryNoteLine.HoraRegistro}'
                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.BaseImponible)}

                                        , {Generators.GetDoubleStrToBBDD(deliveryNoteLine.CuotaIva)}
                                        , {deliveryNoteLine.TagPropio}
                                        , {deliveryNoteLine.TagReenvio}
                                        , {deliveryNoteLine.TagBorrado}
                                        , {deliveryNoteLine.TagEnviado}
                                        ,'{(deliveryNoteLine.EsSincro ? 1 : 0)}'
                                        ,'{deliveryNoteLine.RowIdAlbaranCbModel}'
                                        ,'{deliveryNoteLine.GuidGenericModel}'
                                        )";
                            }

                            //ejecutar insert
                            var result = await AppConfig.Instance.Database.ExecuteQueryIntAsync(query).ConfigureAwait(false);

                            // eliminar primeros 100 registros
                            listDeliveryNoteLine.RemoveRange(0, rangeList);
                        }
                    }
                    while (listDeliveryNoteLine?.Count > 0);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:InsertOrUpdateListaAlbaranLnModelBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Eliminar lista de líneas de albarán
        /// </summary>
        /// <returns></returns>
        public static async Task DeleteListaAlbaranLnModelBBDD(List<AlbaranLnModel> listDeliveryNoteLine)
        {
            try
            {
                if (listDeliveryNoteLine?.Count > 0)
                {
                    foreach (AlbaranLnModel line in listDeliveryNoteLine)
                    {
                        var result = await AppConfig.Instance.Database.DeleteItemAsync(line).ConfigureAwait(false);
                        Debug.WriteLine($"[AlbaranLnModel:DeleteItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:DeleteListaAlbaranLnModelBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de líneas de albarán
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranLnModel>> GetListDeliveryNoteLineByGuidBBDD(Guid guid)
        {
            try
            {
                //Iniciar variables
                List<AlbaranLnModel> listDeliveryNoteLine = null;
                string query = $"SELECT * FROM {TableName} WHERE EsSincro = 0 AND GuidGenericModel = '{guid}'";

                //consulta
                listDeliveryNoteLine = await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranLnModel>(query).ConfigureAwait(false);

                //devolver objeto
                return listDeliveryNoteLine;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetListDeliveryNoteLineByGuidBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener listado de líneas de albarán a partir de RowIdAlbaranCbModel
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranLnModel>> GetListDeliveryNoteLineByRowIdAlbaranCbModel(string rowIdAlbaranCbModel)
        {
            try
            {
                //Iniciar variables
                List<AlbaranLnModel> listDeliveryNoteLine = null;
                string query = $@"
                    SELECT 
                        * 
                    FROM 
                        {TableName} 
                    WHERE   
                        EsSincro = 0 
                        AND RowIdAlbaranCbModel = '{rowIdAlbaranCbModel}'";

                //consulta
                listDeliveryNoteLine = await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranLnModel>(query).ConfigureAwait(false);

                //devolver objeto
                return listDeliveryNoteLine;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetListDeliveryNoteLineByRowIdAlbaranCbModel]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener listado de líneas de albarán con TagEnviado a 0
        /// </summary>
        /// <returns></returns>
        public static async Task<List<AlbaranLnModel>> GetListDeliveryNoteLineByTagEnviado0BBDD()
        {
            try
            {
                //Iniciar variables
                List<AlbaranLnModel> listDeliveryNoteLine = null;
                string query = $@"
                    SELECT 
	                    a.* 
                    FROM 
	                    {TableName} a
		                    LEFT JOIN 
	                    {GenericModel.TableName} g
			                    ON g.GuidGenericModel = a.GuidGenericModel
                    WHERE 
	                    (a.GuidGenericModel = '{Guid.Empty}' OR g.Save = 1)
	                    AND a.TagEnviado = 0
                        AND a.EsSincro = 0
                ";

                //consulta
                listDeliveryNoteLine = await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranLnModel>(query).ConfigureAwait(false);

                //devolver objeto
                return listDeliveryNoteLine;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetListDeliveryNoteLineByTagEnviado0BBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Eliminar líneas de albarán quitándole el guid
        /// </summary>
        /// <returns></returns>
        public static async Task<bool> DeleteDeliveryNoteLineByGuidBBDD(Guid guid)
        {
            try
            {
                //Iniciar variables
                List<AlbaranLnModel> listDeliveryNoteLine = null;
                int deliveryNoteDeleted = 0;

                //obtener listado de líneas de albarán
                listDeliveryNoteLine = await GetListDeliveryNoteLineByGuidBBDD(guid);

                //si tenemos listado de líneas albarán
                if (listDeliveryNoteLine?.Count > 0)
                {
                    foreach (AlbaranLnModel line in listDeliveryNoteLine)
                    {
                        //vaciar guid de cada línea
                        line.GuidGenericModel = Guid.Empty;
                        deliveryNoteDeleted = await AppConfig.Instance.Database.SaveItemAsync(line).ConfigureAwait(false);
                    }
                }
                //si no tenemos líneas
                else
                    deliveryNoteDeleted = 1;
                

                //devolver resultado
                return deliveryNoteDeleted == 1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:DeleteDeliveryNoteLineByGuidBBDD]Error: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Obtener texto de línea
        /// </summary>
        /// <returns></returns>
        public TextPrinterModel GetTextDeliveryNoteLine()
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textLine = string.Empty;
                string lineQuantity = string.Empty;
                string lineConcept = string.Empty;
                int lineConceptSizeDefault = 22;
                string linePrice = string.Empty;
                string lineAmount = string.Empty;

                //obtener las valores necesarios para la línea
                lineQuantity = Cantidad.ToString().PadRight(3);
                lineConcept = Concepto.Length > lineConceptSizeDefault ? Concepto?.Substring(0, lineConceptSizeDefault - 3) + "..." : Concepto;
                lineConcept = lineConcept.ToUpper().PadRight(lineConceptSizeDefault);
                linePrice = string.Format("{0:N2}",Precio).PadLeft(6);
                lineAmount = string.Format("{0:N2}",Importe).PadLeft(7);

                //rellenar texto de línea
                textLine = $" {lineQuantity} {lineConcept}  {linePrice}  {lineAmount}\n";

                //asignar a objeto
                textPrinterModel = new TextPrinterModel(text: textLine);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetTextDeliveryNoteLine]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto de líneas
        /// </summary>
        /// <returns></returns>
        public static List<TextPrinterModel> GetTextDeliveryNoteLines(List<AlbaranLnModel> listAlbaranLnModel)
        {
            try
            {
                //inicializar variables
                List<TextPrinterModel> listTextPrinterModel = new List<TextPrinterModel>();

                if(listAlbaranLnModel?.Count > 0)
                {
                    //obtener las valores necesarios para cada línea
                    foreach (AlbaranLnModel deliveryNoteLine in listAlbaranLnModel)
                    {
                        //obtenemos el texto por línea
                        listTextPrinterModel.Add(deliveryNoteLine.GetTextDeliveryNoteLine());
                    }
                }

                //devolver objeto
                return listTextPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetTextDeliveryNoteLines]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto de cabecera de líneas
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetTextHeaderDeliveryNoteLines()
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textLine = string.Empty;

                //rellenar texto de línea
                textLine = $"Cantidad     Concepto        Precio   Importe\n";
                
                //asignar a objeto
                textPrinterModel = new TextPrinterModel(text: textLine, fontSize: Enums.Enums.FontSizeTextPrinterEnum.Bold);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetTextHeaderDeliveryNoteLines]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto de pie de líneas
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetTextFooterDeliveryNoteLines(List<AlbaranLnModel> listAlbaranLnModel)
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textLine = string.Empty;
                string lineQuantity = string.Empty;
                string lineConcept = string.Empty;
                int lineConceptSizeDefault = 23;
                string linePrice = string.Empty;
                string lineAmount = string.Empty;

                if(listAlbaranLnModel?.Count > 0)
                {
                    //obtener las valores necesarios para la línea
                    lineQuantity = listAlbaranLnModel.Sum(x => x.Cantidad).ToString().PadRight(3);
                    lineConcept = "Productos".PadRight(lineConceptSizeDefault);
                    linePrice = "TOTAL".PadRight(6);
                    lineAmount = string.Format("{0:N2}", listAlbaranLnModel.Sum(x => x.Importe)).ToString().PadLeft(6);

                    //rellenar texto de línea
                    textLine = $" {lineQuantity} {lineConcept}  {linePrice}  {lineAmount}\n";
                }

                //asignar a objeto
                textPrinterModel = new TextPrinterModel(text: textLine, fontSize: Enums.Enums.FontSizeTextPrinterEnum.Bold);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetTextFooterDeliveryNoteLines]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto del desglose de iva de líneas
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetTextIvaDeliveryNoteLines(List<AlbaranLnModel> listAlbaranLnModel)
        {
            try
            {
                //inicializar variables
                TextPrinterModel textPrinterModel = null;
                string textIva = string.Empty;
                string percentageIva = string.Empty;
                string sumAmount = string.Empty;
                string sumIvaXAmount = string.Empty;
                int length = 12;
                List<IGrouping<double, AlbaranLnModel>> listDeliveryNoteGroupByIva = null;

                if (listAlbaranLnModel?.Count > 0)
                {
                    textIva = "-------------- DESGLOSE IVA -----------------\n";

                    //obtener agrupación por iva
                    listDeliveryNoteGroupByIva = listAlbaranLnModel.GroupBy(x => x.TipoIva).OrderBy(o => o.Key).ToList();

                    foreach (IGrouping<double, AlbaranLnModel> ivaAndLines in listDeliveryNoteGroupByIva)
                    {
                        //obtener porcentaje de iva
                        percentageIva = $"{string.Format("{0:N2}", ivaAndLines.Key)}%".PadLeft(8);

                        //obtener suma de importes
                        sumAmount = $"{string.Format("{0:N2}", ivaAndLines.Sum(x => x.ImporteMenosRappel))}".PadLeft(length);

                        //obtener suma iva según importe
                        sumIvaXAmount = $"{string.Format("{0:N2}", ivaAndLines.Sum(x => x.ImporteIva))}".PadLeft(length);

                        textIva += $"{percentageIva}    {sumAmount}    {sumIvaXAmount}\n";
                    }
                }

                //asignar a objeto
                textPrinterModel = new TextPrinterModel(text: textIva);

                //devolver objeto
                return textPrinterModel;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetTextIvaDeliveryNoteLines]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvio de la líneas de albaranes
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioDeliveryNoteLineBBDD(string tableName = AlbaranLnModel.TableName)
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<AlbaranLnModel> listDeliveryNoteLine = null;
                AlbaranLnModel deliveryNoteLine = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {tableName} 
                    WHERE 
                        EsSincro = 1
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listDeliveryNoteLine = await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranLnModel>(query).ConfigureAwait(false);

                //línea de albarán
                deliveryNoteLine = listDeliveryNoteLine?.FirstOrDefault();

                //tagreenvio
                tagReenvio = deliveryNoteLine != null ? deliveryNoteLine.TagReenvio : 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetLastTagReenvioDeliveryNoteLineBBDD]Error: " + ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// Obtener última línea de albarán guardada donde tenga el concepto y la cabecera con el cliente indicado
        /// </summary>
        /// <param name="concepto"></param>
        /// <returns></returns>
        public static async Task<AlbaranLnModel> GetLastSameAlbaranLnByConceptAndCustomer(string concepto, int idCliente)
        {
            try
            {
                //Iniciar variables
                List<AlbaranLnModel> listDeliveryNoteLine = null;
                AlbaranLnModel deliveryNoteLine = null;
                string query = $@" 
                    SELECT 
                        ln.* 
                    FROM 
						 {AlbaranLnModel.TableName}  ln
							INNER JOIN
						 {AlbaranCbModel.TableName}  cb
							ON cb.NAlbaran = ln.NAlbaran
                    WHERE 
                            ln.TagBorrado = 0
						AND cb.TagBorrado = 0
                        AND ln.Concepto = '{concepto}'
						AND cb.IdCliente = {idCliente}
                    ORDER BY 
                        ln.TagPropio DESC 
                    LIMIT 1;
                ";

                //consulta
                listDeliveryNoteLine = await AppConfig.Instance.Database.ExecuteQueryListAsync<AlbaranLnModel>(query).ConfigureAwait(false);

                //línea de albarán
                deliveryNoteLine = listDeliveryNoteLine?.FirstOrDefault();

                //devolver objeto
                return deliveryNoteLine;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AlbaranLnModel:GetLastSameAlbaranLn]Error: " + ex.Message);
                return null;
            }
        }
    }

}
