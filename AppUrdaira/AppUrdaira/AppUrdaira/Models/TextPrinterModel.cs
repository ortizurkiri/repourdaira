﻿using ESCPOS_NET.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static Android.Graphics.Paint;
using static AppUrdaira.Enums.Enums;

namespace AppUrdaira.Models
{
    public class TextPrinterModel
    {
        public Encoding EncodingText => Encoding.UTF8;
        public Encoding EncodingTextSecond => Encoding.GetEncoding(1252); //west european;
        public string Text { get; set; } = string.Empty;
        public FontSizeTextPrinterEnum FontSize { get; set; } = FontSizeTextPrinterEnum.Default;
        public JustifyTextPrinterEnum JustifyText { get; set; } = JustifyTextPrinterEnum.Start;
        public bool IsQR { get; set; } = false;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public TextPrinterModel()
        {

        }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        /// <param name="text"></param>
        /// <param name="fontSize"></param>
        public TextPrinterModel(string text, FontSizeTextPrinterEnum fontSize = FontSizeTextPrinterEnum.Default, 
            JustifyTextPrinterEnum _justifyText = JustifyTextPrinterEnum.Start, bool isQR = false)
        {
            try
            {
                Text = text;
                FontSize = fontSize;
                JustifyText = _justifyText;
                IsQR = isQR;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPageViewModel:()2]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener tamaño de letra en bytes
        /// </summary>
        /// <returns></returns>
        public List<byte> GetBytesFontSize()
        {
            try
            {
                List<byte> listBytes = new List<byte>();

                switch (FontSize)
                {
                    case FontSizeTextPrinterEnum.Default:
                        listBytes = new List<byte>() { 0x1b, 0x21, 0x00 };
                        break;
                    case FontSizeTextPrinterEnum.SmallFont:
                        listBytes = new List<byte>() { 0x1b, 0x21, 0x01 };
                        break;
                    case FontSizeTextPrinterEnum.Bold:
                        listBytes = new List<byte>() { 0x1b, 0x21, 0x08 };
                        break;
                    case FontSizeTextPrinterEnum.DoubleHeight:
                        listBytes = new List<byte>() { 0x1b, 0x21, 0x10 };
                        break;
                    case FontSizeTextPrinterEnum.DoubleWidth:
                        listBytes = new List<byte>() { 0x1b, 0x21, 0x20 };
                        break;
                }

                return listBytes;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetBytesFontSize]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener tamaño de letra en bytes
        /// </summary>
        /// <returns></returns>
        public List<byte> GetBytesJustifyText()
        {
            try
            {
                List<byte> listBytes = new List<byte>();

                switch (JustifyText)
                {
                    case JustifyTextPrinterEnum.Start:
                        listBytes = new List<byte>() { 0x1B, 0x61, 0x00 };
                        break;
                    case JustifyTextPrinterEnum.Center:
                        listBytes = new List<byte>() { 0x1B, 0x61, 0x01 };
                        break;
                    case JustifyTextPrinterEnum.End:
                        listBytes = new List<byte>() { 0x1B, 0x61, 0x02 };
                        break;
                }

                return listBytes;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetBytesFontSize]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener bytes del texto
        /// </summary>
        /// <returns></returns>
        public byte[] GetBytesText()
        {
            try
            {
                byte[] bytes = null;
                List<byte> listBytes = new List<byte>();

                if (IsQR)
                {
                    // añadimos justificación del texto
                    listBytes.AddRange(GetBytesJustifyText());

                    // añadimos qr
                    listBytes.AddRange(ESCPOS.Commands.PrintQRCode(
                        content: Text, 
                        qrCodemodel: ESCPOS.QRCodeModel.Model1,
                        qrodeCorrection: ESCPOS.QRCodeCorrection.Percent7,
                        qrCodeSize: ESCPOS.QRCodeSize.Large));

                    // convertimos todo a array
                    bytes = listBytes.ToArray();
                }
                else
                {
                    // añadimos justificación del texto
                    listBytes.AddRange(GetBytesJustifyText());

                    //añadimos tamaño de texto
                    listBytes.AddRange(GetBytesFontSize());

                    //añadir texto
                    listBytes.AddRange(EncodingText.GetBytes(Text));

                    //convertimos a encoding elegido
                    bytes = Encoding.Convert(EncodingText, EncodingTextSecond, listBytes.ToArray());
                    
                }

                return bytes;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetBytesText]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener texto de logo
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        public static TextPrinterModel GetLogo(string imageName)
        {
            try
            {
                //devolver objeto
                return new TextPrinterModel(text: AppConfig.Instance.BitmapHelper.GetLogo(imageName));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetLogo]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener separador
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetSeparator(int numSeparator = 1, int numWhiteLine = 0)
        {
            try
            {
                // inicializar variables
                string texto = string.Empty;
                string separator = "                     \n";
                string newLine = "\n";

                // añadir separadores
                for (int i = 0; i < numSeparator; i++)
                    texto += separator;

                // añadir líneas en blanco
                for (int i = 0; i < numWhiteLine; i++)
                    texto += newLine;

                //devolver objeto
                return new TextPrinterModel(text: texto);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetSeparator]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener línea en blanco
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetWhiteLine(int numWhiteLine = 1)
        {
            try
            {
                // inicializar variables
                string texto = string.Empty;
                string newLine = "\n";

                // añadir líneas en blanco
                for (int i = 0; i < numWhiteLine; i++)
                    texto += newLine;

                //devolver objeto
                return new TextPrinterModel(text: texto);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetWhiteLine]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener título
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetTitle()
        {
            try
            {
                // inicializar variables
                string texto = "---------------------------------------------\n"
                             + "              URDAIRA SARGADOTEGIA           \n";

                //devolver objeto
                return new TextPrinterModel(text: texto, fontSize: FontSizeTextPrinterEnum.Bold);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetTitle]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener info empresa
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetInfoCompany()
        {
            try
            {
                // inicializar variables
                string texto = "          Patxi Azkonobieta Etxeberria       \n"
                             + "                NIF: 15968787W               \n"
                             + "---------------------------------------------\n";

                //devolver objeto
                return new TextPrinterModel(text: texto, fontSize: FontSizeTextPrinterEnum.SmallFont);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetInfoCompany]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener pie
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetFooter()
        {
            try
            {
                // inicializar variables
                string texto = "\n----------------------------------------------"
                             + "\n----------------------------------------------\n\n";

                //devolver objeto
                return new TextPrinterModel(text: texto);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetFooter]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener datos empresa
        /// </summary>
        /// <returns></returns>
        public static TextPrinterModel GetCompany()
        {
            try
            {
                // inicializar variables
                string phone = "943 372 691 - 630 777 346";
                string cae = "20F1044F";
                string text = "\n         Urdaira Basarria Aginaga-Usurbil    "
                           + $"\n                                             "
                           + $"\n Telefono: {phone}                           "
                           + $"\n C.A.E:    {cae}                             "
                            + "\n---------------------------------------------"
                ;

                //devolver objeto
                return new TextPrinterModel(text: text);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:GetCompany]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Unificar listado de textPrinter en un objeto
        /// </summary>
        /// <param name="listTextPrinter"></param>
        /// <returns></returns>
        public static TextPrinterModel UnionListTextPrinterModel(List<TextPrinterModel> listTextPrinter)
        {
            try
            {
                return new TextPrinterModel(text: string.Join("", listTextPrinter.Select(s => s.Text)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[TextPrinterModel:UnionListTextPrinterModel]Error: " + ex.Message);
                return null;
            }
        }
    }
}
