﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AppUrdaira.Models
{
    [Table("Empresa")]
    public class EmpresaModel
    {
        [MaxLength(100)]
        [DefaultValue("")]
        public string Nombre { get; set; } = string.Empty;

        [MaxLength(100)]
        [DefaultValue("")]
        public string Titular { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Cif { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Nif { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string CodPostal { get; set; } = string.Empty;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Direccion { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string Telefono1 { get; set; } = string.Empty;

        [MaxLength(25)]
        [DefaultValue("")]
        public string Telefono2 { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Poblacion { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Provincia { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Ejercicio { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;
    }
}
