﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AppUrdaira.Models
{
    [Table("Paginacion")]
    public class PaginacionModel
    {
        [DefaultValue("")]
        public string Factura { get; set; } = string.Empty;

        [DefaultValue(0)]
        public int Pag { get; set; } = 0;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;
    }
}
