﻿using Shiny.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    public class PrinterModel
    {
        #region Propiedades

        #endregion

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public PrinterModel() 
        { 
        }

        /// <summary>
        /// Reconectar impresora
        /// </summary>
        public static void ReconectarImpresora()
        {
            try
            {
                Task.Run(() =>
                {
                    try
                    {
                        // si tenemos impresora 
                        if (AppConfig.Instance.Globals.Peripheral != null) 
                        {
                            // si no está conectada, intentamos conectarla
                            if (!AppConfig.Instance.Globals.Peripheral.IsConnected())
                                AppConfig.Instance.Globals.Peripheral.Connect();
                        } 
                        // si no tenemos impresora pero tenemos nombre
                        else if (!string.IsNullOrEmpty(AppConfig.Instance.Globals.DefaultPrinterUuid))
                        {
                            // buscar impresora por nombre e intentamos conectar
                            SearchAndReconnectPeripheralFromPrinterUuid(AppConfig.Instance.Globals.DefaultPrinterUuid);
                        }                                        
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[PrinterModel:ReconectarImpresora:t]Error:" + ex.Message);
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrinterModel:ReconectarImpresora]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Bsucar y reconectar impresora
        /// </summary>
        /// <param name="printerUuid"></param>
        private static void SearchAndReconnectPeripheralFromPrinterUuid(string printerUuid)
        {
            try
            {
                // inicializamos variables
                IDisposable _scanDisposable;
                IBleManager _centralManager = Shiny.ShinyHost.Resolve<IBleManager>();

                // nos subscribimos al escaneo limitándolo a 5 segundos sin encontrar nada
                _scanDisposable = _centralManager
                .ScanForUniquePeripherals()
                .Timeout(TimeSpan.FromSeconds(5))
                .Subscribe(onNext: scanResult =>
                {
                    if (scanResult.Uuid != null && scanResult.Uuid.ToString() == printerUuid)
                    {
                        AppConfig.Instance.Globals.DefaultPrinter = scanResult.Name;
                        AppConfig.Instance.Globals.DefaultPrinterUuid = scanResult.Uuid.ToString();
                        AppConfig.Instance.Globals.Peripheral = scanResult;
                        AppConfig.Instance.Globals.Peripheral?.Connect();
                    }
                    Debug.WriteLine($"********************[PrinterModel:SearchAndReconnectPeripheralFromPrinterName:Name({scanResult?.Name}):Uuid({scanResult?.Uuid})]");
                },
                onError: ex =>
                {
                    Debug.WriteLine($"********************[PrinterModel:SearchAndReconnectPeripheralFromPrinterName:END]");
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrinterModel:SearchAndReconnectPeripheralFromPrinterUuid]Error:" + ex.Message);
            }
        }
    }
}
