﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace AppUrdaira.Models
{
    public class ParametrosInvoiceCreateOutModel
    {
        public string Serie { get; set; } = string.Empty;
        public string NFactura { get; set; } = string.Empty;
        public string Ejercicio { get; set; } = string.Empty;
        public string Fecha { get; set; } = Utilities.Generators.GetDefaultDate();
        public string PathQR { get; set; } = string.Empty;
        public string Base64QR { get; set; } = string.Empty;
        public string TBaiId { get; set; } = string.Empty;
        public string TBaiUrl { get; set; } = string.Empty;
        public string ChainInfoHash { get; set; } = string.Empty;
        public string ChainInfoIssuedTime { get; set; } = string.Empty;
        public string RawJson { get; set; } = string.Empty;
        public bool ResultadoOK { get; set; } = false;
        public string Error { get; set; } = string.Empty;
        public bool EsIntegracionRemota { get; set; } = false;
        public bool PostOperationStatus { get; set; } = false;
        public string PostOperationError { get; set; } = string.Empty;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ParametrosInvoiceCreateOutModel()
        {

        }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        /// <param name="serie"></param>
        /// <param name="nFactura"></param>
        /// <param name="ejercicio"></param>
        /// <param name="fecha"></param>
        /// <param name="pathQR"></param>
        /// <param name="base64QR"></param>
        /// <param name="tBaiId"></param>
        /// <param name="tBaiUrl"></param>
        /// <param name="chainInfoHash"></param>
        /// <param name="chainInfoIssuedTime"></param>
        /// <param name="rawJson"></param>
        /// <param name="resultadoOK"></param>
        /// <param name="error"></param>
        /// <param name="esIntegracionRemota"></param>
        /// <param name="postOperationStatus"></param>
        /// <param name="postOperationError"></param>
        public ParametrosInvoiceCreateOutModel(string serie, string nFactura, string ejercicio, string fecha, 
            string pathQR, string base64QR, string tBaiId, string tBaiUrl, string chainInfoHash, 
            string chainInfoIssuedTime, string rawJson, bool resultadoOK, string error, 
            bool esIntegracionRemota, bool postOperationStatus, string postOperationError)
        {
            try
            {
                Serie = serie;
                NFactura = nFactura;
                Ejercicio = ejercicio;
                Fecha = fecha;
                PathQR = pathQR;
                Base64QR = base64QR;
                TBaiId = tBaiId;
                TBaiUrl = tBaiUrl;
                ChainInfoHash = chainInfoHash;
                ChainInfoIssuedTime = chainInfoIssuedTime;
                RawJson = rawJson;
                ResultadoOK = resultadoOK;
                Error = error;
                EsIntegracionRemota = esIntegracionRemota;
                PostOperationStatus = postOperationStatus;
                PostOperationError = postOperationError;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ParametrosInvoiceCreateOutModel:()]Error: " + ex.Message);
            }  
        }

        /// <summary>
        /// Obtener objeto json en string
        /// </summary>
        /// <returns></returns>
        public string ToJSonString()
        {
            try
            {
                string json = JsonConvert.SerializeObject(this);
                //return HttpUtility.UrlEncode(json);
                return json;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ParametrosInvoiceCreateOutModel:ToJSonString]Error: " + ex.Message);
                return null;
            }
        }
    }
}
