﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class EntidadesModel
    {
        [JsonIgnore]
        public const string TableName = "Entidades";

        public static string ControllerNameWS => "Entidades";

        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [DefaultValue(0)]
        public int IdEnt { get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Descripcion { get; set; } = string.Empty;

        [MaxLength(100)]
        public string FechaRegistro { get; set; } = string.Empty;

        [MaxLength(100)]
        public string HoraRegistro { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Obtener listado de formas de pago del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListEntitysWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetEntidades");

            return url;
        }

        /// <summary>
        /// Insertar o actualizar entidades
        /// </summary>
        /// <param name="listEntity"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListEntityBBDD(List<EntidadesModel> listEntity)
        {
            try
            {
                if (listEntity?.Count > 0)
                {
                    foreach (EntidadesModel entity in listEntity)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(entity).ConfigureAwait(false);
                        Debug.WriteLine($"[EntidadesModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[EntidadesModel:InsertOrUpdateListEntityBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de entidades
        /// </summary>
        /// <returns></returns>
        public static async Task<List<EntidadesModel>> GetListEntityBBDD()
        {
            try
            {
                List<EntidadesModel> listEntity = null;

                listEntity = await AppConfig.Instance.Database.GetListItemsAsync<EntidadesModel>().ConfigureAwait(false);

                return listEntity;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[EntidadesModel:GetListEntityBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvío de las formas de pago
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioEntityBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<EntidadesModel> listItems = null;
                EntidadesModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<EntidadesModel>(query).ConfigureAwait(false);

                //item
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[EntidadesModel:GetLastTagReenvioEntityBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
