﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace AppUrdaira.Models
{
    public class ObservableDbObjectModel : ObservableObjectModel
    {
        #region Propiedades

        [MaxLength(50)]
        [IgnoreDataMember]
        [DefaultValue("")]
        public string RowId { get; set; } = string.Empty;
        #endregion

        public ObservableDbObjectModel()
        {
            try
            {
                RowId = Utilities.Generators.RowIdGeneratorLocal();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ObservableObjectModel:()] Error:" + ex.Message);
            }
        }
    }
}
