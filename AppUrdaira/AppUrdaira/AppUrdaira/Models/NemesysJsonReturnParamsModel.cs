﻿using System;

namespace AppUrdaira.Models
{
    public class NemesysJsonReturnParamsModel
    {
        public string Data { get; set; }
        public string DataSerializationMode { get; set; }
        public string DataTypeName { get; set; }
        public string DataMicrosoftAssemblyQualifiedName { get; set; }
        public NemesysJsonTableModel DataJson => GetDataJson();

        public NemesysJsonReturnParamsModel()
        {

        }

        /// <summary>
        /// Obtener DataJson
        /// </summary>
        /// <returns></returns>
        private NemesysJsonTableModel GetDataJson()
        {
            try
            {
                return !string.IsNullOrEmpty(Data) ? Newtonsoft.Json.JsonConvert.DeserializeObject<NemesysJsonTableModel>(Data) : null;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"[NemesysJsonTableModel:GetDataJson]Error: {ex.Message}");
                return null;
            }
        }
    }
}
