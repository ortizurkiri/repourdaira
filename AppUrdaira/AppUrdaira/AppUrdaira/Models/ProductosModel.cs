﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    [Table(TableName)]
    public class ProductosModel
    {
        [JsonIgnore]
        public const string TableName = "Productos";

        public static string ControllerNameWS => "Productos";

        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [DefaultValue(0)]
        public int IdPdto { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Articulo { get; set; } = string.Empty;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Descripcion { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Unidad { get; set; } = string.Empty;

        [DefaultValue(0)]
        public double Equivalencia { get; set; } = 0;

        [MaxLength(10)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;

        /// <summary>
        /// Obtener listado de artículos del WS
        /// </summary>
        /// <returns></returns>
        public static string GetUrlListArticleWS()
        {
            string url = WSModel.GenerateUrl(ControllerNameWS, "GetProductos");

            return url;
        }

        /// <summary>
        /// Insertar o actualizar artículos
        /// </summary>
        /// <param name="listArticle"></param>
        /// <returns></returns>
        public static async Task InsertOrUpdateListArticleBBDD(List<ProductosModel> listArticle)
        {
            try
            {
                if (listArticle?.Count > 0)
                {
                    foreach (ProductosModel article in listArticle)
                    {
                        var result = await AppConfig.Instance.Database.SaveItemAsync(article).ConfigureAwait(false);
                        Debug.WriteLine($"[ProductosModel:SaveItemAsync] Result: {result}");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ProductosModel:InsertOrUpdateListArticleBBDD]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener listado de artículos
        /// </summary>
        /// <returns></returns>
        public static async Task<List<ProductosModel>> GetListArticleBBDD()
        {
            try
            {
                List<ProductosModel> listArticle = null;

                listArticle = await AppConfig.Instance.Database.GetListItemsAsync<ProductosModel>().ConfigureAwait(false);

                return listArticle;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ProductosModel:GetListArticleBBDD]Error: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Obtener último tag reenvio de los artículos
        /// </summary>
        /// <returns></returns>
        public static async Task<long> GetLastTagReenvioArticleBBDD()
        {
            try
            {
                //Iniciar variables
                long tagReenvio = 0;
                List<ProductosModel> listItems = null;
                ProductosModel item = null;
                string query = $@" 
                    SELECT 
                        * 
                    FROM 
                        {TableName}
                    ORDER BY 
                        TagReenvio DESC 
                    LIMIT 1;
                ";

                //consulta
                listItems = await AppConfig.Instance.Database.ExecuteQueryListAsync<ProductosModel>(query).ConfigureAwait(false);

                //albarán
                item = listItems?.FirstOrDefault();

                //tagreenvio
                tagReenvio = item?.TagReenvio ?? 0;

                //devolver objeto
                return tagReenvio;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ProductosModel:GetLastTagReenvioArticleBBDD]Error: " + ex.Message);
                return 0;
            }
        }
    }
}
