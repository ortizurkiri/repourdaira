﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppUrdaira.Models
{
    public class LoteLnExtendedModel : LoteLnModel
    {
        [DefaultValue(0)]
        public int CantidadActualBotella { get; set; } = 0;

        [DefaultValue(0)]
        public double CantidadActualLitros { get; set; } = 0;

        [DefaultValue(0)]
        public double CantidadTotalBotellas { get; set; } = 0;

        [DefaultValue(0)]
        public double CantidadTotalActualLitros { get; set; } = 0;

        [DefaultValue(0)]
        public double CantidadTotalActualBotellas { get; set; } = 0;

        [JsonIgnore]
        public string VisualizarLoteActual => $"{Lote} \t| {CantidadActualBotella} \t| {CantidadActualLitros}";

        /// <summary>
        /// Obtener listado de lotes extendidos
        /// </summary>
        /// <returns></returns>
        public static async Task<List<LoteLnExtendedModel>> GetListLotExtendedBBDD(bool withQuantity = false)
        {
            try
            {
                // inicializar variables
                List<LoteLnExtendedModel> listLot = null;
                string season = AppConfig.Instance.Globals.Seasons;
                string seasonWithQuotes = !string.IsNullOrEmpty(season) ? string.Join(",", season?.Split(',')?.Select(x => { return $"'{x}'"; })) : string.Empty;
                string queryQuantity = string.Empty;
                string querySeason = string.Empty;
                string query = string.Empty;
                string tableNameLotesAlbaranes = "LotesEnAlbaranes";
                string measuredLiter = "Ltrs.";
                string measuredBottle = "B.";

                //recuperar listado con alguna cantidad
                queryQuantity = withQuantity ? $"AND (CantidadActualBotella > 0 OR CantidadActualLitros > 0)" : string.Empty;

                //rellenar parte de la consulta para temporadas
                querySeason = !string.IsNullOrEmpty(seasonWithQuotes) ? $"AND Temporada IN ({seasonWithQuotes})" : string.Empty;

                // rellenar consulta
                query = $@" WITH {tableNameLotesAlbaranes} AS (
		                        SELECT 
			                         Lote
			                        ,IFNULL(SUM(Cantidad),0.0) AS Cantidad
			                        ,Medida
		                        FROM 
			                        {AlbaranLnOriginalModel.TableName}
		                        WHERE 
			                        TagBorrado = 0 
		                        GROUP BY 
                                     Lote
                                    ,Medida
	                        )
	                        SELECT 
	                           l.Temporada
                              ,l.FechaLote
	                          ,l.IdLoteLn
	                          ,l.Lote
	                          ,IFNULL(l.CantidadBotella,0.0) AS CantidadBotella
	                          ,IFNULL(l.CantidadLitros,0.0)  AS CantidadLitros
	                          ,l.CantidadTotalLitros
	                          ,IFNULL(IFNULL(l.CantidadBotella,0.0)-IFNULL(SUM(ab.Cantidad),0.0),0.0) AS CantidadActualBotella
	                          ,IFNULL(IFNULL(l.CantidadLitros,0.0)- IFNULL(SUM(al.Cantidad),0.0),0.0) AS CantidadActualLitros
	                        FROM 
		                        {LoteLnModel.TableName}  l
			                        LEFT JOIN
		                        {tableNameLotesAlbaranes} al
			                        ON al.Lote = l.Lote AND al.Medida = '{measuredLiter}'
			                        LEFT JOIN
		                        {tableNameLotesAlbaranes} ab
			                        ON ab.Lote = l.Lote AND ab.Medida = '{measuredBottle}'
	                        WHERE 
			                        l.TagBorrado = 0
		                            {querySeason}   
	                        GROUP BY 
		                        l.Temporada, l.FechaLote, l.Lote, l.IdLoteLn, l.CantidadBotella, l.CantidadLitros, l.CantidadTotalLitros
                            HAVING
                                1 = 1
                                {queryQuantity}
	                        ORDER BY
		                         l.Temporada ASC
		                        ,l.IdLoteLn ASC
		                        --,l.Lote ASC
                ";

                //consulta
                listLot = await AppConfig.Instance.Database.ExecuteQueryListAsync<LoteLnExtendedModel>(query).ConfigureAwait(false);

                return listLot;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoteLnExtendedModel:GetListLotExtendedBBDD]Error: " + ex.Message);
                return null;
            }
        }
    }
}
