﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AppUrdaira.Models
{
    [Table("ConceptosEsenciales")]
    public class ConceptosEsencialesModel
    {
        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [DefaultValue(0)]
        public int IdCon { get; set; } = 0;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Descripcion { get; set; } = string.Empty;

        [MaxLength(8)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();   

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [MaxLength(50)]
        [DefaultValue("")]
        public string Unidad { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;
    }
}
