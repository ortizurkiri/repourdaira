﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AppUrdaira.Models
{
    [Table("FacturaLn")]
    public class FacturaLnModel
    {
        [PrimaryKey]
        [DefaultValue(0)]
        public int Id { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Factura { get; set; } = string.Empty;

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string FechaAlbaran { get; set; } = "00000000";

        [MaxLength(50)]
        [DefaultValue("")]
        public string NAlbaran { get; set; } = string.Empty;

        [MaxLength(200)]
        [DefaultValue("")]
        public string Concepto { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Lote { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Cantidad { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Medida { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Equivalencia { get; set; } = string.Empty;

        [DefaultValue(0)]
        public double Precio { get; set; } = 0;

        [MaxLength(10)]
        [DefaultValue("0000000000")]
        public string FechaRegistro { get; set; } = Utilities.Generators.GetDefaultDate();

        [MaxLength(8)]
        [DefaultValue("00000000")]
        public string HoraRegistro { get; set; } = Utilities.Generators.GetDefaultTime();

        [DefaultValue(0)]
        public double Importe { get; set; } = 0;

        [DefaultValue(0)]
        public double Rappel { get; set; } = 0;

        [DefaultValue(0)]
        public int TipoIva { get; set; } = 0;

        [DefaultValue(0)]
        public double Recargo { get; set; } = 0;

        [MaxLength(50)]
        [DefaultValue("")]
        public string NPedido { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Entidad { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string Cobrado { get; set; } = string.Empty;

        [MaxLength(50)]
        [DefaultValue("")]
        public string FormaPago { get; set; } = string.Empty;

        [DefaultValue(0)]
        public long TagPropio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagReenvio { get; set; } = 0;

        [DefaultValue(0)]
        public long TagBorrado { get; set; } = 0;
    }
}
