﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace AppUrdaira.Utilities
{
    public class Constants
    {
        #region WS
        public const string Protocol = "https://";
        public const string HostTest = "uragi.dynu.net:11001";
        public const string HostProd = "uragi.dynu.net:11014";
        public const string BBDDTest = "tbaidemo001";
        public const string BBDDProd = "";
        public const string DeviceInit = "PXX";
# if DEBUG
        public const bool EsTest = true; 
#else
        public const bool EsTest = false; 
#endif
        #endregion

        #region Database local
        public const string DatabaseFilename = "UrdairaSQLite.db3";

        public const SQLite.SQLiteOpenFlags Flags =
            // open the database in read/write mode
            SQLite.SQLiteOpenFlags.ReadWrite |
            // create the database if it doesn't exist
            SQLite.SQLiteOpenFlags.Create |
            // enable multi-threaded database access
            SQLite.SQLiteOpenFlags.SharedCache;

        public static string DatabasePath
        {
            get
            {
                try
                {
                    string basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    return Path.Combine(basePath, DatabaseFilename);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("[DatabaseModel:()]Error:" + ex.Message);
                    return string.Empty;
                }
            }
        }
        #endregion
    }
}
