﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppUrdaira.Utilities
{
    public class Dialogs
    {
        public void DisplayAlert(string title = "Atención", string message = "", string button = "Aceptar")
        {
            try
            {
                _ = Device.InvokeOnMainThreadAsync(async () =>
                    await Application.Current.MainPage.DisplayAlert(
                        title: title,
                        message: message,
                        cancel: button)
                );
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Dialogs:DisplayAlert]Error: " + ex.Message);
            }
        }
        public async Task<bool> DisplayYesNo(string title = "Atención", string message = "")
        {
            try
            {
                return await Application.Current.MainPage.DisplayAlert(title, message, "Sí", "No");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Dialogs:DisplayYesNo]Error: " + ex.Message);
                return false;
            }
        }

        public async Task DisplayAlertAsync(string title, string message, string button)
        {
            try
            {
                await Application.Current.MainPage.DisplayAlert(
                    title: title,
                    message : message,
                    cancel: button);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Dialogs:DisplayAlertAsync]Error: " + ex.Message);
            }
        }
        public void DisplayToast(string message, bool showLongTime = true)
        {
            try
            {
                _ = Device.InvokeOnMainThreadAsync(() =>
                    AppConfig.Instance.DialogsHelper.ShowToast(message, showLongTime)
                );
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Dialogs:DisplayToast]Error: " + ex.Message);
            }
        }
    }
}
