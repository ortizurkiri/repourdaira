﻿using AppUrdaira.Models;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppUrdaira.Utilities
{
    public class Navigations
    {
        public void ShellGoToPageBeginInvoke(string nameOfPage, Action actionBefore = null, Action actionAfter = null, Action actionFinally = null)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    try
                    {
                        actionBefore?.Invoke();

                        await Shell.Current.GoToAsync(nameOfPage);

                        actionAfter?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[Navigations:ShellGoToPageBeginInvoke]Error:" + ex.Message);
                    }
                    finally
                    {
                        actionFinally?.Invoke();
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Navigations:ShellGoToPageBeginInvoke]Error: " + ex.Message);
            }
        }

        public void GoToPageBeginInvoke(Page page, Action actionBefore = null, Action actionAfter = null, Action actionFinally = null)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    try
                    {
                        actionBefore?.Invoke();

                        await App.Current.MainPage.Navigation.PushAsync(page);

                        actionAfter?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[Navigations:GoToPageBeginInvoke]Error:" + ex.Message);
                    }
                    finally
                    {
                        actionFinally?.Invoke();
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Navigations:GoToPageBeginInvoke]Error: " + ex.Message);
            }
        }

        public async Task GoToPageInvokeAsync(Page page, Action actionBefore = null, Action actionAfter = null, Action actionFinally = null)
        {
            try
            {
                await Device.InvokeOnMainThreadAsync(async () =>
                {
                    try
                    {
                        actionBefore?.Invoke();

                        await App.Current.MainPage.Navigation.PushAsync(page);

                        actionAfter?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[Navigations:GoToPageInvokeAsync]Error:" + ex.Message);
                    }
                    finally
                    {
                        actionFinally?.Invoke();
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Navigations:GoToPageInvokeAsync]Error: " + ex.Message);
            }
        }

        public void GoToPageInvokeAsyncVoid(Page page, Action actionBefore = null, Action actionAfter = null, Action actionFinally = null)
        {
            try
            {
                Device.InvokeOnMainThreadAsync(async () =>
                {
                    try
                    {
                        actionBefore?.Invoke();

                        await App.Current.MainPage.Navigation.PushAsync(page);

                        actionAfter?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[Navigations:GoToPageInvokeAsyncVoid]Error:" + ex.Message);
                    }
                    finally
                    {
                        actionFinally?.Invoke();
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Navigations:GoToPageInvokeAsyncVoid]Error: " + ex.Message);
            }
        }

        public void RemovePage(Type type)
        {
            try
            {
                Page page = App.Current.MainPage.Navigation.NavigationStack.FirstOrDefault(x => x?.GetType() == type);

                if (page != null)
                    App.Current.MainPage.Navigation.RemovePage(page);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Navigations:RemovePage]Error: " + ex.Message);
            }
        }
    }
}
