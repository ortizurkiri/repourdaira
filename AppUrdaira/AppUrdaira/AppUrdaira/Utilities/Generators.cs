﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace AppUrdaira.Utilities
{
    public class Generators
    {
        public static string RowIdGeneratorCentral(string fecha = "", string numAlbaran = "00000", int numLinea = 0)
        {
            try
            {

                return $"{(!string.IsNullOrEmpty(fecha) ? fecha : $"{DateTime.Now:yyyyMMdd}")}.{numAlbaran}{(numLinea != 0 ? $".{numLinea}": string.Empty)}";
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:RowIdGeneratorCentral]Error:" + ex.Message);
                return string.Empty;
            }
        }

        public static string RowIdGeneratorLocal(bool addGui = true)
        {
            try
            {
                string rowId = string.Empty;

                rowId = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                //si le añadimos gui, lo cortamos en 50 caracteres
                if (addGui)
                    rowId = (rowId + "." + Guid.NewGuid().ToString("N")).Substring(0, 50);

                return rowId;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:RowIdGenerator]Error:" + ex.Message);
                return string.Empty;
            }
        }

        public static long TagGenerator()
        {
            try
            {
                long tag = 0;

                tag = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff"));

                return tag;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:TagGenerator]Error:" + ex.Message);
                return 0;
            }
        }

        public static T GetNext<T>(IEnumerable<T> list, T current)
        {
            try
            {
                return list.SkipWhile(x => !x.Equals(current)).Skip(1).First();
            }
            catch
            {
                return default(T);
            }
        }

        public static T GetPrevious<T>(IEnumerable<T> list, T current)
        {
            try
            {
                return list.TakeWhile(x => !x.Equals(current)).Last();
            }
            catch
            {
                return default(T);
            }
        }

        public static string GetDefaultDate()
        {
            try
            {
                return "0000000000";
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:GetDefaultDate]Error:" + ex.Message);
                return string.Empty;
            }
        }

        public static string GetDefaultTime()
        {
            try
            {
                return "00000000";
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:GetDefaultTime]Error:" + ex.Message);
                return string.Empty;
            }
        }

        public static string GetCurrentDate()
        {
            try
            {
                return DateTime.Now.ToString("yyyyMMdd");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:GetCurrentDate]Error:" + ex.Message);
                return string.Empty;
            }
        }

        public static string GetCurrentTime()
        {
            try
            {
                return DateTime.Now.ToString("HHmmss");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:GetCurrentTime]Error:" + ex.Message);
                return string.Empty;
            }
        }

        public static string GetDoubleStrToBBDD(double num)
        {
            try
            {
                return num.ToString().Replace(",", ".");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:GetDoubleStrToBBDD]Error:" + ex.Message);
                return string.Empty;
            }
        }

        public static double GetCuotaIva(double importe, double tipoIva)
        {
            try
            {
                return importe > 0 && tipoIva > 0 ? importe*tipoIva/100 : 0;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:GetCuotaIva]Error:" + ex.Message);
                return 0.0;
            }
        }

        public static string GetDateTimeStringByLongString(string dateTimeLongString)
        {
            try
            {
                // inicializar
                string dateTimeStr = dateTimeLongString;

                // si está relleno con al menos 14 caracteres, intentamos convertir a fecha
                if(!string.IsNullOrEmpty(dateTimeLongString) && dateTimeLongString.Length >= 14)
                {
                    // convertir de string a Datetime
                    DateTime.TryParseExact(
                        s: dateTimeLongString.Substring(0,14),
                        format: "yyyyMMddHHmmss",
                        provider: CultureInfo.InvariantCulture,
                        style: DateTimeStyles.None,
                        result: out DateTime resultDateTime
                    );

                    if(resultDateTime != null)
                    {
                        dateTimeStr = resultDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                    }
                }
            
                // devolver
                return dateTimeStr;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Generators:GetDateTimeStringByLongString]Error:" + ex.Message);
                return string.Empty;
            }
        }
    }
}
