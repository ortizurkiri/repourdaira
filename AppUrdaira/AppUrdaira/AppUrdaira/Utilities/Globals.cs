﻿using AppUrdaira.Models;
using Shiny.BluetoothLE;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace AppUrdaira.Utilities
{
    public class Globals
    {
        private string host;
        public string Host
        {
            get { return Preferences.Get("Globals.Host", string.Empty); }
            set
            {
                host = value;
                Preferences.Set("Globals.Host", host);
            }
        }

        private string bbdd;
        public string BBDD
        {
            get { return Preferences.Get("Globals.BBDD", string.Empty); }
            set
            {
                bbdd = value;
                Preferences.Set("Globals.BBDD", bbdd);
            }
        }

        private string device;
        public string Device
        {
            get { return Preferences.Get("Globals.Device", string.Empty); }
            set
            {
                device = value;
                Preferences.Set("Globals.Device", device);
            }
        }

        private string customerDefault;
        public string CustomerDefault
        {
            get { return Preferences.Get("Globals.CustomerDefault", string.Empty); }
            set
            {
                customerDefault = value;
                Preferences.Set("Globals.CustomerDefault", customerDefault);
            }
        }

        private bool checkNumAlbaranWSLocal;
        public bool CheckNumAlbaranWSLocal
        {
            get { return Preferences.Get("Globals.CheckNumAlbaranWSLocal", true); }
            set
            {
                checkNumAlbaranWSLocal = value;
                Preferences.Set("Globals.CheckNumAlbaranWSLocal", checkNumAlbaranWSLocal);
            }
        }

        private UserModel loginUser;
        public UserModel LoginUser
        {
            get { return loginUser; }
            set
            {
                loginUser = value;
                Preferences.Set("Globals.LoginUser.User", loginUser.Usuario);
                Preferences.Set("Globals.LoginUser.Password", loginUser.Password);
            }
        }


        private string defaultPrinter;
        public string DefaultPrinter
        {
            get { return Preferences.Get("Globals.DefaultPrinter", string.Empty); }
            set
            {
                defaultPrinter = value;
                Preferences.Set("Globals.DefaultPrinter", defaultPrinter);
            }
        }

        private string defaultPrinterUuid;
        public string DefaultPrinterUuid
        {
            get { return Preferences.Get("Globals.DefaultPrinterUuid", string.Empty); }
            set
            {
                defaultPrinterUuid = value;
                Preferences.Set("Globals.DefaultPrinterUuid", defaultPrinterUuid);
            }
        }

        private IPeripheral peripheral;
        public IPeripheral Peripheral
        {
            get { return peripheral; }
            set
            {
                peripheral = value;
            }
        }

        private string seasons;
        public string Seasons
        {
            get { return Preferences.Get("Globals.Seasons", string.Empty); }
            set
            {
                seasons = value;
                Preferences.Set("Globals.Seasons", seasons);
            }
        }

        public void CancelConnectionPeripheral()
        {
            try
            {
                if (Peripheral != null)
                {
                    Peripheral.CancelConnection();
                    Peripheral = null;
                    DefaultPrinter = string.Empty;
                    DefaultPrinterUuid = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Globals:CancelConnectionPeripheral]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Inicializar preferencias
        /// </summary>
        public void InicializarPreferencias()
        {
            try
            {
                if (string.IsNullOrEmpty(Host))
                    Preferences.Set("Globals.Host", Constants.EsTest ? Constants.HostTest : Constants.HostProd);

                if (string.IsNullOrEmpty(BBDD))
                    Preferences.Set("Globals.BBDD", Constants.EsTest ? Constants.BBDDTest : Constants.BBDDProd);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Globals:InicializarPreferencias]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Resetear preferencias
        /// </summary>
        public void ResetearPreferencias()
        {
            try
            {
                Preferences.Clear();
                Device = string.Empty;
                CustomerDefault = string.Empty;
                CheckNumAlbaranWSLocal = true;
                Host = Constants.EsTest ? Constants.HostTest : Constants.HostProd;
                BBDD = Constants.EsTest ? Constants.BBDDTest : Constants.BBDDProd;
                DefaultPrinter = string.Empty;
                DefaultPrinterUuid = string.Empty;
                Peripheral = null;
                Seasons = string.Empty;      
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Globals:ResetearPreferencias]Error:" + ex.Message);
            }
        }
    }
}
