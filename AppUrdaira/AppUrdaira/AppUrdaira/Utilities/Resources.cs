﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppUrdaira.Utilities
{
    public static class Resources
    {
        public static string Atencion => "Atención";
        public static string Aviso => "Aviso";
        public static string Aceptar => "Aceptar";
    }
}
