﻿using AppUrdaira.Models;
using AppUrdaira.Utilities;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using static AppUrdaira.Enums.Enums;

namespace AppUrdaira.ViewModels
{
    public class DeliveryNoteViewModel : BaseViewModel
    {
        #region Propiedades
        public GenericModel GenericModel { get; set; } = null;

        //Número de albarán
        private string numDeliveryNote = string.Empty;
        public string NumDeliveryNote
        {
            get { return numDeliveryNote; }
            set 
            { 
                numDeliveryNote = value; 
                OnPropertyChanged(nameof(NumDeliveryNote));
                SaveNumDeliveryNote();
            }
        }

        //Fecha de albarán
        private DateTime dateDeliveryNote = DateTime.Now;
        public DateTime DateDeliveryNote
        {
            get { return dateDeliveryNote; }
            set 
            { 
                dateDeliveryNote = value;
                OnPropertyChanged(nameof(DateDeliveryNote));
                SaveDateDeliveryNote();
            }
        }
        private DateTime minDateDeliveryNote = DateTime.Now.AddMonths(-1);
        public DateTime MinDateDeliveryNote
        {
            get { return minDateDeliveryNote; }
            set { minDateDeliveryNote = value; OnPropertyChanged(nameof(MinDateDeliveryNote)); }
        }
        private DateTime maxDateDeliveryNote = DateTime.Now.AddMonths(1);
        public DateTime MaxDateDeliveryNote
        {
            get { return maxDateDeliveryNote; }
            set { maxDateDeliveryNote = value; OnPropertyChanged(nameof(MaxDateDeliveryNote)); }
        }

        //Listado de clientes

        private bool isVisibleListCustomers = false;
        public bool IsVisibleListCustomers
        {
            get { return isVisibleListCustomers; }
            set { isVisibleListCustomers = value; OnPropertyChanged(nameof(IsVisibleListCustomers)); }
        }
        private List<ClientesModel> listCustomer = null;
        public List<ClientesModel> ListCustomer
        {
            get { return listCustomer; }
            set { listCustomer = value; OnPropertyChanged(nameof(ListCustomer)); }
        }
        private ObservableCollection<ClientesModel> listVisualCustomer = null;
        public ObservableCollection<ClientesModel> ListVisualCustomer
        {
            get { return listVisualCustomer; }
            set { listVisualCustomer = value; OnPropertyChanged(nameof(ListVisualCustomer)); }
        }
        private ClientesModel selectedCustomer = null;
        public ClientesModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set 
            { 
                selectedCustomer = value; 
                OnPropertyChanged(nameof(SelectedCustomer));
                SetSurchargeAndNameCustomer();
                SaveCustomerDeliveryNote();
            }
        }
        private string nameCustomer = string.Empty;
        public string NameCustomer
        {
            get { return nameCustomer; }
            set { nameCustomer = value; OnPropertyChanged(nameof(NameCustomer)); }
        }

        public bool IsFirstAccessCustomers { get; set; } = true;

        //Listado de formas de pago
        private ObservableCollection<FormasPagoModel> listWayToPay = null;
        public ObservableCollection<FormasPagoModel> ListWayToPay
        {
            get { return listWayToPay; }
            set { listWayToPay = value; OnPropertyChanged(nameof(ListWayToPay)); }
        }
        private FormasPagoModel selectedWayToPay = null;
        public FormasPagoModel SelectedWayToPay
        {
            get { return selectedWayToPay; }
            set 
            { 
                selectedWayToPay = value; 
                OnPropertyChanged(nameof(SelectedWayToPay));
                SaveWayToPayDeliveryNote();
            }
        }

        //Listado de entidades
        private ObservableCollection<EntidadesModel> listEntity = null;
        public ObservableCollection<EntidadesModel> ListEntity
        {
            get { return listEntity; }
            set { listEntity = value; OnPropertyChanged(nameof(ListEntity)); }
        }
        private EntidadesModel selectedEntity = null;
        public EntidadesModel SelectedEntity
        {
            get { return selectedEntity; }
            set 
            { 
                selectedEntity = value; 
                OnPropertyChanged(nameof(SelectedEntity));
                SaveEntityDeliveryNote();
            }
        }

        //Listado de recargo
        private ObservableCollection<RecargoEquivalenciaModel> listSurcharge = null;
        public ObservableCollection<RecargoEquivalenciaModel> ListSurcharge
        {
            get { return listSurcharge; }
            set { listSurcharge = value; OnPropertyChanged(nameof(ListSurcharge)); }
        }
        private RecargoEquivalenciaModel selectedSurcharge = null;
        public RecargoEquivalenciaModel SelectedSurcharge
        {
            get { return selectedSurcharge; }
            set
            {
                selectedSurcharge = value;
                OnPropertyChanged(nameof(SelectedSurcharge));
                OnPropertyChanged(nameof(TotalSurcharge));
                OnPropertyChanged(nameof(TotalDeliveryNote));
                SaveRechargeDeliveryNote();
            }
        }

        //Rappel
        private double rappel = 0;
        public double Rappel
        {
            get { return rappel; }
            set 
            { 
                rappel = value; 
                OnPropertyChanged(nameof(Rappel));
                OnPropertyChanged(nameof(TotalRappel));
                OnPropertyChanged(nameof(SubTotal));
                OnPropertyChanged(nameof(TotalSurcharge));
                OnPropertyChanged(nameof(TotalDeliveryNote));
                SaveRappelDeliveryNote();
            }
        }

        //Cobrado
        private bool charge = false;
        public bool Charge
        {
            get { return charge; }
            set
            {
                charge = value;
                OnPropertyChanged(nameof(Charge));
                OnPropertyChanged(nameof(ShowInvoiceButton));
                SaveChargeDeliveryNote();
            }
        }

        //Albarán
        private AlbaranCbModel deliveryNote;
        public AlbaranCbModel DeliveryNote
        {
            get { return deliveryNote; }
            set 
            { 
                deliveryNote = value; 
                OnPropertyChanged(nameof(DeliveryNote));
                SaveDeliveryNote();
            }
        }

        //Listado de líneas de albarán
        private List<AlbaranLnModel> listDeliveryNoteLn = null;
        public List<AlbaranLnModel> ListDeliveryNoteLn
        {
            get { return listDeliveryNoteLn; }
            set 
            { 
                listDeliveryNoteLn = value;
                OnPropertyChanged(nameof(ListDeliveryNoteLn));
                OnPropertyChanged(nameof(ListObservableDeliveryNoteLn));
                OnPropertyChanged(nameof(TotalAmount));
                OnPropertyChanged(nameof(TotalRappel));
                OnPropertyChanged(nameof(SubTotal));
                OnPropertyChanged(nameof(TotalSurcharge));
                OnPropertyChanged(nameof(TotalIva));
                OnPropertyChanged(nameof(TotalDeliveryNote));
                SaveListDeliveryNoteLine();
            }
        }

        private object selectedDeliveryNoteLn;
        public object SelectedDeliveryNoteLn
        {
            get { return selectedDeliveryNoteLn; }
            set
            {
                selectedDeliveryNoteLn = value;
                OnPropertyChanged(nameof(SelectedDeliveryNoteLn));
                ChooseDeliveryNoteLn();
            }
        }

        //Listado de líneas de albarán
        public ObservableCollection<AlbaranLnModel> ListObservableDeliveryNoteLn => new ObservableCollection<AlbaranLnModel>(ListDeliveryNoteLn?.Where(x => x.TagBorrado == 0) ?? new List<AlbaranLnModel>());

        //para guardar o no los cambios en BBDD
        public bool SaveChangesBBDD { get; set; } = true;

        //Activado si no está guardado
        public bool EnabledOption => !Save;

        //Mostrar botones de opciones según si está guardado o no
        public bool ShowNewButton => Save & CanEditDelivery;
        public bool ShowEditButton => Save && CanEditDelivery;
        public bool ShowPrintButton => Save;
        public bool ShowDeleteButton => !Save;
        public bool ShowSaveButton => !Save;

        #region Pendiente
        // Ver botones para ver todos los albarnaes
        public bool ShowAllDelivery =>  Save;
        //public bool ShowAllDelivery => false;// Save;
        // Ver botón de factura
        public bool ShowInvoiceButton => Save && Charge;
        #endregion

        //Guardado
        private bool save = false;
        public bool Save
        {
            get { return save; }
            set
            {
                try
                {
                    save = value;
                    SaveStatusDeliveryNote();

                    Device.InvokeOnMainThreadAsync(async() =>
                    {
                        try
                        {
                            await Task.Delay(100).ConfigureAwait(false);
                            OnPropertyChanged(nameof(Save));
                            OnPropertyChanged(nameof(EnabledOption));
                            OnPropertyChanged(nameof(ShowInvoiceButton));
                            OnPropertyChanged(nameof(ShowNewButton));
                            OnPropertyChanged(nameof(ShowEditButton));
                            OnPropertyChanged(nameof(ShowPrintButton));
                            OnPropertyChanged(nameof(ShowDeleteButton));
                            OnPropertyChanged(nameof(ShowSaveButton));
                            OnPropertyChanged(nameof(ShowAllDelivery));
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("[DeliveryNoteViewModel:Save:Device]Error: "+ex.Message);
                        }
                    });
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("[DeliveryNoteViewModel:Save]Error: " + ex.Message);
                }
            }
        }

        // flag para comprobar si el usuario puede acceder a todos los albaranes
        private bool accessAllDelivery = false;
        public bool AccessAllDelivery
        {
            get { return accessAllDelivery; }
            set { accessAllDelivery = value; OnPropertyChanged(nameof(AccessAllDelivery));    }
        }


        // flag para comprobar si el usuario puede crear nuevo albarán
        private bool canEditDelivery = true;
        public bool CanEditDelivery
        {
            get { return canEditDelivery; }
            set 
            { 
                canEditDelivery = value; 
                OnPropertyChanged(nameof(CanEditDelivery)); 
                OnPropertyChanged(nameof(ShowNewButton));
                OnPropertyChanged(nameof(ShowEditButton));
                SaveCanEditDeliveryNote();
            }
        }


        //Totales
        public double TotalAmount => ListObservableDeliveryNoteLn?.Sum(x => x.Importe) ?? 0;
        public double TotalRappel => (TotalAmount*Rappel)/100;
        public double SubTotal => TotalAmount - TotalRappel;
        public double TotalSurcharge => (SubTotal*(SelectedSurcharge?.Recargo ?? 0))/100;
        public double TotalIva=> ListObservableDeliveryNoteLn?.Sum(x => x.ImporteIva) ?? 0;                                                
        public double TotalDeliveryNote => SubTotal + TotalSurcharge + TotalIva;

        //Listado de productos
        public List<ProductosModel> ListArticle = null;
        public List<LoteLnExtendedModel> ListLotsExtended = null;
        public List<UnidadesMedidaModel> ListUnits = null;
        public List<TipoIvaModel> ListIVA = null;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public DeliveryNoteViewModel()
        {
            try
            {
                //Inicializar variables
                Title = "Alta albaranes";
                ListCustomer = new List<ClientesModel>();
                ListVisualCustomer = new ObservableCollection<ClientesModel>();
                ListWayToPay = new ObservableCollection<FormasPagoModel>();
                ListEntity = new ObservableCollection<EntidadesModel>();
                ListArticle = new List<ProductosModel>();
                ListLotsExtended = new List<LoteLnExtendedModel>();
                ListUnits = new List<UnidadesMedidaModel>();

                //Inicializar listados
                InitializeFieldsCommand.Execute(true);

                //prueba iker
                //var testIker = DependencyService.Get<Interfaces.ITestHelper>()?.Test();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:()]Error:" + ex.Message);
            }
        }


        #region Commands
        /// <summary>
        /// Crear albaranes
        /// </summary>
        public ICommand CreateDeliveryNoteCommand => new Command(CreateDeliveryNote);
        private void CreateDeliveryNote()
        {
            try
            {
                DeliveryNoteModel deliveryNote = new DeliveryNoteModel(numPedido: "1", dispositivo: "P1");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:CreateDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Inicializar campos
        /// </summary>
        public ICommand InitializeFieldsCommand => new Command<bool>(async (init) => await InitializeFields(init).ConfigureAwait(false));
        private async Task InitializeFields(bool init = false)
        {
            try
            {
                IsBusy = true;

                if (init)
                    IsAllBusy = true;

                //inicializar variables
                List<ClientesModel> listaClientes = null;
                List<FormasPagoModel> listaFormasPago = null;
                List<EntidadesModel> listaEntidades = null;
                List<ProductosModel> listaProductos = null;
                List<LoteLnExtendedModel> listaLotes = null;
                List<UnidadesMedidaModel> listaUnidades = null;
                List<TipoIvaModel> listaIVA = null;
                List<RecargoEquivalenciaModel> listaSurcharge = null;
                GenericModel genericModel = null;
                ListCustomer = new List<ClientesModel>();
                ListVisualCustomer = new ObservableCollection<ClientesModel>();
                ListWayToPay = new ObservableCollection<FormasPagoModel>();
                ListEntity = new ObservableCollection<EntidadesModel>();
                ListArticle = new List<ProductosModel>();
                ListLotsExtended = new List<LoteLnExtendedModel>();
                ListUnits = new List<UnidadesMedidaModel>();
                ListIVA = new List<TipoIvaModel>();
                ListEntity = new ObservableCollection<EntidadesModel>();
                AccessAllDelivery = true; // test

                //asignar clientes
                listaClientes = await ClientesModel.GetListCustomerBBDD();

                if (listaClientes != null)
                {
                    ListCustomer = new List<ClientesModel>(listaClientes);
                    ListVisualCustomer = new ObservableCollection<ClientesModel>(ListCustomer);
                }

                //asignar formas de pago
                listaFormasPago = await FormasPagoModel.GetListListWayToPayBBDD();

                if (listaFormasPago != null)
                    ListWayToPay = new ObservableCollection<FormasPagoModel>(listaFormasPago);

                //asignar entidades
                listaEntidades = await EntidadesModel.GetListEntityBBDD();

                if (listaEntidades != null)
                    ListEntity = new ObservableCollection<EntidadesModel>(listaEntidades);

                //asignar productos
                listaProductos = await ProductosModel.GetListArticleBBDD();

                if (listaProductos != null)
                    ListArticle = listaProductos;

                //asignar lotes
                listaLotes = await LoteLnExtendedModel.GetListLotExtendedBBDD(withQuantity: true);

                if (listaLotes != null)
                    ListLotsExtended = listaLotes;

                //asignar unidades
                listaUnidades = await UnidadesMedidaModel.GetListUnitBBDD();

                if (listaUnidades != null)
                    ListUnits = listaUnidades;

                //asignar iva
                listaIVA = await TipoIvaModel.GetListTipoIvaBBDD();

                if (listaIVA != null)
                    ListIVA = listaIVA;

                //asignar recargo
                listaSurcharge = await RecargoEquivalenciaModel.GetListSurchargeBBDD();

                if (listaUnidades != null)
                    ListSurcharge = new ObservableCollection<RecargoEquivalenciaModel>(listaSurcharge);

                //asignar generic model
                genericModel = await GenericModel.GetGenericModelBBDD();

                //si recuperamos el modelo genérico guardado a medias
                if (genericModel != null)
                {
                    GenericModel = genericModel;
                    GenericModel.DeliveryNote = await AlbaranCbModel.GetDeliveryNoteByGuidBBDD(GenericModel.GuidGenericModel).ConfigureAwait(false);
                    GenericModel.ListDeliveryNoteLns = await AlbaranLnModel.GetListDeliveryNoteLineByGuidBBDD(GenericModel.GuidGenericModel).ConfigureAwait(false);

                    await AssignDataDeliveryNoteAndLines().ConfigureAwait(false);
                }
                //si no conseguimos recuperar ninguno,inicializamos los campos y listados
                else
                {
                    GenericModel = new GenericModel();
                    NumDeliveryNote = await AlbaranCbModel.GetNextNumDeliverynoteBBDD(NumDeliveryNote).ConfigureAwait(false);
                    InitializeDeliveryNote();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:InitializeFields]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;

                if (init)
                    IsAllBusy = false;
            }
        }

        /// <summary>
        /// Añadir nueva línea de albarán
        /// </summary>
        public ICommand NewDeliveryNoteLineCommand => new Command(NewDeliveryNoteLine);
        private void NewDeliveryNoteLine()
        {
            try
            {
                //completar GenericModel
                GenericModel.DeliveryNoteLine = null;
                GenericModel.ListArticles = ListArticle;
                GenericModel.ListLotsExtended = ListLotsExtended;
                GenericModel.ListUnits = ListUnits;
                GenericModel.ListIVA = ListIVA;

                //ir a pantalla de línea de albarán
                AppConfig.Instance.Navigations.GoToPageBeginInvoke(new DeliveryNoteLinePage(gm: GenericModel));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:NewDeliveryNoteLine]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Ver factura
        /// </summary>
        public ICommand ViewInvoiceCommand => new Command(ViewInvoice);
        private void ViewInvoice()
        {
            try
            {
                AppConfig.Instance.Navigations.GoToPageBeginInvoke(new InvoicePage(genericModel: GenericModel));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:ViewInvoice]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Eliminar línea de albarán
        /// </summary>
        public ICommand DeleteDeliveryNoteLineCommand => new Command<AlbaranLnModel>(async (deliveryNoteLine) => await DeleteDeliveryNoteLine(deliveryNoteLine).ConfigureAwait(false));
        private async Task DeleteDeliveryNoteLine(AlbaranLnModel deliveryNoteLine)
        {
            try
            {
                if (deliveryNoteLine != null && ListDeliveryNoteLn?.Count > 0
                    && await AppConfig.Instance.Dialogs.DisplayYesNo(message: "¿Está seguro que desea eliminar la línea de albarán actual?"))
                {
                    //Eliminar listado local actualizando el tag borrado
                    ListDeliveryNoteLn.FirstOrDefault(x => x.RowId == deliveryNoteLine.RowId).TagBorrado = Utilities.Generators.TagGenerator();

                    //Refrecar listados
                    RefreshCommand.Execute(null);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:DeleteDeliveryNoteLine]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Edita línea de albarán
        /// </summary>
        public ICommand EditDeliveryNoteLineCommand => new Command<AlbaranLnModel>((deliveryNoteLine) => EditDeliveryNoteLine(deliveryNoteLine));
        private void EditDeliveryNoteLine(AlbaranLnModel deliveryNoteLine)
        {
            try
            {
                if (deliveryNoteLine != null && ListDeliveryNoteLn?.Count > 0)
                {
                    IsBusy = true;

                    //completar GenericModel
                    GenericModel.DeliveryNoteLine = deliveryNoteLine;
                    GenericModel.ListArticles = ListArticle;
                    GenericModel.ListLotsExtended = ListLotsExtended;
                    GenericModel.ListUnits = ListUnits;
                    GenericModel.ListIVA = ListIVA;

                    //ir a pantalla de línea de albarán
                    AppConfig.Instance.Navigations.GoToPageBeginInvoke(page: new DeliveryNoteLinePage(gm: GenericModel), actionFinally: () => IsBusy = false);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:DeleteDeliveryNoteLine]Error:" + ex.Message);
                IsBusy = false;
            }
        }

        /// <summary>
        /// Editar albarán
        /// </summary>
        public ICommand EditCommand => new Command(async () => await Edit());
        private async Task Edit()
        {
            try
            {
                IsBusy = true;

                await Task.Delay(200);

                // si el albarán está cobrado y cerrado, no permitimos editar, mostrando un mensaje
                if (GenericModel?.DeliveryNote != null && GenericModel.DeliveryNote.Cobrado && GenericModel.DeliveryNote.Cerrado)
                {
                    AppConfig.Instance.Dialogs.DisplayAlert(message: $"El albarán no se puede editar porque ya se ha cobrado");
                }
                // si el albarán no está cobrado y cerrado, activamos edición
                else
                {
                    AppConfig.Instance.Dialogs.DisplayToast("Edición activada");
                    Save = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:Edit]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        /// <summary>
        /// Imprimir albarán
        /// </summary>
        public ICommand PrintCommand => new Command(async () => await Print());
        private async Task Print()
        {
            try
            {
                IsBusy = true;
                await Task.Delay(100);

                //obtener texto para imprimir
                List<TextPrinterModel> listTextDeliveryNote = GetTextToPrintDeliveryNote();

                //ir a pantalla de imprimir
                AppConfig.Instance.Navigations.GoToPageBeginInvoke(page: new SelectedPrintPage(listTextToPrint: listTextDeliveryNote, autoLoadPrinter: true));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:Print]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        /// <summary>
        /// Nuevo albarán
        /// </summary>
        public ICommand NewCommand => new Command(async () => await New());
        private async Task New()
        {
            try
            {
                IsBusy = true;
        
                // si tenemos modelo
                if (GenericModel != null)
                {
                    // eliminamos cabecera y líneas
                    await GenericModel.DeleteHeadAndLinesGenericModelBBDDByGuid().ConfigureAwait(false);

                    //no guardar los cambios en BBDD
                    SaveChangesBBDD = false;

                    //Reiniciar
                    await Delete().ConfigureAwait(false);

                    await Task.Delay(100).ConfigureAwait(false);

                    //guardar cambios de nuevo en BBDD
                    SaveChangesBBDD = true;

                    //actualizar dispositivo
                    GenericModel.Dispositivo = AppConfig.Instance.Globals.Device;

                    //Actualizar nº de albarán
                    NumDeliveryNote = await AlbaranCbModel.GetNextNumDeliverynoteBBDD(NumDeliveryNote).ConfigureAwait(false);

                    AppConfig.Instance.Dialogs.DisplayToast("Nuevo albarán");

                    //Restablecer edición
                    EditCommand.Execute(null);
                }
                //si no se han eliminado correctamente la cabecera y líneas
                else
                {
                    AppConfig.Instance.Dialogs.DisplayToast("No se ha podido iniciar un nuevo albarán");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:New]Error:" + ex.Message);
            }
            finally
            {
                SaveChangesBBDD = true;
                IsBusy = false;
            }
        }

        /// <summary>
        /// Eliminar albarán
        /// </summary>
        public ICommand DeleteCommand => new Command(async () => await DeleteAndLoadLast());
        /// <summary>
        /// Eliminar y cargar último
        /// </summary>
        /// <param name="newDelivery"></param>
        /// <returns></returns>
        private async Task DeleteAndLoadLast()
        {
            try
            {
                // si confirma
                if (await AppConfig.Instance.Dialogs.DisplayYesNo(message: "¿Está seguro que desea eliminar todos los datos del albarán actual y volver al último?"))
                {
                    //// resetear y guardar campos si no venimos desde New               
                    //await ResetAndSaveDataDelivery().ConfigureAwait(false);

                    // indicamos que no hemos enviado y que hemos borrado cabecera y líneas
                    GenericModel.DeliveryNote.TagEnviado = 0;
                    GenericModel.DeliveryNote.TagBorrado = Generators.TagGenerator();
                    GenericModel.DeliveryNote.UpdateListDeliveryNoteLineToSend(listDeliveryNoteLn: ListDeliveryNoteLn, updateDelete: true);

                    // meter en envío segundo plano
                    await GenericModel.DeliveryNote?.SaveToBBDD();

                    // eliminamos cabecera y líneas
                    await GenericModel.DeleteHeadAndLinesGenericModelBBDDByGuid().ConfigureAwait(false);

                    // nos vamos al último guardado
                    await ChangeToSelectDelivery(NavigateDeliveryNoteEnum.LastSave).ConfigureAwait(false);

                    // establecemos modo de guardado con los botones necesarios
                    GenericModel.Save = Save = true;

                    // guardar en BBDD
                    await GenericModel.SaveGenericModelBBDD(genericModel: GenericModel, saveDeliveryNote: true, saveListDeliveryNoteLine: true).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:DeleteAndLoadLast]Error:" + ex.Message);
            }
        }

        private async Task Delete()
        {
            try
            {         
                // inicializar listados
                await InitializeFields().ConfigureAwait(false);                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:Delete]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Enviar/guardar albarán
        /// </summary>
        public ICommand SendCommand => new Command(async () => await Send());
        private async Task Send()
        {
            try
            {
                IsBusy = true;
                SaveChangesBBDD = true;

                //Inicializar variables
                AlbaranCbModel albaranCbModel = null;
                ParametrosInvoiceCreateOutModel parametrosInvoiceCreateOut = null;
                string fieldError = HasFieldError();

                // si no hay campo con error
                if (string.IsNullOrEmpty(fieldError))
                {
                    //generar cabecera de albarán
                    albaranCbModel = GenericModel.DeliveryNote;

                    //actualizar campos para enviar
                    albaranCbModel.UpdateFieldsToSend();

                    // actualizamos campos de las líneas 
                    albaranCbModel.UpdateListDeliveryNoteLineToSend(ListDeliveryNoteLn);

                    // enviar cabecera y líneas a facturar
                    if (albaranCbModel.Cobrado)
                    {
                        if (await AppConfig.Instance.Dialogs.DisplayYesNo(
                            title: "Albarán cobrado",
                            message: "¿Está seguro que desea generar la factura de este albarán?"))
                        {
                            // enviar a albarán y líneas a facturar
                            parametrosInvoiceCreateOut = await AlbaranCbModel.InsertAlbaranYLineasWS(albaranCbModel);

                            // si se ha facturado
                            if (parametrosInvoiceCreateOut?.ResultadoOK ?? false)
                            {
                                // asignar campos de factura a albarán
                                albaranCbModel.UpdateFieldsByInvoice(parametrosInvoiceCreateOut);

                                // indicamos que se ha guardado el albarán
                                albaranCbModel.Guardado = true;

                                //guardar cabecera y líneas de albarán
                                await albaranCbModel.SaveToBBDD(updateTagEnviado: true);

                                //confirmar envío
                                AppConfig.Instance.Dialogs.DisplayToast($"Albarán {albaranCbModel.NAlbaran} facturado");
                                AppConfig.Instance.Dialogs.DisplayToast("Edición desactivada");

                                // indicamos que se ha guardado
                                Save = true;

                                // guardar en bbdd
                                SaveGenericModelInBBDDAsync();
                            }
                            // si no se ha facturado
                            else
                                AppConfig.Instance.Dialogs.DisplayAlert(message: $"No se ha podido enviar el albarán a facturar. Por favor, inténtelo de nuevo o envíelo sin cobrar. (Error: {parametrosInvoiceCreateOut?.Error})");
                        }
                    }
                    else
                    {
                        // indicamos que se ha guardado el albarán
                        albaranCbModel.Guardado = true;

                        //guardar cabecera y líneas de albarán
                        await albaranCbModel.SaveToBBDD();

                        //confirmar envío
                        AppConfig.Instance.Dialogs.DisplayToast("Cabecera enviada y líneas guardadas");
                        AppConfig.Instance.Dialogs.DisplayToast("Edición desactivada");

                        // indicamos que se ha guardado
                        Save = true;

                        // guardar en bbdd
                        SaveGenericModelInBBDDAsync();
                    }
                }
                //si nos falta algo por rellenar
                else
                    AppConfig.Instance.Dialogs.DisplayAlert(message: $"Falta el campo '{fieldError}' por rellenar");

            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:Send]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        /// <summary>
        /// Refrescar albarán
        /// </summary>
        public ICommand RefreshCommand => new Command(Refresh);
        private void Refresh()
        {
            try
            {
                ListDeliveryNoteLn = new List<AlbaranLnModel>(ListDeliveryNoteLn ?? new List<AlbaranLnModel>());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:Refresh]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Mostrar/ocultar listado de clientes
        /// </summary>
        public ICommand ShowHideListCustomerCommand => new Command(() => ShowHideListCustomer());
        private void ShowHideListCustomer()
        {
            try
            {
                // si tenemos listado de clientes
                if (ListCustomer?.Count > 0)
                {
                    // asignamos listado total de clientes a clientes visibles
                    ListVisualCustomer = new ObservableCollection<ClientesModel>(ListCustomer);

                    // mostramos / ocultamos listado de clientes visibles
                    IsVisibleListCustomers = !IsVisibleListCustomers;

                    // indicamos que ya no es el primer acceso a clientes
                    if (IsFirstAccessCustomers)
                        IsFirstAccessCustomers = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:ShowHideListCustomer]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Cambio cliente seleccionado
        /// </summary>
        public ICommand ChangeSelectedCustomerCommand => new Command(() => ChangeSelectedCustomer());
        private void ChangeSelectedCustomer()
        {
            try
            {
                if (ListCustomer?.Count > 0)
                {
                    //si tenemos algo para filtrar, lo filtramos. Si no tenemos nada, mostrar el listado completo
                    ListVisualCustomer = new ObservableCollection<ClientesModel>(!string.IsNullOrEmpty(NameCustomer)
                        ? ListCustomer.Where(x => x.NombreEmpresa?.ToUpper()?.Contains(NameCustomer.ToUpper()) ?? false).ToList()
                        : ListCustomer.ToList());

                    //si no se visualiza el listado de clientes y no es el primer acceso, mostrarlo
                    if (!IsVisibleListCustomers && !IsFirstAccessCustomers)
                        IsVisibleListCustomers = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:ChangeSelectedCustomer]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Cambiar entre albaranes
        /// </summary>
        public ICommand FirstDeliveryCommand => new Command(async () => await ChangeToSelectDelivery(NavigateDeliveryNoteEnum.First).ConfigureAwait(false));
        public ICommand BeforeDeliveryCommand => new Command(async () => await ChangeToSelectDelivery(NavigateDeliveryNoteEnum.Before).ConfigureAwait(false));
        public ICommand NextDeliveryCommand => new Command(async () => await ChangeToSelectDelivery(NavigateDeliveryNoteEnum.Next).ConfigureAwait(false));
        public ICommand LastDeliveryCommand => new Command(async () => await ChangeToSelectDelivery(NavigateDeliveryNoteEnum.Last).ConfigureAwait(false));
        private async Task ChangeToSelectDelivery(NavigateDeliveryNoteEnum navigateDelivery)
        {
            try
            {
                IsBusy = true;
                SaveChangesBBDD = false;

                // consultamos todos los albaranes guardados en el dispositivo con fecha actual no sincro
                AlbaranCbModel albaranElegido = null, albaranReferencia = null;
                List<AlbaranCbModel> listadoAlbaranes = await AlbaranCbModel.GetListDeliveryNoteThisDeviceAndActualYear().ConfigureAwait(false);

                if (listadoAlbaranes?.Count > 0)
                {
                    // según a qué albarán queremos ir
                    switch (navigateDelivery)
                    {
                        case NavigateDeliveryNoteEnum.First:
                            albaranElegido = listadoAlbaranes.FirstOrDefault();
                            CanEditDelivery = false;
                            break;
                        case NavigateDeliveryNoteEnum.Before:
                            albaranReferencia = listadoAlbaranes.FirstOrDefault(x => x.RowId == GenericModel.DeliveryNote.RowId);
                            albaranElegido = Generators.GetPrevious(listadoAlbaranes, albaranReferencia);
                            CanEditDelivery = false;
                            break;
                        case NavigateDeliveryNoteEnum.Next:
                            albaranReferencia = listadoAlbaranes.FirstOrDefault(x => x.RowId == GenericModel.DeliveryNote.RowId);
                            albaranElegido = Generators.GetNext(listadoAlbaranes, albaranReferencia);
                            CanEditDelivery = false;
                            break;
                        case NavigateDeliveryNoteEnum.Last:                     
                            albaranElegido = listadoAlbaranes.LastOrDefault();
                            CanEditDelivery = true;
                            break;
                        case NavigateDeliveryNoteEnum.LastSave:
                            albaranElegido = listadoAlbaranes.LastOrDefault(x => x.Guardado);
                            CanEditDelivery = true;
                            break;
                    }

                    if (albaranElegido != null)
                    {
                        GenericModel.DeliveryNote = albaranElegido;
                        GenericModel.NumDeliveryNote = GenericModel.DeliveryNote.GetNumDeliveryNote();
                        GenericModel.DateDeliveryNote = GenericModel.DeliveryNote.GetDateDeliveryNote();
                        GenericModel.ListDeliveryNoteLns = await AlbaranLnModel.GetListDeliveryNoteLineByRowIdAlbaranCbModel(GenericModel.DeliveryNote.RowId).ConfigureAwait(false);
                        GenericModel.DeliveryNote.GuidGenericModel = GenericModel.GuidGenericModel;
                        GenericModel.ListDeliveryNoteLns = GenericModel.ListDeliveryNoteLns?.Select(x => { x.GuidGenericModel = GenericModel.GuidGenericModel; return x; }).ToList();
                       await AssignDataDeliveryNoteAndLines().ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:ChangeToSelectDelivery]Error: " + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        /// <summary>
        /// Guardar GenericModel en BBDD
        /// </summary>
        public ICommand SaveGenericModelInBBDDAsyncCommand => new Command(SaveGenericModelInBBDDAsync);
        
        private void SaveGenericModelInBBDDAsync()
        {
            try
            {
                if(SaveChangesBBDD)
                    Device.BeginInvokeOnMainThread(async () => await GenericModel.SaveGenericModelBBDD(genericModel: GenericModel, saveDeliveryNote: true, saveListDeliveryNoteLine: true).ConfigureAwait(false));
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:SaveGenericModelInBBDDAsync]Error: " + ex.Message);
            }
        }
        private void SaveGenericModelInBBDD(bool save = false, bool saveDeliveryNote = true, bool saveListLine = false)
        {
            try
            {
                if (save)
                {
                    if (SaveChangesBBDD)                                  
                        Task.Run(() => GenericModel.SaveGenericModelBBDD(genericModel: GenericModel, saveDeliveryNote: saveDeliveryNote, saveListDeliveryNoteLine: saveListLine));                  
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:SaveGenericModelInBBDD]Error: " + ex.Message);
            }
        }
        #endregion

        #region Métodos

        /// <summary>
        /// Activar guardado de albarán
        /// </summary>
        private bool IsSaveDelivery()
        {
            try
            {
                return GenericModel?.DeliveryNote?.Guardado ?? false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:IsSaveDelivery]Error: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Resetear y guardar datos del albarán
        /// </summary>
        private async Task ResetAndSaveDataDelivery()
        {
            try
            {
                if(GenericModel != null)
                {
                    //Restablecer fecha
                    DateDeliveryNote = DateTime.Now;

                    // resetear campos albarán
                    GenericModel.DeliveryNote.FechaAlbaran = DateTime.Now.ToString("dd/MM/yyyy");
                    GenericModel.DeliveryNote.Entidad = string.Empty;
                    GenericModel.DeliveryNote.FormaPago = "Contado";
                    GenericModel.DeliveryNote.Recargo = 0;
                    GenericModel.DeliveryNote.Cliente = AppConfig.Instance.Globals.CustomerDefault ?? string.Empty;
                    GenericModel.DeliveryNote.Cobrado = false;
                    GenericModel.DeliveryNote.Rappel = 0;
            
                    // eliminar líneas de albaran con TagBorrado
                    DeleteAllDeliveryNoteLines();
                    GenericModel.ListDeliveryNoteLns = ListDeliveryNoteLn ?? new List<AlbaranLnModel>();

                    // guardar en BBDD
                    await GenericModel.SaveGenericModelBBDD(genericModel: GenericModel, saveDeliveryNote: true, saveListDeliveryNoteLine: true).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:ResetAndSaveDataDelivery]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Asignar datos a albarán y líneas
        /// </summary>
        /// <returns></returns>
        private async Task AssignDataDeliveryNoteAndLines()
        {
            try
            {
                //comprobamos si hemos cambiado el dispositivo
                await CheckChangeDevice().ConfigureAwait(false);

                //recuperar nº y fecha de albarán
                NumDeliveryNote = GenericModel.NumDeliveryNote;
                DateDeliveryNote = GenericModel.DateDeliveryNote;
                Save = GenericModel.Save;
                CanEditDelivery = GenericModel.CanEditDelivery;

                //si hemos recuperado correctamente el albarán guardado a medias
                if (GenericModel.DeliveryNote != null)
                {
                    //seleccionar campos
                    IsFirstAccessCustomers = true;
                    SelectedEntity = ListEntity?.FirstOrDefault(x => x.Descripcion == GenericModel.DeliveryNote.Entidad);
                    SelectedWayToPay = ListWayToPay?.FirstOrDefault(x => x.Descripcion == GenericModel.DeliveryNote.FormaPago);
                    SelectedSurcharge = ListSurcharge?.FirstOrDefault(x => x.Recargo == GenericModel.DeliveryNote.Recargo);
                    SelectedCustomer = ListCustomer?.FirstOrDefault(x => x.NombreEmpresa == GenericModel.DeliveryNote.Cliente);

                    Charge = GenericModel.DeliveryNote.Cobrado;
                    Rappel = GenericModel.DeliveryNote.Rappel;

                    //recuperamos las líneas no eliminadas
                    ListDeliveryNoteLn = GenericModel?.ListDeliveryNoteLns?.ToList() ?? new List<AlbaranLnModel>();
                }
                //si no hemos recuperado el albarán
                else
                {
                    //inicializamos el albarán
                    InitializeDeliveryNote();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:AssignDataDeliveryNoteAndLines]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Indica si tiene campo con error
        /// </summary>
        /// <returns></returns>
        private string HasFieldError()
        {
            try
            {
                string fieldError = string.Empty;

                if (string.IsNullOrEmpty(NumDeliveryNote))
                    fieldError = "Número";
                else if (!(ListDeliveryNoteLn?.Count > 0))
                    fieldError = "Listado de artículos";
                else if (SelectedWayToPay == null)
                    fieldError = "Forma de pago";
                else if (SelectedCustomer == null)
                    fieldError = "Cliente";

                return fieldError;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:HasFieldError]Error:" + ex.Message);
                return "Todo";
            }
        }

        /// <summary>
        /// Guardar nº de albarán
        /// </summary>
        private void SaveNumDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.NumDeliveryNote = NumDeliveryNote;

                    if(GenericModel.DeliveryNote != null)
                    {
                        GenericModel.DeliveryNote.NAlbaran = $"{GenericModel.Dispositivo}.{DateTime.Now:yyyy}.{NumDeliveryNote.PadLeft(5, '0')}";
                        GenericModel.DeliveryNote.UpdateRowId();
                    }
                   

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveNumDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar fecha de albarán
        /// </summary>
        private void SaveDateDeliveryNote()
        {
            try
            {

                if (GenericModel != null)
                {
                    GenericModel.DateDeliveryNote = DateDeliveryNote;
                    GenericModel.DeliveryNote.FechaAlbaran = DateDeliveryNote.ToString("dd/MM/yyyy");
                    GenericModel.DeliveryNote.FechaRowId = DateDeliveryNote.ToString("yyyyMMdd");
                    GenericModel.DeliveryNote.UpdateRowId();

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveDateDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar cliente de albarán
        /// </summary>
        private void SaveCustomerDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.DeliveryNote.IdCliente = SelectedCustomer?.IdCliente ?? 0;
                    GenericModel.DeliveryNote.Cliente = SelectedCustomer?.NombreEmpresa ?? string.Empty;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveCustomerDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar forma de pago de albarán
        /// </summary>
        private void SaveWayToPayDeliveryNote()
        {
            try
            {

                if (GenericModel != null)
                {
                    GenericModel.DeliveryNote.FormaPago = SelectedWayToPay?.Descripcion ?? string.Empty;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveWayToPayDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar entidad de albarán
        /// </summary>
        private void SaveEntityDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.DeliveryNote.Entidad = SelectedEntity?.Descripcion ?? string.Empty;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveWayToPayDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar rappel de albarán
        /// </summary>
        private void SaveRappelDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.DeliveryNote.Rappel = Rappel;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveRappelDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar recargo de albarán
        /// </summary>
        private void SaveRechargeDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.DeliveryNote.Recargo = SelectedSurcharge?.Recargo ?? 0;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveRechargeDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar cobrado de albarán
        /// </summary>
        private void SaveChargeDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.DeliveryNote.Cobrado = Charge;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveWayToPayDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar líneas de albarán
        /// </summary>
        private void SaveListDeliveryNoteLine()
        {
            try
            {
                if (GenericModel != null && ListDeliveryNoteLn != null)
                {
                    GenericModel.ListDeliveryNoteLns = ListDeliveryNoteLn.ToList();

                    GenericModel.NLineas = GenericModel.ListDeliveryNoteLns.Count();

                    SaveGenericModelInBBDD(saveDeliveryNote: false, saveListLine: true);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveListDeliveryNoteLine]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar albarán
        /// </summary>
        private void SaveStatusDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.Save = Save;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveWayToPayDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar si editar el albarán
        /// </summary>
        private void SaveCanEditDeliveryNote()
        {
            try
            {
                if (GenericModel != null)
                {
                    GenericModel.CanEditDelivery = CanEditDelivery;

                    SaveGenericModelInBBDD();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveCanEditDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Guardar información del albarán
        /// </summary>
        private void SaveDeliveryNote()
        {
            try
            {
                if (GenericModel != null && DeliveryNote != null)
                {
                    GenericModel.DeliveryNote = DeliveryNote;

                    SaveGenericModelInBBDD();
                }           
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SaveDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Inicializar albarán
        /// </summary>
        private void InitializeDeliveryNote()
        {
            try
            {
                //inicializar variables
                GenericModel.Dispositivo = AppConfig.Instance.Globals.Device;
                string numAlbaranCompuesto = $"{GenericModel.Dispositivo}.{DateTime.Now:yyyy}.{(NumDeliveryNote ?? "0").PadLeft(5, '0')}";
                DateTime fecha = DateTime.Now;
                bool cobrado = false;
                Rappel = 0;

                // inicializar cliente 
                IsFirstAccessCustomers = true;
                SelectedCustomer = ListCustomer?.FirstOrDefault(x => x.NombreEmpresa == AppConfig.Instance.Globals.CustomerDefault);

                // inicializar recargo
                if (SelectedSurcharge == null)
                    SelectedSurcharge = ListSurcharge.FirstOrDefault(x => x.Recargo == (SelectedCustomer?.Recargo ?? 0));              
               
                //inicializar forma de pago
                if (SelectedWayToPay == null)
                    SelectedWayToPay = ListWayToPay.FirstOrDefault(x => x.Descripcion == "Contado");

                //albarán
                DeliveryNote = new AlbaranCbModel(
                    fecha: fecha,
                    nAlbaran: numAlbaranCompuesto,
                    idCliente: SelectedCustomer?.IdCliente ?? 0,
                    formaPago: SelectedWayToPay?.Descripcion ?? string.Empty,
                    fechaRegistro: DateTime.Now.ToString("dd/MM/yyyy"),
                    horaRegistro: DateTime.Now.ToString("HH:mm:ss"),
                    cliente: SelectedCustomer?.NombreEmpresa ?? string.Empty,
                    serieFactura: string.Empty,
                    fechaFactura: Generators.GetDefaultDate(),
                    factura: string.Empty,
                    cerrado: false,
                    nPedido: 0,
                    entidad: SelectedEntity?.Descripcion ?? string.Empty,
                    cobrado: cobrado,
                    recargo: SelectedSurcharge?.Recargo ?? 0,
                    rappel: Rappel,
                    tagPropio: Utilities.Generators.TagGenerator(),
                    tagReenvio: 0,
                    tagBorrado: 0,
                    dispositivo: GenericModel.Dispositivo,
                    tagEnviado: 0,
                    guidGenericModel: GenericModel?.GuidGenericModel ?? Guid.NewGuid()
                );

                DateDeliveryNote = fecha;
                Charge = cobrado;
        
                ListDeliveryNoteLn = new List<AlbaranLnModel>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:InicializeDeliveryNote]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Borrar todas las líneas de albarán
        /// </summary>
        private void DeleteAllDeliveryNoteLines()
        {
            try
            {
                //Si tenemos líneas las borramos
                if (ListDeliveryNoteLn?.Count > 0)
                {
                    //actualizado
                    foreach (AlbaranLnModel deliveryNoteLine in ListDeliveryNoteLn)
                    {
                        deliveryNoteLine.TagBorrado = Utilities.Generators.TagGenerator();
                    }

                    //refrescar listado
                    RefreshCommand.Execute(null);
                }
                //si no tiene, inicializamos el listado
                else
                    ListDeliveryNoteLn = new List<AlbaranLnModel>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:DeleteAllDeliveryNoteLines]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Obtener texto para imprimir el albarán en la impresora
        /// </summary>
        /// <returns></returns>
        private List<TextPrinterModel> GetTextToPrintDeliveryNote()
        {
            try
            {
                //inicializar variables
                List<TextPrinterModel> listTextToPrint = new List<TextPrinterModel>();
                TextPrinterModel head = null;
                List<TextPrinterModel> lines = new List<TextPrinterModel>();
                TextPrinterModel lineHeader = null;
                TextPrinterModel lineFooter = null;
                TextPrinterModel iva = null;
                TextPrinterModel totalsHeader = null;
                TextPrinterModel totals = null;
                TextPrinterModel idQrInvoice = null;
                TextPrinterModel qrInvoice = null;
   
                //TextPrinterModel logo = null;
                //string pathLogo = "Logo_grande_embedded.png";

                AlbaranCbModel albaranCbModel = GenericModel?.DeliveryNote;
                List<AlbaranLnModel> listAlbaranLn = ListObservableDeliveryNoteLn?.ToList();

                //rellenar logo
                //logo = TextPrinterModel.GetLogo(pathLogo);

                if (albaranCbModel != null)
                {
                    //rellenar cabecera
                    head = albaranCbModel.GetHeadDeliveryNote();
                
                    if (listAlbaranLn?.Count > 0)
                    {
                        //cabecera de líneas
                        lineHeader = AlbaranLnModel.GetTextHeaderDeliveryNoteLines();

                        //rellenar líneas
                        lines = AlbaranLnModel.GetTextDeliveryNoteLines(listAlbaranLn);

                        //pie de líneas
                        lineFooter = AlbaranLnModel.GetTextFooterDeliveryNoteLines(listAlbaranLn);

                        //rellenar iva
                        iva = AlbaranLnModel.GetTextIvaDeliveryNoteLines(listAlbaranLn);
                    }

                    //rellenar totales
                    totalsHeader = albaranCbModel.GetTotalsHeaderDeliveryNote();
                    totals = albaranCbModel.GetTotalsDeliveryNote(
                        _sumaImporte: TotalAmount,
                        _rappel: TotalRappel,
                        _subTotal: SubTotal,
                        _recargo: TotalSurcharge,
                        _iva: TotalIva,
                        _totalAlbaran: TotalDeliveryNote);

                    //rellenar pagado
                    //charged = albaranCbModel.GetChargedDeliveryNote();

                    // rellenar id qr
                    idQrInvoice = albaranCbModel.GetIdQRTBaiDeliveryNote();

                    // rellenar qr
                    qrInvoice = albaranCbModel.GetQRTBaiDeliveryNote();

                    //rellenar cliente
                    //customer = albaranCbModel.GetCustomerDeliveryNote(customer: SelectedCustomer);
                }

                // rellenar texto
                //listTextToPrint.Add(logo);

                // título
                listTextToPrint.Add(TextPrinterModel.GetTitle());

                // info empresa
                listTextToPrint.Add(TextPrinterModel.UnionListTextPrinterModel(new List<TextPrinterModel>()
                {
                    TextPrinterModel.GetInfoCompany(),
                    TextPrinterModel.GetSeparator()
                }));

                // cabeceras
                listTextToPrint.Add(TextPrinterModel.UnionListTextPrinterModel(new List<TextPrinterModel>()
                {
                    head,
                    TextPrinterModel.GetSeparator(numSeparator: 2),
                    lineHeader,
                    TextPrinterModel.GetSeparator()
                }));
       
                // líneas
                listTextToPrint.AddRange(lines);

                // totales e iva
                listTextToPrint.Add(TextPrinterModel.UnionListTextPrinterModel(new List<TextPrinterModel>()
                {
                    lineFooter,
                    TextPrinterModel.GetSeparator(numSeparator: 2),
                    totalsHeader,
                    TextPrinterModel.GetSeparator(),
                    totals,
                    TextPrinterModel.GetSeparator(numSeparator: 2),
                    iva,
                    TextPrinterModel.GetSeparator(numWhiteLine: 1)
                }));

                // si tenemos url de la factura de ticket bai, añadimos id y qr
                if (!string.IsNullOrEmpty(albaranCbModel.TBaiUrl))
                {
                    listTextToPrint.Add(qrInvoice);
                    listTextToPrint.Add(idQrInvoice);
                }          
                
                // final
                listTextToPrint.Add(TextPrinterModel.GetFooter());

                return listTextToPrint;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:GetTextToPrintDeliveryNote]Error:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Comprobar cambio de dispositivo
        /// </summary>
        private async Task CheckChangeDevice()
        {
            try
            {
                //comprobamos si hemos cambiado el dispositivo
                if (GenericModel.Dispositivo != AppConfig.Instance.Globals.Device)
                {
                    GenericModel.Dispositivo = AppConfig.Instance.Globals.Device;
                    GenericModel.NumDeliveryNote = await AlbaranCbModel.GetNextNumDeliverynoteBBDD(NumDeliveryNote).ConfigureAwait(false);
                    GenericModel.DeliveryNote.NAlbaran = $"{GenericModel.Dispositivo}.{DateTime.Now:yyyy}.{(GenericModel.NumDeliveryNote ?? "0").PadLeft(5, '0')}";
                    GenericModel.DeliveryNote.Dispositivo = GenericModel.Dispositivo;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:CheckChangeDevice]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Establecer recargo y nombre del cliente
        /// </summary>
        private void SetSurchargeAndNameCustomer()
        {
            try
            {
                double surcharge = 0;
                string nameCustomer = string.Empty;

                // si hemos seleccionado un cliente, establecemos su recargo y nombre del cliente
                if (SelectedCustomer != null && !string.IsNullOrEmpty(SelectedCustomer.NombreEmpresa))
                {
                    // asignar recargo
                    surcharge = SelectedCustomer.Recargo;

                    // asignar nombre del cliente
                    nameCustomer = SelectedCustomer.NombreEmpresa;
                }

                // asignar el recargo si no es el primer acceso
                if(!IsFirstAccessCustomers)
                    SelectedSurcharge = ListSurcharge.FirstOrDefault(x => x.Recargo == surcharge);

                // asignar nombre del cliente
                NameCustomer = nameCustomer;

                // ocultamos el listado de clientes
                IsVisibleListCustomers = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteViewModel:SetSurchargeAndNameCustomer]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Elegir línea de albarán
        /// </summary>
        private void ChooseDeliveryNoteLn()
        {
            try
            {
                // si la opción está activada y es una línea de albarán ir a editar
                if (EnabledOption && SelectedDeliveryNoteLn != null && SelectedDeliveryNoteLn is AlbaranLnModel)              
                    EditDeliveryNoteLineCommand.Execute((AlbaranLnModel)SelectedDeliveryNoteLn);

                // volver a null para que no esté elegida
                if (SelectedDeliveryNoteLn != null)
                    SelectedDeliveryNoteLn = null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:ChooseDeliveryNoteLn]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Buscar por albarán
        /// </summary>
        private void SearchByDeliveryNote(int numDeliveryNote)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[DeliveryNoteViewModel:SearchByDeliveryNote]Error: " + ex.Message);
            }
        }
        #endregion
    }
}
