﻿using AppUrdaira.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppUrdaira.ViewModels
{
    public class StockLotsViewModel : BaseViewModel
    {
        #region Propiedades
        public string Seasons { get; set; } = null;

        //Listado de lotes
        private List<LoteLnExtendedModel> listLots = null;
        public List<LoteLnExtendedModel> ListLots
        {
            get { return listLots; }
            set
            {
                listLots = value;
                OnPropertyChanged(nameof(ListLots));
                OnPropertyChanged(nameof(TotalStockB));
                OnPropertyChanged(nameof(TotalStockL));
                OnPropertyChanged(nameof(TotalStockActualB));
                OnPropertyChanged(nameof(TotalStockActualL));
                OnPropertyChanged(nameof(ListObservableLots));
            }
        }
        public ObservableCollection<LoteLnExtendedModel> ListObservableLots => new ObservableCollection<LoteLnExtendedModel>(ListLots ?? new List<LoteLnExtendedModel>());
        public int TotalStockB => ListLots?.Sum(x => x.CantidadBotella) ?? 0;
        public double TotalStockL => ListLots?.Sum(x => x.CantidadLitros) ?? 0;
        public int TotalStockActualB => ListLots?.Sum(x => x.CantidadActualBotella) ?? 0;
        public double TotalStockActualL => ListLots?.Sum(x => x.CantidadActualLitros) ?? 0;
        #endregion

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public StockLotsViewModel(List<LoteLnExtendedModel> listLots)
        {
            try
            {
                // inicializar variables
                Title = "Stock";
                Seasons = AppConfig.Instance.Globals.Seasons;
                ListLots = listLots ?? new List<LoteLnExtendedModel>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LotsViewModel:()]Error:" + ex.Message);
            }
        }
    }
}
