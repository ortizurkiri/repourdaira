﻿using AppUrdaira.Models;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppUrdaira.ViewModels
{
    public class LotsViewModel : BaseViewModel
    {
        #region Propiedades
        private string seasons;

        public string Seasons
        {
            get { return seasons; }
            set { seasons = value; OnPropertyChanged(nameof(Seasons)); }
        }

        //Listado de lotes
        private List<LoteLnExtendedModel> listLots = null;
        public List<LoteLnExtendedModel> ListLots
        {
            get { return listLots; }
            set
            {
                listLots = value;
                OnPropertyChanged(nameof(ListLots));
                OnPropertyChanged(nameof(ListObservableLots));
                //OnPropertyChanged(nameof(TotalStockB));
                //OnPropertyChanged(nameof(TotalStockL));
                //OnPropertyChanged(nameof(TotalStockActualB));
                //OnPropertyChanged(nameof(TotalStockActualL));
            }
        }
        public ObservableCollection<LoteLnExtendedModel> ListObservableLots => new ObservableCollection<LoteLnExtendedModel>(ListLots ?? new List<LoteLnExtendedModel>());

        //public int TotalStockB => ListLots?.Sum(x => x.CantidadBotella) ?? 0;
        //public double TotalStockL => ListLots?.Sum(x => x.CantidadLitros) ?? 0;
        //public int TotalStockActualB => ListLots?.Sum(x => x.CantidadActualBotella) ?? 0;
        //public double TotalStockActualL => ListLots?.Sum(x => x.CantidadActualLitros) ?? 0;

        public Command SetSeasonsCommand { get; }
        public Command ShowLotsCommand { get; }
        public Command ShowPageStockCommand { get; }
        #endregion

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public LotsViewModel()
        {
            try
            {
                // inicializar variables
                Title = "Lotes";
                Seasons = AppConfig.Instance.Globals.Seasons;

                // commands
                SetSeasonsCommand = new Command(SetSeasons);
                ShowLotsCommand = new Command(async () => await ShowLots());
                ShowPageStockCommand = new Command(ShowPageStock);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LotsViewModel:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Cambiar las temporadas 
        /// </summary>
        private void SetSeasons()
        {
            try
            {
                AppConfig.Instance.Globals.Seasons = Seasons;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LotsViewModel:SetSeasons]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Visualizar lotes
        /// </summary>
        private async Task ShowLots()
        {
            try
            {
                IsBusy = true;

                //asignar lotes
                ListLots = await LoteLnExtendedModel.GetListLotExtendedBBDD();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LotsViewModel:ShowLots]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        /// <summary>
        /// Visualizar página de stock
        /// </summary>
        private void ShowPageStock()
        {
            try
            {
                //ir a pantalla de imprimir
                AppConfig.Instance.Navigations.GoToPageBeginInvoke(
                    page: new StockLotsPage(listLots: ListLots), 
                    actionBefore: async () =>
                    {
                        //asignar lotes si no los hemos obtenido antes
                        if (!(ListLots?.Count > 0))
                            await ShowLots().ConfigureAwait(false);
                    }
                );
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LotsViewModel:ShowPageStock]Error:" + ex.Message);
            }
        }
    }
}
