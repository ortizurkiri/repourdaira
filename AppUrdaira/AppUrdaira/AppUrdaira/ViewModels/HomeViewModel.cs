﻿using AppUrdaira.Models;
using AppUrdaira.Utilities;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppUrdaira.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public HomeViewModel()
        {
            try
            {
                //Inicializar variables
                Title = "Inicio";
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[HomeViewModel:()]Error:" + ex.Message);
            }
        }

        public ICommand OpenDeliveryNoteCommand => new Command(OpenDeliveryNote);
        /// <summary>
        /// Abrir la página de albaranes
        /// </summary>
        private async void OpenDeliveryNote()
        {
            try
            {
                IsBusy = true;

                // inicializar variable
                string errorNumAlbaran = null;

                // verificamos si queremos comprobar el nº de albarán
                bool comprobarNumAlbaran = AppConfig.Instance.Globals.CheckNumAlbaranWSLocal;

                // si queremos comprobar nº de albarán, realizamos la consulta en WS y SQLite
                if (comprobarNumAlbaran)
                    errorNumAlbaran = await AlbaranCbModel.ComprobarNumAlbaranSegunNAlbaranWSYSQLite().ConfigureAwait(false);
                
                // si no tenemos mensaje de error
                if (string.IsNullOrEmpty(errorNumAlbaran))
                {
                    // si tenemos activada la comprobación, la desactivamos
                    if (comprobarNumAlbaran)
                        AppConfig.Instance.Globals.CheckNumAlbaranWSLocal = false;

                    // nos vamos a página de albarán
                    AppConfig.Instance.Navigations.ShellGoToPageBeginInvoke(nameOfPage: nameof(DeliveryNotePage), actionFinally: () => IsBusy = false);

                    // intentamos reconectar impresora
                    PrinterModel.ReconectarImpresora();
                }
                // si tenemos mensaje de error, mostrarlo
                else
                {
                    AppConfig.Instance.Dialogs.DisplayAlert(title: "Error", message: errorNumAlbaran);
                    IsBusy = false;
                }              
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[HomeViewModel:OpenDeliveryNote]Error:" + ex.Message);
                IsBusy = false;
            }
        }

        public ICommand ExportBBDDCommand => new Command(ExportBBDD);
        /// <summary>
        /// Exportar BBDD
        /// </summary>
        private async void ExportBBDD()
        {
            try
            {
                IsBusy = true;

                await Task.Delay(100).ConfigureAwait(false);

                string exportBBDD = AppConfig.Instance.FileHelper.Copy(pathFile: Constants.DatabasePath, file: Constants.DatabaseFilename);

                AppConfig.Instance.Dialogs.DisplayAlert(message: !string.IsNullOrEmpty(exportBBDD) ? $"La base de datos se ha exportado en '{exportBBDD}'" : "La base de datos no se ha conseguido exportar");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[HomeViewModel:ExportBBDD]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public ICommand SelectedPrinterCommand => new Command(SelectedPrinter);
        /// <summary>
        /// Seleccionar impresora bluetooth
        /// </summary>
        private async void SelectedPrinter()
        {
            try
            {
                IsBusy = true;

                //ir a pantalla de impresora
                await AppConfig.Instance.Navigations.GoToPageInvokeAsync(page: new SelectedPrintPage());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[HomeViewModel:SelectedPrinter]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
