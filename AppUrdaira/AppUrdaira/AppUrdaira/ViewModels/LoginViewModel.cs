﻿using AppUrdaira.Models;
using AppUrdaira.Utilities;
using AppUrdaira.Views;
using Org.Apache.Http.Authentication;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppUrdaira.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        #region Propiedades
        private UserModel usuarioModel;
        public UserModel UsuarioModel
        {
            get { return usuarioModel; }
            set { usuarioModel = value; OnPropertyChanged(nameof(UsuarioModel)); }
        }

        public string IdDevice = string.Empty;

        private string versionApp;
        public string VersionApp
        {
            get { return versionApp; }
            set { versionApp = value; OnPropertyChanged(nameof(VersionApp)); }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public LoginViewModel()
        {
            try
            {
                //Inicializar variables
                UsuarioModel = new UserModel();
                IdDevice = AppConfig.Instance.Globals.Device;
                VersionApp = GetVersionApp();
                AppConfig.Instance.Globals.InicializarPreferencias();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginViewModel:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Inicializar
        /// </summary>
        public void Initialize()
        {
            try
            {
                UsuarioModel = new UserModel
                {
                    Usuario = string.Empty,
                    Password = string.Empty
                };
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginViewModel:Initialize]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Obtener versión de app
        /// </summary>
        /// <returns></returns>
        private string GetVersionApp()
        {
            try
            {
                return $"Versión: {VersionTracking.CurrentVersion} ({VersionTracking.CurrentBuild})";
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginViewModel:GetVersionApp]Error:" + ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Comprueba si el usuario es válido
        /// </summary>
        /// <returns></returns>
        private async Task<bool> CheckValidUser()
        {
            try
            {
                bool validUser = false;

                //si el usuario no tiene los campos vacíos, lo consideramos válido
                if (UsuarioModel != null && !string.IsNullOrEmpty(UsuarioModel.Usuario) && !string.IsNullOrEmpty(UsuarioModel.Password))
                {
                    validUser = await Login().ConfigureAwait(false);
                }


                return validUser;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginViewModel:CheckUserValid]Error:" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Descargar listas usadas en la app
        /// </summary>
        /// <returns></returns>
        private async Task DownloadList(bool showMsg = false)
        {
            try
            {
                await AppConfig.Instance.SyncService.SyncInitialList(showMsg).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginViewModel:DownloadList]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Descargar listas usadas en la app
        /// </summary>
        /// <returns></returns>
        private async Task<bool> Login()
        {
            try
            {
                bool validUser = false;

                //validar usuario en local
                validUser = await UserModel.ValidLoginLocal(UsuarioModel.Usuario, UsuarioModel.Password);

                //si no ha sido validado en local, probamos en remoto
                if (!validUser)
                    validUser = await WSModel.Login(UsuarioModel.Usuario, UsuarioModel.Password);

                return validUser;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginViewModel:Login]Error:" + ex.Message);
                return false;
            }
        }

        #region Commands
        public ICommand LoginCommand => new Command(() => OnLoginClicked());
        public ICommand LoginAutoCommand => new Command(() => OnLoginClicked(true));
        /// <summary>
        /// Evento al pulsar el botón login
        /// </summary>
        private async void OnLoginClicked(bool auto = false)
        {
            try
            {
                IsBusy = true;

                await Task.Delay(100);

                //si hacemos login automático, introducimos directamente el usuario y password
                if (auto)
                {
#if DEBUG
                    UsuarioModel.Usuario = "811";
                    UsuarioModel.Password = "8110";
#endif
                }

                //si el usuario es válido
                if(await CheckValidUser())
                {
                    //asignamos el usuario logueado
                    AppConfig.Instance.Globals.LoginUser = new UserModel(UsuarioModel.Usuario, UsuarioModel.Password);

                    //si el dispositivo no tiene nombre
                    //if (string.IsNullOrEmpty(IdDevice) || IdDevice == "PXX")
                    //{
                    //ejecutar sincro y esperar a que termine
                    AppConfig.Instance.Dialogs.DisplayToast("Inicio sincro. Por favor, espere unos segundos...");
                    await DownloadList(showMsg: string.IsNullOrEmpty(IdDevice) || IdDevice == Constants.DeviceInit).ConfigureAwait(false);
                    AppConfig.Instance.Dialogs.DisplayToast("Fin sincro.", false);
                    //}
                    ////si el dispositivo ya tiene nombre
                    //else
                    //{
                    //    //ejecutar sincro en segundo plano
                    //    _ = Task.Run( () => DownloadList());
                    //}

                    await Device.InvokeOnMainThreadAsync(async () =>
                    {
                        try
                        {
                            //si el dispositivo no tiene nombre
                            if (string.IsNullOrEmpty(IdDevice) || IdDevice == Constants.DeviceInit)
                            {
                                // nos vamos a pantalla del dispositivo
                                await Shell.Current.GoToAsync($"//{nameof(InfoDevicePage)}?InitialConfig=true");
                            }
                            //si el dispositivo ya tiene nombre
                            else
                            {
                                // nos vamos a pantalla de home
                                await Shell.Current.GoToAsync($"//{nameof(HomePage)}");
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("[LoginViewModel:OnLoginClicked:d]Error:" + ex.Message);
                        }
                    });
                }
                else
                    AppConfig.Instance.Dialogs.DisplayToast("Error en usuario o contraseña.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[LoginViewModel:OnLoginClicked]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }
        #endregion
    }
}
