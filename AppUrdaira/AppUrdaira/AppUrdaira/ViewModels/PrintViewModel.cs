﻿using AppUrdaira.Models;
using Shiny.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppUrdaira.ViewModels
{
    public class PrintViewModel : BaseViewModel
    {
        #region Propiedades 
        //public IPeripheral Peripheral { get; set; }

        IDisposable _perifDisposable;
        IDisposable _connectDisposable;
        IDisposable _disConnectDisposable;
        IGattCharacteristic _savedCharacteristic;

        public bool IsReadyToPrint { get; set; } = true;

        public string TextToPrintTest { get; set; } =
          "\n-----------------------------------------------\n" +
            "<h1>Total</h1>---------------2 € á é í ó ú----\n" +
          "\n-----------------------------------------------\n\n";

        public string TextToPrintTest2 { get; set; } =
      "\n-----------------------------------------------\n" +
        "--------------------Urdaira--------------------\n" +
        "Cervesita fresquita-----------------1 euros----\n" +
        "Cervesita fresquita-----------------1 euros----\n" +
        "Total-------------------------------2 € áéíóú----\n" +
        "\n" +
        "Prueba para Iker ------------------------------\n" +
      "\n-----------------------------------------------\n\n";

        private List<TextPrinterModel> _listTextToPrint;
        public List<TextPrinterModel> ListTextToPrint
        {
            get { return _listTextToPrint; }
            set { _listTextToPrint = value; OnPropertyChanged(nameof(TextToPrint)); }
        }

        private string _textToPrint = string.Empty;
        public string TextToPrint
        {
            get { return _textToPrint; }
            set { _textToPrint = value; OnPropertyChanged(nameof(TextToPrint)); }
        }
        #endregion
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="peripheral"></param>
        public PrintViewModel(List<TextPrinterModel> listTextToPrint)
        {
            try
            {
                ListTextToPrint = listTextToPrint?.Count > 0 ? listTextToPrint : new List<TextPrinterModel>() { new TextPrinterModel(TextToPrintTest) };
                TextToPrint = string.Concat(ListTextToPrint.Select(x => x?.Text ?? string.Empty));
                ConnectDeviceCommand.Execute(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintViewModel:()]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Conectar con impresora
        /// </summary>
        public ICommand ConnectDeviceCommand => new Command(ConnectPrinter);
        private void ConnectPrinter()
        {
            try
            {
                //Conectar impresora
                if(!AppConfig.Instance.Globals.Peripheral.IsConnected())
                    AppConfig.Instance.Globals.Peripheral.Connect();

                //Eventos
                SuscripcionEventosImpresora();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintViewModel:ConnectPrinter]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Desconectar con impresora
        /// </summary>
        public ICommand DisconnectDeviceCommand => new Command(DisconnectPrinter);
        private void DisconnectPrinter()
        {
            try
            {
                _perifDisposable?.Dispose();
                _connectDisposable?.Dispose();
                _disConnectDisposable?.Dispose();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintViewModel:DisconnectPrinter]Error: " + ex.Message);
            }
        }

        private byte[] GetBytesLogo()
        {
            try
            {
                //string base64 = "Qk3SUQAAAAAAAJIAAAB8AAAAkAEAAJABAAABAAEAAAAAAEBRAADDDgAAww4AAAIAAAACAAAAAAD/AAD/AAD/AAAAAAAA/0JHUnOAwvUoYLgeFSCF6wFAMzMTgGZmJkBmZgagmZkJPArXAyRcjzIAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAA////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA///////////////gAP/////8AB//AAf/wAH////////AAAAP//////////gAAAAAAAAAAP//////////////4AD//////AAf/wAH/8AB////////wAAAD//////////4AAAAAAAAAAD//////////////+AA//////wAH/8AB//AAf///////8AAAA//////////+AAAAAAAAAAA///////////////gAP/////8AB//AAf/wAH////////AAAAP//////////gAAAAAAAAAAP//////////////4AD//////AAf/wAH/8AB////////wAAAD//////////4AAAAAAAAAAD//////////////+AA//////wAH/8AB//AAf///////8AAAA//////////+AAAAAAAAAAA///////////////gAP/////8AB//AAf/wAH////////AAAAP//////////gAAAAAAAAAAP//////////////4AD//////AAf/wAH/8AB////////wAAAD//////////4AAAAAAAAAAD//////////////+AA//////wAH/8AB//AAf///////8AAAA//////////+AAAAAAAAAAA///////////////gAP/////8AB//AAf/wAH////////AAAAP//////////gAAAAAAAAAAP//////////////4AD//////AAf/wAH/8AB////////wAAAD//////////4AAAAAAAAAAD//////////////+AA//////wAH/8AB//AAf///////8AAAA//////////+AAAAAAAAAAA///////////////gAP/////8AB//AAf/wAH////////AAAAP//////////gAAAAAAAAAAP/4AAAAAAAAAAP/4AD///+AAAAf///4AAAB//AAf/wAP/4AAAB//AAf/wAH/8AAAAAAAAD/+AAAAAAAAAAD/+AA////gAAAH///+AAAAf/wAH/8AD/+AAAAf/wAH/8AB//AAAAAAAAA//gAAAAAAAAAA//gAP///4AAAB////gAAAH/8AB//AA//gAAAH/8AB//AAf/wAAAAAAAAP/4AAAAAAAAAAP/4AD///+AAAAf///4AAAB//AAf/wAP/4AAAB//AAf/wAH/8AAAAAAAAD/+AAAAAAAAAAD/+AA////gAAAH///+AAAAf/wAH/8AD/+AAAAf/wAH/8AB//AAAAAAAAA//gAAAAAAAAAA//gAP///4AAAB////gAAAH/8AB//AA//gAAAH/8AB//AAf/wAAAAAAAAP/4AAAAAAAAAAP/4AD///+AAAAf///4AAAB//AAf/wAP/4AAAB//AAf/wAH/8AAAAAAAAD/+AAAAAAAAAAD/+AA////gAAAH///+AAAAf/wAH/8AD/+AAAAf/wAH/8AB//AAAAAAAAA//gAAAAAAAAAA//gAP///4AAAB////gAAAH/8AB//AA//gAAAH/8AB//AAf/wAAAAAAAAP/4AAAAAAAAAAP/4AD///+AAAAf///4AAAB//AAf/wAP/4AAAB//AAf/wAH/8AAAAAAAAD/+AAAAAAAAAAD/+AA////gAAAH///+AAAAf/wAH/8AD/+AAAAf/wAH/8AB//AAAAAAAAA//gAAAAAAAAAA//gAP///4AAAB////gAAAH/8AB//AA//gAAAH/8AB//AAf/wAAAAAAAAP/4AAAAAAAAAAP/4AD///+AAAAf///4AAAB//AAf/wAP/4AAAB//AAf/wAH/8AAAAAAAAD/+AA//////4AD/+AA////gAAAAAD////////wAH/////////////////////AAAAAAAAA//gAP/////+AA//gAP///4AAAAAA////////8AB/////////////////////wAAAAAAAAP/4AD//////gAP/4AD///+AAAAAAP////////AAf////////////////////8AAAAAAAAD/+AA//////4AD/+AA////gAAAAAD////////wAH/////////////////////AAAAAAAAA//gAP/////+AA//gAP///4AAAAAA////////8AB/////////////////////wAAAAAAAAP/4AD//////gAP/4AD///+AAAAAAP////////AAf////////////////////8AAAAAAAAD/+AA//////4AD/+AA////gAAAAAD////////wAH/////////////////////AAAAAAAAA//gAP/////+AA//gAP///4AAAAAA////////8AB/////////////////////wAAAAAAAAP/4AD//////gAP/4AD///+AAAAAAP////////AAf////////////////////8AAAAAAAAD/+AA//////4AD/+AA////gAAAAAD////////wAH/////////////////////AAAAAAAAA//gAP/////+AA//gAP///4AAAAAA////////8AB/////////////////////wAAAAAAAAP/4AD//////gAP/4AD///+AAAAAAP////////AAf////////////////////8AAAAAAAAD/+AA//////4AD/+AA////gAAAAAD////////wAH/////////////////////AAAAAAAAA//gAP/////+AA//gAP/4AH/8AB//////wAH//////AAAAAAAAH/8AB////////4AAAAAAP/4AD//////gAP/4AD/+AB//AAf/////8AB//////wAAAAAAAB//AAf///////+AAAAAAD/+AA//////4AD/+AA//gAf/wAH//////AAf/////8AAAAAAAAf/wAH////////gAAAAAA//gAP/////+AA//gAP/4AH/8AB//////wAH//////AAAAAAAAH/8AB////////4AAAAAAP/4AD//////gAP/4AD/+AB//AAf/////8AB//////wAAAAAAAB//AAf///////+AAAAAAD/+AA//////4AD/+AA//gAf/wAH//////AAf/////8AAAAAAAAf/wAH////////gAAAAAA//gAP/////+AA//gAP/4AH/8AB//////wAH//////AAAAAAAAH/8AB////////4AAAAAAP/4AD//////gAP/4AD/+AB//AAf/////8AB//////wAAAAAAAB//AAf///////+AAAAAAD/+AA//////4AD/+AA//gAf/wAH//////AAf/////8AAAAAAAAf/wAH////////gAAAAAA//gAP/////+AA//gAP/4AH/8AB//////wAH//////AAAAAAAAH/8AB////////4AAAAAAP/4AD//////gAP/4AD/+AB//AAf/////8AB//////wAAAAAAAB//AAf///////+AAAAAAD/+AA//////4AD/+AA//gAf/wAH//////AAf/////8AAAAAAAAf/wAH////////gAAAAAA//gAP/////+AA//gAP/4AH/8AB//////wAH//////AAAAAAAAH/8AB////////4AAAAAAP/4AD//////gAP/4AD/+AAAA//gAAAAAD//////gAAAP//////////gAP/4AAAAAAAAAAD/+AA//////4AD/+AA//gAAAP/4AAAAAA//////4AAAD//////////4AD/+AAAAAAAAAAA//gAP/////+AA//gAP/4AAAD/+AAAAAAP/////+AAAA//////////+AA//gAAAAAAAAAAP/4AD//////gAP/4AD/+AAAA//gAAAAAD//////gAAAP//////////gAP/4AAAAAAAAAAD/+AA//////4AD/+AA//gAAAP/4AAAAAA//////4AAAD//////////4AD/+AAAAAAAAAAA//gAP/////+AA//gAP/4AAAD/+AAAAAAP/////+AAAA//////////+AA//gAAAAAAAAAAP/4AD//////gAP/4AD/+AAAA//gAAAAAD//////gAAAP//////////gAP/4AAAAAAAAAAD/+AA//////4AD/+AA//gAAAP/4AAAAAA//////4AAAD//////////4AD/+AAAAAAAAAAA//gAP/////+AA//gAP/4AAAD/+AAAAAAP/////+AAAA//////////+AA//gAAAAAAAAAAP/4AD//////gAP/4AD/+AAAA//gAAAAAD//////gAAAP//////////gAP/4AAAAAAAAAAD/+AA//////4AD/+AA//gAAAP/4AAAAAA//////4AAAD//////////4AD/+AAAAAAAAAAA//gAP/////+AA//gAP/4AAAD/+AAAAAAP/////+AAAA//////////+AA//gAAAAAAAAAAP/4AD//////gAP/4AD/+AAAA//gAAAAAD//////gAAAP//////////gAP/4AAAAAAAAAAD/+AAAAAAAAAAD/+AAAAf/gAP/4AAAB////gAAAAAD///+AAAAAAP/4AAAB////gAAAAAA//gAAAAAAAAAA//gAAAH/4AD/+AAAAf///4AAAAAA////gAAAAAD/+AAAAf///4AAAAAAP/4AAAAAAAAAAP/4AAAB/+AA//gAAAH///+AAAAAAP///4AAAAAA//gAAAH///+AAAAAAD/+AAAAAAAAAAD/+AAAAf/gAP/4AAAB////gAAAAAD///+AAAAAAP/4AAAB////gAAAAAA//gAAAAAAAAAA//gAAAH/4AD/+AAAAf///4AAAAAA////gAAAAAD/+AAAAf///4AAAAAAP/4AAAAAAAAAAP/4AAAB/+AA//gAAAH///+AAAAAAP///4AAAAAA//gAAAH///+AAAAAAD/+AAAAAAAAAAD/+AAAAf/gAP/4AAAB////gAAAAAD///+AAAAAAP/4AAAB////gAAAAAA//gAAAAAAAAAA//gAAAH/4AD/+AAAAf///4AAAAAA////gAAAAAD/+AAAAf///4AAAAAAP/4AAAAAAAAAAP/4AAAB/+AA//gAAAH///+AAAAAAP///4AAAAAA//gAAAH///+AAAAAAD/+AAAAAAAAAAD/+AAAAf/gAP/4AAAB////gAAAAAD///+AAAAAAP/4AAAB////gAAAAAA//gAAAAAAAAAA//gAAAH/4AD/+AAAAf///4AAAAAA////gAAAAAD/+AAAAf///4AAAAAAP/4AAAAAAAAAAP/4AAAB/+AA//gAAAH///+AAAAAAP///4AAAAAA//gAAAH///+AAAAAAD/+AAAAAAAAAAD/+AAAAf/gAP/4AAAB////gAAAAAD///+AAAAAAP/4AAAB////gAAAAAA///////////////gAP/4AH//////AAAAP////////////gAP/4AD//////gAAAAAAAAAAP//////////////4AD/+AB//////wAAAD////////////4AD/+AA//////4AAAAAAAAAAD//////////////+AA//gAf/////8AAAA////////////+AA//gAP/////+AAAAAAAAAAA///////////////gAP/4AH//////AAAAP////////////gAP/4AD//////gAAAAAAAAAAP//////////////4AD/+AB//////wAAAD////////////4AD/+AA//////4AAAAAAAAAAD//////////////+AA//gAf/////8AAAA////////////+AA//gAP/////+AAAAAAAAAAA///////////////gAP/4AH//////AAAAP////////////gAP/4AD//////gAAAAAAAAAAP//////////////4AD/+AB//////wAAAD////////////4AD/+AA//////4AAAAAAAAAAD//////////////+AA//gAf/////8AAAA////////////+AA//gAP/////+AAAAAAAAAAA///////////////gAP/4AH//////AAAAP////////////gAP/4AD//////gAAAAAAAAAAP//////////////4AD/+AB//////wAAAD////////////4AD/+AA//////4AAAAAAAAAAD//////////////+AA//gAf/////8AAAA////////////+AA//gAP/////+AAAAAAAAAAA///////////////gAP/4AH//////AAAAP////////////gAP/4AD//////gAAAAAAAAAAAAAAAAAAAAAAAAAAAD//////AAAAAAAAAAAAAAAf/wAP/4AAAAAA//////////+AAAAAAAAAAAAAAAAAAAAAAAA//////wAAAAAAAAAAAAAAH/8AD/+AAAAAAP//////////gAAAAAAAAAAAAAAAAAAAAAAAP/////8AAAAAAAAAAAAAAB//AA//gAAAAAD//////////4AAAAAAAAAAAAAAAAAAAAAAAD//////AAAAAAAAAAAAAAAf/wAP/4AAAAAA//////////+AAAAAAAAAAAAAAAAAAAAAAAA//////wAAAAAAAAAAAAAAH/8AD/+AAAAAAP//////////gAAAAAAAAAAAAAAAAAAAAAAAP/////8AAAAAAAAAAAAAAB//AA//gAAAAAD//////////4AAAAAAAAAAAAAAAAAAAAAAAD//////AAAAAAAAAAAAAAAf/wAP/4AAAAAA//////////+AAAAAAAAAAAAAAAAAAAAAAAA//////wAAAAAAAAAAAAAAH/8AD/+AAAAAAP//////////gAAAAAAAAAAAAAAAAAAAAAAAP/////8AAAAAAAAAAAAAAB//AA//gAAAAAD//////////4AAAAAAAAAAAAAAAAAAAAAAAD//////AAAAAAAAAAAAAAAf/wAP/4AAAAAA//////////+AAAAAAAAAAAAAAAAAAAAAAAA//////wAAAAAAAAAAAAAAH/8AD/+AAAAAAP//////////gAAAAAAAAAAAAAAAAAAAAAAAP/////8AAAAAAAAAAAAAAB//AA//gAAAAAD//////////4AAAAAAAAAAAAAAAAAAAAAAAD//////AAAAAAAAAAAAAAAf/wAP/4AAAAAA//////////+AAAAAAD/+AA//gAP////////////gAAAH/8AB//AAf///4AAAD//////////4AD//////gAAAAAA//gAP/4AD////////////4AAAB//AAf/wAH///+AAAA//////////+AA//////4AAAAAAP/4AD/+AA////////////+AAAAf/wAH/8AB////gAAAP//////////gAP/////+AAAAAAD/+AA//gAP////////////gAAAH/8AB//AAf///4AAAD//////////4AD//////gAAAAAA//gAP/4AD////////////4AAAB//AAf/wAH///+AAAA//////////+AA//////4AAAAAAP/4AD/+AA////////////+AAAAf/wAH/8AB////gAAAP//////////gAP/////+AAAAAAD/+AA//gAP////////////gAAAH/8AB//AAf///4AAAD//////////4AD//////gAAAAAA//gAP/4AD////////////4AAAB//AAf/wAH///+AAAA//////////+AA//////4AAAAAAP/4AD/+AA////////////+AAAAf/wAH/8AB////gAAAP//////////gAP/////+AAAAAAD/+AA//gAP////////////gAAAH/8AB//AAf///4AAAD//////////4AD//////gAAAAAA//gAP/4AD////////////4AAAB//AAf/wAH///+AAAA//////////+AA//////4AAAAAAP/4AD/+AA////////////+AAAAf/wAH/8AB////gAAAP//////////gAP/////+AAAAAAD/+AA//gAP////////////gAAAH/8AB//AAf///4AAAD//////////4AD//////gAAAAAA//gAP///8AAAAAAAAAAH///8AB////gAP///8AB//AA//gAP/////+AAAAf/wAAAAAAAAP/4AD////AAAAAAAAAAB////AAf///4AD////AAf/wAP/4AD//////gAAAH/8AAAAAAAAD/+AA////wAAAAAAAAAAf///wAH///+AA////wAH/8AD/+AA//////4AAAB//AAAAAAAAA//gAP///8AAAAAAAAAAH///8AB////gAP///8AB//AA//gAP/////+AAAAf/wAAAAAAAAP/4AD////AAAAAAAAAAB////AAf///4AD////AAf/wAP/4AD//////gAAAH/8AAAAAAAAD/+AA////wAAAAAAAAAAf///wAH///+AA////wAH/8AD/+AA//////4AAAB//AAAAAAAAA//gAP///8AAAAAAAAAAH///8AB////gAP///8AB//AA//gAP/////+AAAAf/wAAAAAAAAP/4AD////AAAAAAAAAAB////AAf///4AD////AAf/wAP/4AD//////gAAAH/8AAAAAAAAD/+AA////wAAAAAAAAAAf///wAH///+AA////wAH/8AD/+AA//////4AAAB//AAAAAAAAA//gAP///8AAAAAAAAAAH///8AB////gAP///8AB//AA//gAP/////+AAAAf/wAAAAAAAAP/4AD////AAAAAAAAAAB////AAf///4AD////AAf/wAP/4AD//////gAAAH/8AAAAAAAAD/+AA////wAAAAAAAAAAf///wAH///+AA////wAH/8AD/+AA//////4AAAB//AAAAAAAAA//gAAAAAD/+AA//gAP///4AAAAAA/////////////AA//gAP///8AAAA//gAAAAAAAAAAP/4AAAAAA//gAP/4AD///+AAAAAAP////////////wAP/4AD////AAAAP/4AAAAAAAAAAD/+AAAAAAP/4AD/+AA////gAAAAAD////////////8AD/+AA////wAAAD/+AAAAAAAAAAA//gAAAAAD/+AA//gAP///4AAAAAA/////////////AA//gAP///8AAAA//gAAAAAAAAAAP/4AAAAAA//gAP/4AD///+AAAAAAP////////////wAP/4AD////AAAAP/4AAAAAAAAAAD/+AAAAAAP/4AD/+AA////gAAAAAD////////////8AD/+AA////wAAAD/+AAAAAAAAAAA//gAAAAAD/+AA//gAP///4AAAAAA/////////////AA//gAP///8AAAA//gAAAAAAAAAAP/4AAAAAA//gAP/4AD///+AAAAAAP////////////wAP/4AD////AAAAP/4AAAAAAAAAAD/+AAAAAAP/4AD/+AA////gAAAAAD////////////8AD/+AA////wAAAD/+AAAAAAAAAAA//gAAAAAD/+AA//gAP///4AAAAAA/////////////AA//gAP///8AAAA//gAAAAAAAAAAP/4AAAAAA//gAP/4AD///+AAAAAAP////////////wAP/4AD////AAAAP/4AAAAAAAAAAD/+AAAAAAP/4AD/+AA////gAAAAAD////////////8AD/+AA////wAAAD/+AAAAAAAAAAA//gAAAAAD/+AA//gAP///4AAAAAA/////////////AA//gAP///8AAAA//gAAAAAAAAAAP///8AB//////wAH/////+AAAAf/////8AB//AAAAP/wAH////////gAP/4AD/+AAAAAAD////AAf/////8AB//////gAAAH//////AAf/wAAAD/8AB////////4AD/+AA//gAAAAAA////wAH//////AAf/////4AAAB//////wAH/8AAAA//AAf///////+AA//gAP/4AAAAAAP///8AB//////wAH/////+AAAAf/////8AB//AAAAP/wAH////////gAP/4AD/+AAAAAAD////AAf/////8AB//////gAAAH//////AAf/wAAAD/8AB////////4AD/+AA//gAAAAAA////wAH//////AAf/////4AAAB//////wAH/8AAAA//AAf///////+AA//gAP/4AAAAAAP///8AB//////wAH/////+AAAAf/////8AB//AAAAP/wAH////////gAP/4AD/+AAAAAAD////AAf/////8AB//////gAAAH//////AAf/wAAAD/8AB////////4AD/+AA//gAAAAAA////wAH//////AAf/////4AAAB//////wAH/8AAAA//AAf///////+AA//gAP/4AAAAAAP///8AB//////wAH/////+AAAAf/////8AB//AAAAP/wAH////////gAP/4AD/+AAAAAAD////AAf/////8AB//////gAAAH//////AAf/wAAAD/8AB////////4AD/+AA//gAAAAAA////wAH//////AAf/////4AAAB//////wAH/8AAAA//AAf///////+AA//gAP/4AAAAAAP///8AB//////wAH/////+AAAAf/////8AB//AAAAP/wAH////////gAP/4AD/+AAAAAAAAB//AAAAP/4AD//////gAAAP/4AAAAAA//////4AD/8AAAAAAf/wAH///+AAAAAAAAAAAAAf/wAAAD/+AA//////4AAAD/+AAAAAAP/////+AA//AAAAAAH/8AB////gAAAAAAAAAAAAH/8AAAA//gAP/////+AAAA//gAAAAAD//////gAP/wAAAAAB//AAf///4AAAAAAAAAAAAB//AAAAP/4AD//////gAAAP/4AAAAAA//////4AD/8AAAAAAf/wAH///+AAAAAAAAAAAAAf/wAAAD/+AA//////4AAAD/+AAAAAAP/////+AA//AAAAAAH/8AB////gAAAAAAAAAAAAH/8AAAA//gAP/////+AAAA//gAAAAAD//////gAP/wAAAAAB//AAf///4AAAAAAAAAAAAB//AAAAP/4AD//////gAAAP/4AAAAAA//////4AD/8AAAAAAf/wAH///+AAAAAAAAAAAAAf/wAAAD/+AA//////4AAAD/+AAAAAAP/////+AA//AAAAAAH/8AB////gAAAAAAAAAAAAH/8AAAA//gAP/////+AAAA//gAAAAAD//////gAP/wAAAAAB//AAf///4AAAAAAAAAAAAB//AAAAP/4AD//////gAAAP/4AAAAAA//////4AD/8AAAAAAf/wAH///+AAAAAAAAAAAAAf/wAAAD/+AA//////4AAAD/+AAAAAAP/////+AA//AAAAAAH/8AB////gAAAAAAAAAAAAH/8AAAA//gAP/////+AAAA//gAAAAAD//////gAP/wAAAAAB//AAf///4AAAAAAAAAAAAB//AAAAP/4AD//////gAAAP/4AAAAAA//////4AD/8AAAAAAf/wAH///+AAAAAAAAAAA////wAH/8AB//AAAAAAH/////+AAAAf///4AAAAAA////gAP/////+AAAAf/wAAAAAAAAP///8AB//AAf/wAAAAAB//////gAAAH///+AAAAAAP///4AD//////gAAAH/8AAAAAAAAD////AAf/wAH/8AAAAAAf/////4AAAB////gAAAAAD///+AA//////4AAAB//AAAAAAAAA////wAH/8AB//AAAAAAH/////+AAAAf///4AAAAAA////gAP/////+AAAAf/wAAAAAAAAP///8AB//AAf/wAAAAAB//////gAAAH///+AAAAAAP///4AD//////gAAAH/8AAAAAAAAD////AAf/wAH/8AAAAAAf/////4AAAB////gAAAAAD///+AA//////4AAAB//AAAAAAAAA////wAH/8AB//AAAAAAH/////+AAAAf///4AAAAAA////gAP/////+AAAAf/wAAAAAAAAP///8AB//AAf/wAAAAAB//////gAAAH///+AAAAAAP///4AD//////gAAAH/8AAAAAAAAD////AAf/wAH/8AAAAAAf/////4AAAB////gAAAAAD///+AA//////4AAAB//AAAAAAAAA////wAH/8AB//AAAAAAH/////+AAAAf///4AAAAAA////gAP/////+AAAAf/wAAAAAAAAP///8AB//AAf/wAAAAAB//////gAAAH///+AAAAAAP///4AD//////gAAAH/8AAAAAAAAD////AAf/wAH/8AAAAAAf/////4AAAB////gAAAAAD///+AA//////4AAAB//AAAAAAAAA////wAH/8AB//AAAAAAH/////+AAAAf///4AAAAAA////gAP/////+AAAAf/wAAAAAAAAAAH//////AAAAP/4AAAB////////wAAAD/+AAAAf/wAAAAAD////AAf///4AAAAAAAAAAAAB//////wAAAD/+AAAAf///////8AAAA//gAAAH/8AAAAAA////wAH///+AAAAAAAAAAAAAf/////8AAAA//gAAAH////////AAAAP/4AAAB//AAAAAAP///8AB////gAAAAAAAAAAAAH//////AAAAP/4AAAB////////wAAAD/+AAAAf/wAAAAAD////AAf///4AAAAAAAAAAAAB//////wAAAD/+AAAAf///////8AAAA//gAAAH/8AAAAAA////wAH///+AAAAAAAAAAAAAf/////8AAAA//gAAAH////////AAAAP/4AAAB//AAAAAAP///8AB////gAAAAAAAAAAAAH//////AAAAP/4AAAB////////wAAAD/+AAAAf/wAAAAAD////AAf///4AAAAAAAAAAAAB//////wAAAD/+AAAAf///////8AAAA//gAAAH/8AAAAAA////wAH///+AAAAAAAAAAAAAf/////8AAAA//gAAAH////////AAAAP/4AAAB//AAAAAAP///8AB////gAAAAAAAAAAAAH//////AAAAP/4AAAB////////wAAAD/+AAAAf/wAAAAAD////AAf///4AAAAAAAAAAAAB//////wAAAD/+AAAAf///////8AAAA//gAAAH/8AAAAAA////wAH///+AAAAAAAAAAAAAf/////8AAAA//gAAAH////////AAAAP/4AAAB//AAAAAAP///8AB////gAAAAAAAAAAAAH//////AAAAP/4AAAB////////wAAAD/+AAAAf/wAAAAAD////AAf///4AAAAAAAAAAD/+AAAAf///4AAAAAA//gAAAAAAAAAAAAAAf/wAH//////////gAP/4AAAAAA//gAAAAAA//gAAAH///+AAAAAAP/4AAAAAAAAAAAAAAH/8AB//////////4AD/+AAAAAAP/4AAAAAAP/4AAAB////gAAAAAD/+AAAAAAAAAAAAAAB//AAf/////////+AA//gAAAAAD/+AAAAAAD/+AAAAf///4AAAAAA//gAAAAAAAAAAAAAAf/wAH//////////gAP/4AAAAAA//gAAAAAA//gAAAH///+AAAAAAP/4AAAAAAAAAAAAAAH/8AB//////////4AD/+AAAAAAP/4AAAAAAP/4AAAB////gAAAAAD/+AAAAAAAAAAAAAAB//AAf/////////+AA//gAAAAAD/+AAAAAAD/+AAAAf///4AAAAAA//gAAAAAAAAAAAAAAf/wAH//////////gAP/4AAAAAA//gAAAAAA//gAAAH///+AAAAAAP/4AAAAAAAAAAAAAAH/8AB//////////4AD/+AAAAAAP/4AAAAAAP/4AAAB////gAAAAAD/+AAAAAAAAAAAAAAB//AAf/////////+AA//gAAAAAD/+AAAAAAD/+AAAAf///4AAAAAA//gAAAAAAAAAAAAAAf/wAH//////////gAP/4AAAAAA//gAAAAAA//gAAAH///+AAAAAAP/4AAAAAAAAAAAAAAH/8AB//////////4AD/+AAAAAAP/4AAAAAAP/4AAAB////gAAAAAD/+AAAAAAAAAAAAAAB//AAf/////////+AA//gAAAAAD/+AAAAAAD/+AAAAf///4AAAAAA//gAAAAAAAAAAAAAAf/wAH//////////gAP/4AAAAAA//gAAAAAA////wAH/8AB////gAP/////8AB//AAf///////+AAAAAAAAAAAAAAB////gAAAAAAAAAAP///8AB//AAf///4AD//////AAf/wAH////////gAAAAAAAAAAAAAAf///4AAAAAAAAAAD////AAf/wAH///+AA//////wAH/8AB////////4AAAAAAAAAAAAAAH///+AAAAAAAAAAA////wAH/8AB////gAP/////8AB//AAf///////+AAAAAAAAAAAAAAB////gAAAAAAAAAAP///8AB//AAf///4AD//////AAf/wAH////////gAAAAAAAAAAAAAAf///4AAAAAAAAAAD////AAf/wAH///+AA//////wAH/8AB////////4AAAAAAAAAAAAAAH///+AAAAAAAAAAA////wAH/8AB////gAP/////8AB//AAf///////+AAAAAAAAAAAAAAB////gAAAAAAAAAAP///8AB//AAf///4AD//////AAf/wAH////////gAAAAAAAAAAAAAAf///4AAAAAAAAAAD////AAf/wAH///+AA//////wAH/8AB////////4AAAAAAAAAAAAAAH///+AAAAAAAAAAA////wAH/8AB////gAP/////8AB//AAf///////+AAAAAAAAAAAAAAB////gAAAAAAAAAAP///8AB//AAf///4AD//////AAf/wAH////////gAAAAAAAAAAAAAAf///4AAAAAAAAAAD////AAf/wAH///+AA//////wAH/8AB////////4AAAAAAAAAAAAAAH///+AAAAAAAAAAA////wAH/8AB////gAP/////8AB//AAf///////+AAAAAAAAAAAAAAB////gAAAAAAAAAAAAH//////////wAAAD/+AAAAAAf///4AAAAAAAAAAP/wAAAD////AAf/wAH/8AAAAAAAAAAB//////////8AAAA//gAAAAAH///+AAAAAAAAAAD/8AAAA////wAH/8AB//AAAAAAAAAAAf//////////AAAAP/4AAAAAB////gAAAAAAAAAA//AAAAP///8AB//AAf/wAAAAAAAAAAH//////////wAAAD/+AAAAAAf///4AAAAAAAAAAP/wAAAD////AAf/wAH/8AAAAAAAAAAB//////////8AAAA//gAAAAAH///+AAAAAAAAAAD/8AAAA////wAH/8AB//AAAAAAAAAAAf//////////AAAAP/4AAAAAB////gAAAAAAAAAA//AAAAP///8AB//AAf/wAAAAAAAAAAH//////////wAAAD/+AAAAAAf///4AAAAAAAAAAP/wAAAD////AAf/wAH/8AAAAAAAAAAB//////////8AAAA//gAAAAAH///+AAAAAAAAAAD/8AAAA////wAH/8AB//AAAAAAAAAAAf//////////AAAAP/4AAAAAB////gAAAAAAAAAA//AAAAP///8AB//AAf/wAAAAAAAAAAH//////////wAAAD/+AAAAAAf///4AAAAAAAAAAP/wAAAD////AAf/wAH/8AAAAAAAAAAB//////////8AAAA//gAAAAAH///+AAAAAAAAAAD/8AAAA////wAH/8AB//AAAAAAAAAAAf//////////AAAAP/4AAAAAB////gAAAAAAAAAA//AAAAP///8AB//AAf/wAAAAAAAAAAH//////////wAAAD/+AAAAAAf///4AAAAAAAAAAP/wAAAD////AAf/wAH/8AAAAAAAAAAB//AAf///4AD////AAAAf/wAAAD//////gAAAAAAAD/+AAAAAAAAAAAAAAAAAAAAAAAAAAf/wAH///+AA////wAAAH/8AAAA//////4AAAAAAAA//gAAAAAAAAAAAAAAAAAAAAAAAAAH/8AB////gAP///8AAAB//AAAAP/////+AAAAAAAAP/4AAAAAAAAAAAAAAAAAAAAAAAAAB//AAf///4AD////AAAAf/wAAAD//////gAAAAAAAD/+AAAAAAAAAAAAAAAAAAAAAAAAAAf/wAH///+AA////wAAAH/8AAAA//////4AAAAAAAA//gAAAAAAAAAAAAAAAAAAAAAAAAAH/8AB////gAP///8AAAB//AAAAP/////+AAAAAAAAP/4AAAAAAAAAAAAAAAAAAAAAAAAAB//AAf///4AD////AAAAf/wAAAD//////gAAAAAAAD/+AAAAAAAAAAAAAAAAAAAAAAAAAAf/wAH///+AA////wAAAH/8AAAA//////4AAAAAAAA//gAAAAAAAAAAAAAAAAAAAAAAAAAH/8AB////gAP///8AAAB//AAAAP/////+AAAAAAAAP/4AAAAAAAAAAAAAAAAAAAAAAAAAB//AAf///4AD////AAAAf/wAAAD//////gAAAAAAAD/+AAAAAAAAAAAAAAAAAAAAAAAAAAf/wAH///+AA////wAAAH/8AAAA//////4AAAAAAAA//gAAAAAAAAAAAAAAAAAAAAAAAAAH/8AB////gAP///8AAAB//AAAAP/////+AAAAAAAAP/4AAAAAAAAAAAAAAAAAAAAAAAAAB//AAf///4AD////AAAAf/wAAAD//////gAAAAAAAD/+AAAAAAAAAAAAAAAAAAAAAAAA//gAP/4AAAAAAAAAAAAAAH/8AB////gAAAH/8AB//////////////+AAAAAAP/4AAAAAAP/4AD/+AAAAAAAAAAAAAAB//AAf///4AAAB//AAf//////////////gAAAAAD/+AAAAAAD/+AA//gAAAAAAAAAAAAAAf/wAH///+AAAAf/wAH//////////////4AAAAAA//gAAAAAA//gAP/4AAAAAAAAAAAAAAH/8AB////gAAAH/8AB//////////////+AAAAAAP/4AAAAAAP/4AD/+AAAAAAAAAAAAAAB//AAf///4AAAB//AAf//////////////gAAAAAD/+AAAAAAD/+AA//gAAAAAAAAAAAAAAf/wAH///+AAAAf/wAH//////////////4AAAAAA//gAAAAAA//gAP/4AAAAAAAAAAAAAAH/8AB////gAAAH/8AB//////////////+AAAAAAP/4AAAAAAP/4AD/+AAAAAAAAAAAAAAB//AAf///4AAAB//AAf//////////////gAAAAAD/+AAAAAAD/+AA//gAAAAAAAAAAAAAAf/wAH///+AAAAf/wAH//////////////4AAAAAA//gAAAAAA//gAP/4AAAAAAAAAAAAAAH/8AB////gAAAH/8AB//////////////+AAAAAAP/4AAAAAAP/4AD/+AAAAAAAAAAAAAAB//AAf///4AAAB//AAf//////////////gAAAAAD/+AAAAAAD/+AA//gAAAAAAAAAAAAAAf/wAH///+AAAAf/wAH//////////////4AAAAAA//gAAAAAA//////////+AA////////4AD/+AAAAAAP/////+AA//AAf/wAH/8AB//AAf/wAAAAAAAAP//////////gAP///////+AA//gAAAAAD//////gAP/wAH/8AB//AAf/wAH/8AAAAAAAAD//////////4AD////////gAP/4AAAAAA//////4AD/8AB//AAf/wAH/8AB//AAAAAAAAA//////////+AA////////4AD/+AAAAAAP/////+AA//AAf/wAH/8AB//AAf/wAAAAAAAAP//////////gAP///////+AA//gAAAAAD//////gAP/wAH/8AB//AAf/wAH/8AAAAAAAAD//////////4AD////////gAP/4AAAAAA//////4AD/8AB//AAf/wAH/8AB//AAAAAAAAA//////////+AA////////4AD/+AAAAAAP/////+AA//AAf/wAH/8AB//AAf/wAAAAAAAAP//////////gAP///////+AA//gAAAAAD//////gAP/wAH/8AB//AAf/wAH/8AAAAAAAAD//////////4AD////////gAP/4AAAAAA//////4AD/8AB//AAf/wAH/8AB//AAAAAAAAA//////////+AA////////4AD/+AAAAAAP/////+AA//AAf/wAH/8AB//AAf/wAAAAAAAAP//////////gAP///////+AA//gAAAAAD//////gAP/wAH/8AB//AAf/wAH/8AAAAAAAAD//////////4AD////////gAP/4AAAAAA//////4AD/8AB//AAf/wAH/8AB//AAAAAAAAA//////////+AA////////4AD/+AAAAAAP/////+AA//AAf/wAH/8AB//AAf/wAAAAAAAAAAAAAAAAAAAAAAAAAAAB//////gAAAH//////AAAAP///4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/////4AAAB//////wAAAD///+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/////+AAAAf/////8AAAA////gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//////gAAAH//////AAAAP///4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/////4AAAB//////wAAAD///+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/////+AAAAf/////8AAAA////gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//////gAAAH//////AAAAP///4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/////4AAAB//////wAAAD///+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/////+AAAAf/////8AAAA////gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//////gAAAH//////AAAAP///4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/////4AAAB//////wAAAD///+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/////+AAAAf/////8AAAA////gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//////gAAAH//////AAAAP///4AAAAAAAAAAAAAAAAAAAAAAAD//////////////+AA//gAf/wAH/8AB//AAf/wAH/8AD/+AA///////////////gAAAAAA///////////////gAP/4AH/8AB//AAf/wAH/8AB//AA//gAP//////////////4AAAAAAP//////////////4AD/+AB//AAf/wAH/8AB//AAf/wAP/4AD//////////////+AAAAAAD//////////////+AA//gAf/wAH/8AB//AAf/wAH/8AD/+AA///////////////gAAAAAA///////////////gAP/4AH/8AB//AAf/wAH/8AB//AA//gAP//////////////4AAAAAAP//////////////4AD/+AB//AAf/wAH/8AB//AAf/wAP/4AD//////////////+AAAAAAD//////////////+AA//gAf/wAH/8AB//AAf/wAH/8AD/+AA///////////////gAAAAAA///////////////gAP/4AH/8AB//AAf/wAH/8AB//AA//gAP//////////////4AAAAAAP//////////////4AD/+AB//AAf/wAH/8AB//AAf/wAP/4AD//////////////+AAAAAAD//////////////+AA//gAf/wAH/8AB//AAf/wAH/8AD/+AA///////////////gAAAAAA///////////////gAP/4AH/8AB//AAf/wAH/8AB//AA//gAP//////////////4AAAAAAP//////////////4AD/+AB//AAf/wAH/8AB//AAf/wAP/4AD//////////////+AAAAAAD//////////////+AA//gAf/wAH/8AB//AAf/wAH/8AD/+AA///////////////gAAAAAA//gAAAAAAAAAA//gAP///4AD////AAAAP////////AA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD///+AA////wAAAD////////wAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA////gAP///8AAAA////////8AD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP///4AD////AAAAP////////AA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD///+AA////wAAAD////////wAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA////gAP///8AAAA////////8AD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP///4AD////AAAAP////////AA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD///+AA////wAAAD////////wAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA////gAP///8AAAA////////8AD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP///4AD////AAAAP////////AA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD///+AA////wAAAD////////wAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA////gAP///8AAAA////////8AD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP///4AD////AAAAP////////AA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AD//////gAP/4AAAB/+AAAAAAAAAAAAB//AAAAP///4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAf/gAAAAAAAAAAAAf/wAAAD///+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAH/4AAAAAAAAAAAAH/8AAAA////gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAB/+AAAAAAAAAAAAB//AAAAP///4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAf/gAAAAAAAAAAAAf/wAAAD///+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAH/4AAAAAAAAAAAAH/8AAAA////gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAB/+AAAAAAAAAAAAB//AAAAP///4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAf/gAAAAAAAAAAAAf/wAAAD///+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAH/4AAAAAAAAAAAAH/8AAAA////gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAB/+AAAAAAAAAAAAB//AAAAP///4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAf/gAAAAAAAAAAAAf/wAAAD///+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAH/4AAAAAAAAAAAAH/8AAAA////gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAB/+AAAAAAAAAAAAB//AAAAP///4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AA//////wAH////////gAP/4AD/8AAAA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAP/////8AB////////4AD/+AA//AAAAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AD//////AAf///////+AA//gAP/wAAAD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AA//////wAH////////gAP/4AD/8AAAA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAP/////8AB////////4AD/+AA//AAAAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AD//////AAf///////+AA//gAP/wAAAD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AA//////wAH////////gAP/4AD/8AAAA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAP/////8AB////////4AD/+AA//AAAAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AD//////AAf///////+AA//gAP/wAAAD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AA//////wAH////////gAP/4AD/8AAAA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAP/////8AB////////4AD/+AA//AAAAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AD//////AAf///////+AA//gAP/wAAAD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AA//////wAH////////gAP/4AD/8AAAA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAAAAAAAB//AAAAP///8AAAAAA//gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAAAAAAAAf/wAAAD////AAAAAAP/4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAAAAAAAH/8AAAA////wAAAAAD/+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAAAAAAAB//AAAAP///8AAAAAA//gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAAAAAAAAf/wAAAD////AAAAAAP/4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAAAAAAAH/8AAAA////wAAAAAD/+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAAAAAAAB//AAAAP///8AAAAAA//gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAAAAAAAAf/wAAAD////AAAAAAP/4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAAAAAAAH/8AAAA////wAAAAAD/+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAAAAAAAB//AAAAP///8AAAAAA//gAP/4AD//////gAP/4AAAAAAP/4AD//////gAP/4AAAAAAAAAAf/wAAAD////AAAAAAP/4AD/+AA//////4AD/+AAAAAAD/+AA//////4AD/+AAAAAAAAAAH/8AAAA////wAAAAAD/+AA//gAP/////+AA//gAAAAAA//gAP/////+AA//gAAAAAAAAAB//AAAAP///8AAAAAA//gAP/4AD//////gAP/4AAAAAAP/4AAAAAAAAAAP/4AD/+AB//AAAAP/4AD//////gAAAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA//gAf/wAAAD/+AA//////4AAAD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP/4AH/8AAAA//gAP/////+AAAA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD/+AB//AAAAP/4AD//////gAAAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA//gAf/wAAAD/+AA//////4AAAD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP/4AH/8AAAA//gAP/////+AAAA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD/+AB//AAAAP/4AD//////gAAAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA//gAf/wAAAD/+AA//////4AAAD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP/4AH/8AAAA//gAP/////+AAAA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD/+AB//AAAAP/4AD//////gAAAP/4AD/+AAAAAAAAAAD/+AAAAAAD/+AAAAAAAAAAD/+AA//gAf/wAAAD/+AA//////4AAAD/+AA//gAAAAAAAAAA//gAAAAAA//gAAAAAAAAAA//gAP/4AH/8AAAA//gAP/////+AAAA//gAP/4AAAAAAAAAAP/4AAAAAAP/4AAAAAAAAAAP/4AD/+AB//AAAAP/4AD//////gAAAP/4AD/+AAAAAAAAAAD/+AAAAAAD//////////////+AAAAAAf/wAH/8AAAAAAf///////8AAAA///////////////gAAAAAA///////////////gAAAAAH/8AB//AAAAAAH////////AAAAP//////////////4AAAAAAP//////////////4AAAAAB//AAf/wAAAAAB////////wAAAD//////////////+AAAAAAD//////////////+AAAAAAf/wAH/8AAAAAAf///////8AAAA///////////////gAAAAAA///////////////gAAAAAH/8AB//AAAAAAH////////AAAAP//////////////4AAAAAAP//////////////4AAAAAB//AAf/wAAAAAB////////wAAAD//////////////+AAAAAAD//////////////+AAAAAAf/wAH/8AAAAAAf///////8AAAA///////////////gAAAAAA///////////////gAAAAAH/8AB//AAAAAAH////////AAAAP//////////////4AAAAAAP//////////////4AAAAAB//AAf/wAAAAAB////////wAAAD//////////////+AAAAAAD//////////////+AAAAAAf/wAH/8AAAAAAf///////8AAAA///////////////gAAAAAA///////////////gAAAAAH/8AB//AAAAAAH////////AAAAP//////////////4AAAAAAP//////////////4AAAAAB//AAf/wAAAAAB////////wAAAD//////////////+AAAAAAD//////////////+AAAAAAf/wAH/8AAAAAAf///////8AAAA///////////////gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
                return DependencyService.Get<Interfaces.IBitmapHelper>()?.GetBLogo("");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPageViewModel:GetBytesLogo]Error: " + ex.Message);
                return new byte[] { };
            }
        }

        /// <summary>
        /// Imprimir
        /// </summary>
        public ICommand PrintCommand => new Command(PrintText);
        private void PrintText()
        {
            try
            {
                //inicializar variables
                bool showPrinted = true;

                //si tenemos la conexión y el listado de texto
                if (_savedCharacteristic != null && ListTextToPrint?.Count > 0)
                {
                    //recorremos el listado
                    foreach (TextPrinterModel textPrinterModel in ListTextToPrint)
                    {
                        // imprimir listado de array
                        PrintListArray(arrayBytes: textPrinterModel.GetBytesText(), showPrinted: showPrinted);

                        // solo mostramos mensaje la primera vez
                        showPrinted = false;
                    } 
                }
                else
                {
                    AppConfig.Instance.Dialogs.DisplayToast("Impresora no disponible para imprimir. Inténtelo de nuevo más tarde", showLongTime: false);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPageViewModel:PrintText]Error: " + ex.Message);
            }
        }

     

        private void PrintListArray(byte[] arrayBytes, bool showPrinted)
        {
            try
            {
                //si tenemos la conexión y el listado de texto
                if (_savedCharacteristic != null && arrayBytes != null)
                {               

                    //imprimir
                    _savedCharacteristic?.Write(
                        value: arrayBytes,
                        withResponse: true
                        ).Subscribe(
                        result => {
                            //si se ha impreso, mostrar mensaje 1 vez
                            if (showPrinted)
                                AppConfig.Instance.Dialogs.DisplayToast("Datos impresos");
                      
                        },
                        exception => {
                            AppConfig.Instance.Dialogs.DisplayToast("No puede imprimir");
                        }
                    );
                }
                else
                    AppConfig.Instance.Dialogs.DisplayToast("Impresora no disponible para imprimir. Inténtelo de nuevo más tarde", showLongTime: false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPageViewModel:PrintListArray]Error: " + ex.Message);
            }
        }

        private void CancelConnection()
        {
            try
            {
                if (AppConfig.Instance.Globals.Peripheral != null)
                {
                    AppConfig.Instance.Globals.CancelConnectionPeripheral();
                    _ = Application.Current.MainPage.Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintViewModel:CancelConnection]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Suscripción a eventos de la impresora
        /// </summary>
        private void SuscripcionEventosImpresora()
        {
            try
            {
                //perfil para imprimir
                _perifDisposable = AppConfig.Instance.Globals.Peripheral.WhenAnyCharacteristicDiscovered().Subscribe((characteristic) =>
                {
                    try
                    {
                        if (characteristic != null && characteristic.CanWrite() && !characteristic.CanRead() && !characteristic.CanNotify())
                        {
                            _savedCharacteristic = characteristic;
                            Debug.WriteLine($"Writing {characteristic.Uuid} - {characteristic.CanRead()} - {characteristic.CanIndicate()} - {characteristic.CanNotify()}");
                            _perifDisposable.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[PrintPageViewModel:WhenAnyCharacteristicDiscovered]Error: " + ex.Message);
                    }

                });

                _connectDisposable = AppConfig.Instance.Globals.Peripheral.WhenConnected().Subscribe((x) =>
                {
                    try
                    {
                        AppConfig.Instance.Dialogs.DisplayToast("Impresora conectada");
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[PrintPageViewModel:WhenConnected]Error: " + ex.Message);
                    }
                });

                _disConnectDisposable = AppConfig.Instance.Globals.Peripheral.WhenDisconnected().Subscribe((x) =>
                {
                    try
                    {
                        AppConfig.Instance.Dialogs.DisplayToast("Impresora desconectada", showLongTime: false);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[PrintPageViewModel:WhenDisconnected]Error: " + ex.Message);
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[PrintPageViewModel:EventosImpresora]Error: " + ex.Message);
                AppConfig.Instance.Dialogs.DisplayToast("Error. Por favor, reinicie la impresora y vuelva a intentar imprimir");
                CancelConnection();
            }
        }
    }
}
