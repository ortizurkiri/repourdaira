﻿using AppUrdaira.Models;
using AppUrdaira.Utilities;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Shapes;
using static Android.App.Assist.AssistStructure;

namespace AppUrdaira.ViewModels
{
    public class DeliveryNoteLineViewModel : BaseViewModel
    {
        #region Propiedades
        public GenericModel GenericModel { get; set; } = null;

        //Listado de artículos
        private ObservableCollection<ProductosModel> listArticle = null;
        public ObservableCollection<ProductosModel> ListArticle
        {
            get { return listArticle; }
            set { listArticle = value; OnPropertyChanged(nameof(ListArticle)); }
        }
        private ProductosModel selectedArticle = null;
        public ProductosModel SelectedArticle
        {
            get { return selectedArticle; }
            set 
            {
                try
                {
                    selectedArticle = value;
                    OnPropertyChanged(nameof(SelectedArticle));
                    SetPriceIvaAndUnit();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("[DeliveryNoteLineViewModel:SelectedArticle]Error:" + ex.Message);
                }
  
            }
        }

        //Listado de lotes
        private ObservableCollection<LoteLnExtendedModel> listLotsExtended = null;
        public ObservableCollection<LoteLnExtendedModel> ListLotsExtended
        {
            get { return listLotsExtended; }
            set { listLotsExtended = value; OnPropertyChanged(nameof(ListLotsExtended)); }
        }
        private LoteLnModel selectedLot = null;
        public LoteLnModel SelectedLot
        {
            get { return selectedLot; }
            set { selectedLot = value; OnPropertyChanged(nameof(SelectedLot)); }
        }

        //Listado de unidades
        private ObservableCollection<UnidadesMedidaModel> listUnits = null;
        public ObservableCollection<UnidadesMedidaModel> ListUnits
        {
            get { return listUnits; }
            set { listUnits = value; OnPropertyChanged(nameof(ListUnits)); }
        }
        private UnidadesMedidaModel selectedUnit = null;
        public UnidadesMedidaModel SelectedUnit
        {
            get { return selectedUnit; }
            set { selectedUnit = value; OnPropertyChanged(nameof(SelectedUnit)); }
        }

        //Listado de IVA
        private ObservableCollection<TipoIvaModel> listIVA = null;
        public ObservableCollection<TipoIvaModel> ListIVA
        {
            get { return listIVA; }
            set { listIVA = value; OnPropertyChanged(nameof(ListIVA)); }
        }
        private TipoIvaModel selectedIVA = null;
        public TipoIvaModel SelectedIVA
        {
            get { return selectedIVA; }
            set { selectedIVA = value; OnPropertyChanged(nameof(SelectedIVA)); }
        }

        //Cantidad
        private string quantityStr = "0";
        public string QuantityStr
        {
            get { return quantityStr; }
            set { quantityStr = value; OnPropertyChanged(nameof(QuantityStr)); }
        }

        //Precio
        private string priceStr = "0";
        public string PriceStr
        {
            get { return priceStr; }
            set { priceStr = value; OnPropertyChanged(nameof(PriceStr)); }
        }

        //Importe
        private string amountStr = "0";
        public string AmountStr
        {
            get { return amountStr; }
            set { amountStr = value; OnPropertyChanged(nameof(AmountStr)); }
        }

        //Rappel
        private double rappel = 0;
        public double Rappel
        {
            get { return rappel; }
            set { rappel = value; OnPropertyChanged(nameof(Rappel)); }
        }

        //Recargo
        private double recargo = 0;
        public double Recargo
        {
            get { return recargo; }
            set { recargo = value; OnPropertyChanged(nameof(Recargo)); }
        }

        //Texto boton insertar/editar
        private string textButtonInsertEdit = string.Empty;
        public string TextButtonInsertEdit
        {
            get { return textButtonInsertEdit; }
            set { textButtonInsertEdit = value; OnPropertyChanged(nameof(TextButtonInsertEdit)); }
        }

        // Cabecera de albarán
        public AlbaranCbModel DeliveryNote { get; set; } = null;
        //Línea de albarán
        public AlbaranLnModel DeliveryNoteLine { get; set; } = null;

        public bool IsEditLine { get; set; } = false;

        public bool IsInitializingFields { get; set; } = false;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public DeliveryNoteLineViewModel(GenericModel gm)
        {
            try
            {
                //Inicializar variables
                GenericModel = gm;
                InitializeFields(); 
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLineViewModel:()]Error:" + ex.Message);
            }
        }

        public ICommand InsertEditCommand => new Command(() => InsertEdit());
        public ICommand RecalculateAmountCommand => new Command(() => RecalculateAmount());

        /// <summary>
        /// Insertar línea de albarán
        /// </summary>
        private void InsertEdit()
        {
            try
            {
                IsBusy = true;

                //inicializar variables
                AlbaranLnModel deliveryNoteLineExist = null;
                string fieldError = HasFieldError();

                // si no hay campo con error
                if (string.IsNullOrEmpty(fieldError))
                {
                    //asignar campos de la línea de albarán
                    DeliveryNoteLine.SetNAlbaranAndNLinea(
                        fecha:GenericModel.DeliveryNote.FechaRowId, 
                        nAlbaran: GenericModel.DeliveryNote.NAlbaran, 
                        nLinea: IsEditLine ? DeliveryNoteLine.NLinea : GenericModel.GetNextNLine()
                    );
                    DeliveryNoteLine.Concepto = SelectedArticle.Descripcion;
                    DeliveryNoteLine.Lote = SelectedLot?.Lote ?? string.Empty;
                    //DeliveryNoteLine.Cantidad = Quantity;
                    DeliveryNoteLine.Cantidad = Convert.ToDouble(QuantityStr);
                    DeliveryNoteLine.Medida = SelectedUnit?.NombreCorto ?? string.Empty;
                    DeliveryNoteLine.Equivalencia = 0;
                    DeliveryNoteLine.Rappel = Rappel;
                    DeliveryNoteLine.TipoIva = SelectedIVA.TipoIva;
                    DeliveryNoteLine.Recargo = Recargo;
                    DeliveryNoteLine.Precio = Convert.ToDouble(PriceStr);
                    DeliveryNoteLine.Importe = Convert.ToDouble(AmountStr);
                    DeliveryNoteLine.BaseImponible = DeliveryNoteLine.Importe;
                    DeliveryNoteLine.CuotaIva = DeliveryNoteLine.GetCuotaIva();
                    DeliveryNoteLine.FechaRegistro = DateTime.Now.ToString("dd/MM/yyyy");
                    DeliveryNoteLine.HoraRegistro = DateTime.Now.ToString("HH:mm:ss");
                    DeliveryNoteLine.TagPropio = Utilities.Generators.TagGenerator();
                    DeliveryNoteLine.TagReenvio = 0;
                    DeliveryNoteLine.TagBorrado = 0;
                    DeliveryNoteLine.GuidGenericModel = GenericModel.GuidGenericModel;
                    DeliveryNoteLine.RowIdAlbaranCbModel = GenericModel.DeliveryNote.RowId;

                    //cogemos la página de albarán
                    var pageDeliveryNote = App.Current.MainPage.Navigation.NavigationStack.LastOrDefault(x => x.GetType() == typeof(DeliveryNotePage));
                    if (pageDeliveryNote != null)
                    {
                        //si estamos editando una línea existente
                        if (IsEditLine)
                        {
                            //cogemos la línea existente
                            deliveryNoteLineExist = ((DeliveryNotePage)pageDeliveryNote).ViewModel.ListDeliveryNoteLn.FirstOrDefault(x => x.RowId == DeliveryNoteLine.RowId);

                            //establecemos los cambios de la línea de albarán
                            deliveryNoteLineExist = DeliveryNoteLine;
                        }
                        //si estamos añadiendo una nueva línea
                        else                       
                            ((DeliveryNotePage)pageDeliveryNote).ViewModel.ListDeliveryNoteLn.Add(DeliveryNoteLine);
                        
                        //volver a pantalla de albarán
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await App.Current.MainPage.Navigation.PopAsync();
                        });
                    }
                }
                //si algún campo tiene algún error
                else
                {
                    AppConfig.Instance.Dialogs.DisplayAlert(message: $"Falta el campo '{fieldError}' por rellenar");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLineViewModel:InsertEdit]Error:" + ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        /// <summary>
        /// Recalcular importe
        /// </summary>
        private void RecalculateAmount()
        {
            try
            {
                string priceStr = !string.IsNullOrEmpty(PriceStr) ? PriceStr : "0";
                double price = double.Parse(priceStr);
                string quantityStr = !string.IsNullOrEmpty(QuantityStr) ? QuantityStr : "0";
                double quantity = double.Parse(quantityStr);
                double amount = quantity * price;
                PriceStr = price.ToString();
                AmountStr = amount.ToString();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLineViewModel:RecalculateAmount]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Inicializar campos
        /// </summary>
        private void InitializeFields()
        {
            try
            {
                IsInitializingFields = true;
                IsEditLine = GenericModel?.DeliveryNoteLine != null;
                TextButtonInsertEdit = IsEditLine ? "Editar línea" : "Insertar línea";
                Title = IsEditLine ? "Editar línea de albaran" : "Nueva línea de albarán";
                DeliveryNote = GenericModel?.DeliveryNote ?? new AlbaranCbModel();
                DeliveryNoteLine = GenericModel?.DeliveryNoteLine ?? new AlbaranLnModel();
                ListArticle = new ObservableCollection<ProductosModel>(GenericModel?.ListArticles ?? new List<ProductosModel>());
                ListLotsExtended = new ObservableCollection<LoteLnExtendedModel>(GenericModel?.ListLotsExtended ?? new List<LoteLnExtendedModel>());
                ListUnits = new ObservableCollection<UnidadesMedidaModel>(GenericModel?.ListUnits ?? new List<UnidadesMedidaModel>());
                ListIVA = new ObservableCollection<TipoIvaModel>(GenericModel?.ListIVA ?? new List<TipoIvaModel>());

                SelectedIVA = ListIVA.FirstOrDefault(x => x.TipoIva == ((DeliveryNoteLine?.TipoIva) ?? 21));
                SelectedArticle = ListArticle.FirstOrDefault(x => x.Descripcion == DeliveryNoteLine.Concepto);
                SelectedLot = ListLotsExtended.FirstOrDefault(x => x.Lote == DeliveryNoteLine.Lote);
                SelectedUnit = ListUnits.FirstOrDefault(x => x.NombreCorto == DeliveryNoteLine.Medida);
                QuantityStr = DeliveryNoteLine.Cantidad.ToString();
                PriceStr = DeliveryNoteLine.Precio.ToString();
                AmountStr = DeliveryNoteLine.Importe.ToString();
                Rappel = DeliveryNoteLine.Rappel;
                Recargo = DeliveryNoteLine.Recargo;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLineViewModel:InitializeFields]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Indica si tiene campo con error
        /// </summary>
        /// <returns></returns>
        private string HasFieldError()
        {
            try
            {
                string fieldError = string.Empty;

                if(SelectedArticle == null)
                    fieldError = "Concepto";
                else if(SelectedLot == null)
                    fieldError = "Lote";
                else if (SelectedIVA == null)
                    fieldError = "Iva";
                else if(string.IsNullOrEmpty(QuantityStr) || double.Parse(QuantityStr) <= 0)
                    fieldError = "Cantidad";
                else if (string.IsNullOrEmpty(PriceStr))
                    fieldError = "Precio";
                else if (string.IsNullOrEmpty(AmountStr))
                    fieldError = "Importe";

                return fieldError;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLineViewModel:HasFieldError]Error:" + ex.Message);
                return "Todo";
            }
        }

        /// <summary>
        /// Establecer precio e iva 
        /// </summary>
        private async void SetPriceIvaAndUnit()
        {
            try
            {
                // inicializar variables
                string price = "0";
                int iva = 21;
                string unit = "Botella";
                AlbaranLnModel lastAlbaranLn = null;

                if (!IsEditLine || !IsInitializingFields)
                {
                    //si hemos seleccionado un artículo, establecemos su precio e iva
                    if (!string.IsNullOrEmpty(SelectedArticle?.Descripcion) && GenericModel?.DeliveryNote != null)
                    {
                        //obtener última línea de albarán
                        lastAlbaranLn = await AlbaranLnModel.GetLastSameAlbaranLnByConceptAndCustomer(
                            concepto: SelectedArticle.Descripcion,
                            idCliente: GenericModel.DeliveryNote.IdCliente);

                        //si encontramos alguna línea de albaran igual, asignamos el precio y el iva
                        if (lastAlbaranLn != null)
                        {
                            price = lastAlbaranLn.Precio.ToString();
                            iva = Convert.ToInt32(lastAlbaranLn.TipoIva);
                        }

                        //asignar unidad
                        unit = SelectedArticle.Unidad;
                    }

                    //asignar el precio, iva y medida
                    PriceStr = price;
                    SelectedIVA = ListIVA.FirstOrDefault(x => x.TipoIva == iva);
                    SelectedUnit = ListUnits.FirstOrDefault(x => x.Descripcion == unit);
                    RecalculateAmount();
                }


            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLineViewModel:SetPriceAndIva]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Deshabilitar inicialización
        /// </summary>
        public void DisableInitialize()
        {
            try
            {
                if (IsInitializingFields)
                    IsInitializingFields = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[DeliveryNoteLineViewModel:DisableInitialize]Error:" + ex.Message);
            }
        }
    }
}
