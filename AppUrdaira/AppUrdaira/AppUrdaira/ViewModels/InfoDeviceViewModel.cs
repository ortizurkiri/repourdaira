﻿using AppUrdaira.Models;
using AppUrdaira.Utilities;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppUrdaira.ViewModels
{
    [QueryProperty(nameof(InitialConfig), nameof(InitialConfig))]
    public class InfoDeviceViewModel : BaseViewModel
    {
        #region Propiedades
        private string idDevice = string.Empty;
        public string IdDevice
        {
            get { return idDevice; }
            set { idDevice = value; OnPropertyChanged(nameof(IdDevice)); }
        }

        private string customerDefault = string.Empty;
        public string CustomerDefault
        {
            get { return customerDefault; }
            set { customerDefault = value; OnPropertyChanged(nameof(CustomerDefault)); }
        }

        private string host = string.Empty;
        public string Host
        {
            get { return host; }
            set { host = value; OnPropertyChanged(nameof(Host)); }
        }

        private string bbdd = string.Empty;
        public string BBDD
        {
            get { return bbdd; }
            set { bbdd = value; OnPropertyChanged(nameof(BBDD)); }
        }

        private bool initialConfig = false;
        public bool InitialConfig
        {
            get { return initialConfig; }
            set 
            { 
                initialConfig = value; 
                OnPropertyChanged(nameof(InitialConfig));
                OnPropertyChanged(nameof(ShowNavBar));
                OnPropertyChanged(nameof(ShowContinueButton));
                OnPropertyChanged(nameof(ShowResetButton));
            }
        }

        private bool canEditHostAndBBDD = false;
        public bool CanEditHostAndBBDD
        {
            get { return canEditHostAndBBDD; }
            set
            {
                canEditHostAndBBDD = value;
                OnPropertyChanged(nameof(CanEditHostAndBBDD));
            }
        }

        public bool ShowNavBar => !InitialConfig;
        public bool ShowContinueButton => InitialConfig;
        public bool ShowResetButton => !InitialConfig;

        //Listado de clientes

        private bool isVisibleListCustomers = false;
        public bool IsVisibleListCustomers
        {
            get { return isVisibleListCustomers; }
            set { isVisibleListCustomers = value; OnPropertyChanged(nameof(IsVisibleListCustomers)); }
        }
        private List<ClientesModel> listCustomer = null;
        public List<ClientesModel> ListCustomer
        {
            get { return listCustomer; }
            set { listCustomer = value; OnPropertyChanged(nameof(ListCustomer)); }
        }
        private ObservableCollection<ClientesModel> listVisualCustomer = null;
        public ObservableCollection<ClientesModel> ListVisualCustomer
        {
            get { return listVisualCustomer; }
            set { listVisualCustomer = value; OnPropertyChanged(nameof(ListVisualCustomer)); }
        }
        private ClientesModel selectedCustomer = null;
        public ClientesModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                OnPropertyChanged(nameof(SelectedCustomer));
                SetNameCustomer();
            }
        }
        private string nameCustomer = string.Empty;
        public string NameCustomer
        {
            get { return nameCustomer; }
            set { nameCustomer = value; OnPropertyChanged(nameof(NameCustomer)); }
        }
        public bool IsFirstAccessCustomers { get; set; } = true;
        #endregion


        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public InfoDeviceViewModel()
        {
            try
            {
                //inicializar variables
                Title = "Configuración dispositivo";
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDeviceViewModel:()]Error:" + ex.Message);
            }
        }

        #region Métodos
        /// <summary>
        /// Establecer nombre del cliente
        /// </summary>
        private void SetNameCustomer()
        {
            try
            {
                // asignar nombre del cliente
                AppConfig.Instance.Globals.CustomerDefault = NameCustomer = SelectedCustomer?.NombreEmpresa ?? string.Empty;

                // ocultamos el listado de clientes
                IsVisibleListCustomers = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDeviceViewModel:SetSurchargeAndNameCustomer]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Modificar listado clientes
        /// </summary>
        /// <returns></returns>
        private async Task SetListCustomer()
        {
            try
            {
                List<ClientesModel> listaClientes = await ClientesModel.GetListCustomerBBDD().ConfigureAwait(false);
                if (listaClientes != null)
                {
                    ListCustomer = new List<ClientesModel>(listaClientes);
                    ListVisualCustomer = new ObservableCollection<ClientesModel>(ListCustomer);
                }
                       
                SelectedCustomer = ListCustomer?.FirstOrDefault(x => x.NombreEmpresa == CustomerDefault);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[InfoDeviceViewModel:SetListCustomer]Error: " + ex.Message);
            }
        } 
        #endregion

        #region Commands
        public ICommand InitializeFieldsCommand => new Command(async () => await InitializeFieldsExecute());
        private async Task InitializeFieldsExecute()
        {
            try
            {
                // puede editar si el usuario es el 811
                CanEditHostAndBBDD = AppConfig.Instance.Globals.LoginUser?.Usuario == "811";

                IdDevice = AppConfig.Instance.Globals.Device;
                CustomerDefault = AppConfig.Instance.Globals.CustomerDefault;
                Host = AppConfig.Instance.Globals.Host;
                BBDD = AppConfig.Instance.Globals.BBDD;

                //asignar clientes
                await SetListCustomer().ConfigureAwait(false);        
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[InfoDeviceViewModel:InitializeFieldsExecute]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Cambio cliente seleccionado
        /// </summary>
        public ICommand ChangeSelectedCustomerCommand => new Command(() => ChangeSelectedCustomer());
        private void ChangeSelectedCustomer()
        {
            try
            {
                if (ListCustomer?.Count > 0)
                {
                    //si tenemos algo para filtrar, lo filtramos. Si no tenemos nada, mostrar el listado completo
                    ListVisualCustomer = new ObservableCollection<ClientesModel>(!string.IsNullOrEmpty(NameCustomer)
                        ? ListCustomer.Where(x => x.NombreEmpresa?.ToUpper()?.Contains(NameCustomer.ToUpper()) ?? false).ToList()
                        : ListCustomer.ToList());

                    //si no se visualiza el listado de clientes y no es el primer acceso, mostrarlo
                    if (!IsVisibleListCustomers && !IsFirstAccessCustomers)
                        IsVisibleListCustomers = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[InfoDeviceViewModel:ChangeSelectedCustomer]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Mostrar/ocultar listado de clientes
        /// </summary>
        public ICommand ShowHideListCustomerCommand => new Command(() => ShowHideListCustomer());
        private void ShowHideListCustomer()
        {
            try
            {
                // si tenemos listado de clientes
                if (ListCustomer?.Count > 0)
                {
                    // asignamos listado total de clientes a clientes visibles
                    ListVisualCustomer = new ObservableCollection<ClientesModel>(ListCustomer);

                    // mostramos / ocultamos listado de clientes visibles
                    IsVisibleListCustomers = !IsVisibleListCustomers;

                    // indicamos que ya no es el primer acceso a clientes
                    if (IsFirstAccessCustomers)
                        IsFirstAccessCustomers = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"[InfoDeviceViewModel:ShowHideListCustomer]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Cambiar el id del dispositivo
        /// </summary>
        public ICommand SetIdDeviceCommand => new Command(SetIdDevice);
        private void SetIdDevice()
        {
            try
            {
                AppConfig.Instance.Globals.Device = IdDevice;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDeviceViewModel:SetIdDevice]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Cambiar el host
        /// </summary>
        public ICommand SetHostCommand => new Command(SetHost);
        private void SetHost()
        {
            try
            {
                AppConfig.Instance.Globals.Host = Host;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDeviceViewModel:SetHost]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Cambiar el BBDD
        /// </summary>
        public ICommand SetBBDDCommand => new Command(SetBBDD);
        private void SetBBDD()
        {
            try
            {
                AppConfig.Instance.Globals.BBDD = BBDD;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDeviceViewModel:SetBBDD]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Continuar 
        /// </summary>
        public ICommand ContinueCommand => new Command(Continue);
        private async void Continue()
        {
            try
            {
                //si no se ha rellenado el id del dispositivo
                if(string.IsNullOrEmpty(IdDevice) || IdDevice == Constants.DeviceInit)
                {
                    AppConfig.Instance.Dialogs.DisplayAlert(message: "Debe cambiar el nombre del dispositivo. Ej.: P01,P05,P20...");
                }
                //si se ha rellenado correctamente el id del dispositivo
                else
                {
                    // nos vamos a pantalla de home
                    await Shell.Current.GoToAsync($"//{nameof(HomePage)}");
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDeviceViewModel:Continue]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Resetear la información del dispositivo del dispositivo
        /// </summary>
        public ICommand ResetCommand => new Command(async () => await Reset());
        private async Task Reset()
        {
            try
            {
                //mensaje de confirmación
                if(await AppConfig.Instance.Dialogs.DisplayYesNo(message: "¿Está seguro que desea resetear la información del dispositivo?"))
                {
                    //resetear preferencias
                    AppConfig.Instance.Globals.ResetearPreferencias();

                    // inicializar variables
                    IdDevice = AppConfig.Instance.Globals.Device;
                    CustomerDefault = AppConfig.Instance.Globals.CustomerDefault;
                    Host = AppConfig.Instance.Globals.Host;
                    BBDD = AppConfig.Instance.Globals.BBDD;

                    // borrar BBDD
                    await AppConfig.Instance.Database.DeleteAsync();

                    // nos vamos a pantalla de login
                    await Shell.Current.GoToAsync(nameof(LoginPage));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InfoDeviceViewModel:Reset]Error:" + ex.Message);
            }
        }
        #endregion
    }
}
