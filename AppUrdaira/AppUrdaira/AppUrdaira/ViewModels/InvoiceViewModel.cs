﻿using AppUrdaira.Models;
using AppUrdaira.Utilities;
using AppUrdaira.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using static AppUrdaira.Enums.Enums;

namespace AppUrdaira.ViewModels
{
    public class InvoiceViewModel : BaseViewModel
    {
        #region Propiedades
        public string Factura { get; set; } = string.Empty;
        public string SerieFactura { get;set; } = string.Empty;
        public string FechaFactura { get;set; } = string.Empty;
        public string TBai { get;set; } = string.Empty;
        public string TBaiUrl { get;set; } = string.Empty;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public InvoiceViewModel(GenericModel genericModel)
        {
            try
            {
                //Inicializar variables
                Title = "Factura";
                InitializeFields(genericModel);
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InvoiceViewModel:()]Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Inicializar campos
        /// </summary>
        public void InitializeFields(GenericModel genericModel)
        {
            try
            {
                if(genericModel != null)
                {
                    Factura = genericModel.DeliveryNote?.Factura ?? string.Empty;
                    SerieFactura = genericModel.DeliveryNote?.SerieFactura ?? string.Empty;
                    FechaFactura = Generators.GetDateTimeStringByLongString(genericModel.DeliveryNote?.FechaFactura) ?? string.Empty;
                    TBaiUrl = genericModel.DeliveryNote?.TBaiUrl ?? string.Empty;
                    TBai = genericModel.DeliveryNote?.TBaiId ?? string.Empty;               
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[InvoiceViewModel:InitializeFields]Error:" + ex.Message);
            }
        }
    }
}
