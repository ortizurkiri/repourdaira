﻿using AppUrdaira.Models;
using AppUrdaira.Views;
using Newtonsoft.Json;
using Shiny;
using Shiny.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppUrdaira.ViewModels
{
    public class SelectedPrintViewModel : BaseViewModel
    {
        #region Propiedades
        IDisposable _scanDisposable, _connectedDisposable;
        IBleManager _centralManager = Shiny.ShinyHost.Resolve<IBleManager>();

        private bool isScanning = false;
        public bool IsScanning
        {
            get { return isScanning; }
            set { isScanning = value; OnPropertyChanged(nameof(IsScanning)); OnPropertyChanged(nameof(TextIsScanning)); }
        }


        public string TextIsScanning => IsScanning ? "Escaneando..." : "Sin escanear"; 
       

        private ObservableCollection<IPeripheral> peripherals = new ObservableCollection<IPeripheral>();

        public ObservableCollection<IPeripheral> Peripherals
        {
            get { return peripherals; }
            set { peripherals = value; OnPropertyChanged(nameof(Peripherals)); }
        }

        public ICommand GetDeviceListCommand { get; set; }

        public ICommand SetAdapterCommand { get; set; }
        public ICommand CheckPermissionsCommand { get; set; }
        public ICommand ResetScanCommand { get; set; }
        public ICommand LoadCommand { get; set; }
        public ICommand CancelCommand { get; set; }


        IPeripheral _selectedPeripheral;
        public IPeripheral SelectedPeripheral
        {
            get
            {
                return _selectedPeripheral;
            }
            set
            {
                _selectedPeripheral = value;
                if (_selectedPeripheral != null)
                {
                    try
                    {
                        AppConfig.Instance.Globals.DefaultPrinter = _selectedPeripheral.Name;
                        AppConfig.Instance.Globals.DefaultPrinterUuid = _selectedPeripheral.Uuid.ToString();
                        AppConfig.Instance.Globals.Peripheral = _selectedPeripheral;
                        OnSelectedPeripheral();
                        OnPropertyChanged(nameof(ShowButtonOldPrint));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("[SelectedPrintViewModel:SelectedPeripheral]Error: " + ex.Message);
                    }

                }
            }
        }

        public bool ShowButtonOldPrint => AppConfig.Instance.Globals.Peripheral?.IsConnected() ?? false;

        public List<TextPrinterModel> ListTextToPrint { get; set; } = null;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SelectedPrintViewModel(List<TextPrinterModel> listTextToPrint, bool autoLoadPrinter)
        {
            try
            {
                //Inicializar variables
                Title = "Seleccionar impresora";
                ListTextToPrint = listTextToPrint;

                //Commands
                GetDeviceListCommand = new Command(async () => await GetDeviceList());
                SetAdapterCommand = new Command(async () => await ActiveBluetooth());
                CheckPermissionsCommand = new Command(async () => await CheckPermissions());
                LoadCommand = new Command(() => Load());
                CancelCommand = new Command(CancelConnection);

                //cargar directamente la impresora si queremos autocargar la impresora
                if(autoLoadPrinter)
                    Load(removePage: true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:()]Error: " + ex.Message);
            }
        }

        private void Load(bool removePage = false)
        {
            try
            {
                //si tenemos una impresora guardada y, no queremos eliminar página actual o la impresora está conectada
                if (!string.IsNullOrEmpty(AppConfig.Instance.Globals.DefaultPrinterUuid) && AppConfig.Instance.Globals.Peripheral != null 
                    && (!removePage || (AppConfig.Instance.Globals.Peripheral?.IsConnected() ?? false)))
                {
                    //ir a pantalla de imprimir
                    AppConfig.Instance.Navigations.GoToPageBeginInvoke(
                        page: new PrintPage(ListTextToPrint), 
                        actionAfter: () =>
                        {
                            SelectedPeripheral = null;

                            //si hemos indicado que queremos eliminar la página, eliminamos la página de seleccionar impresora 
                            if (removePage)
                                AppConfig.Instance.Navigations.RemovePage(type: typeof(SelectedPrintPage));
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:Load]Error: " + ex.Message);
            }
        }

        private async Task CheckPermissions()
        {
            try
            {
                var status = await _centralManager.RequestAccess();
                if (status == AccessState.Denied)
                {
                    await App.Current.MainPage.DisplayAlert("Permissions", "You need to have your bluetooth ON to use this app", "Ok");
                    Xamarin.Essentials.AppInfo.ShowSettingsUI();
                }
                else
                {
                    SetAdapterCommand.Execute(null);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:CheckPermissions]Error: " + ex.Message);
            }
        }

        private async Task ActiveBluetooth()
        {
            try
            {
                var poweredOn = _centralManager.Status == AccessState.Available;
                if (!poweredOn && !_centralManager.TrySetAdapterState(true))
                    await App.Current.MainPage.DisplayAlert("Cannot change bluetooth adapter state", "", "Ok");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:ActiveBluetooth]Error: " + ex.Message);
            }
        }

        private void GetConnectedDevices()
        {
            try
            {
                Peripherals = new ObservableCollection<IPeripheral>();

                _connectedDisposable = _centralManager.GetConnectedPeripherals().Subscribe(scanResult =>
                {
                    scanResult.ToList().ForEach(
                     item =>
                     {
                         if (!string.IsNullOrEmpty(item.Name))
                             Peripherals.Add(item);
                     });

                    _connectedDisposable?.Dispose();
                });

                if (_centralManager.IsScanning)
                    _centralManager.StopScan();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:GetConnectedDevices]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Evento al seleccionar la impresora
        /// </summary>
        /// <param name="peripheral"></param>
        private void OnSelectedPeripheral()
        {
            try
            {
                //ir a pantalla de imprimir
                AppConfig.Instance.Navigations.GoToPageInvokeAsyncVoid(page:new PrintPage(ListTextToPrint), actionAfter: () => SelectedPeripheral = null);

                _scanDisposable?.Dispose();
                IsScanning = _centralManager.IsScanning;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:OnSelectedPeripheral]Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtiene el listado de dispositivos
        /// </summary>
        private async Task GetDeviceList()
        {
            try
            {
                IsScanning = _centralManager.IsScanning;
                await Task.Delay(100);

                //si no está disponible, intentamos activar el bluetooth
                if (_centralManager.Status != Shiny.AccessState.Available)
                {
                    await ActiveBluetooth();

                    //si hemos activado el bluetooth, mostramos mensaje
                    if (_centralManager.Status == Shiny.AccessState.Available)
                        AppConfig.Instance.Dialogs.DisplayToast("Bluetooth activado", showLongTime: false);
                }

                //si está escaneando, paramos de escanear
                if (_centralManager.IsScanning)
                {
                    _centralManager.StopScan();
                    _scanDisposable?.Dispose();

                }
                //si está disponible para escanear
                else
                {
                    Peripherals = new ObservableCollection<IPeripheral>();

                    //conectados
                    _connectedDisposable = _centralManager.GetConnectedPeripherals().Subscribe(scanResult =>
                    {
                        scanResult.ToList().ForEach(
                         item =>
                         {
                             if (!string.IsNullOrEmpty(item.Name))
                                 Peripherals.Add(item);
                         });

                        _connectedDisposable?.Dispose();
                    });

                    //no conectados
                    _scanDisposable = _centralManager.ScanForUniquePeripherals().Subscribe(scanResult =>
                    {
                        if (!string.IsNullOrEmpty(scanResult.Name) && !Peripherals.Contains(scanResult))
                            Peripherals.Add(scanResult);

                    });
                }
                IsScanning = _centralManager.IsScanning;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:GetDeviceList]Error: " + ex.Message);
            }
        }

        private void CancelConnection()
        {
            try
            {
                if(AppConfig.Instance.Globals.Peripheral != null)
                {
                    AppConfig.Instance.Globals.CancelConnectionPeripheral();
                    OnPropertyChanged(nameof(ShowButtonOldPrint));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[SelectedPrintViewModel:CancelConnection]Error: " + ex.Message);
            }
        }
    }
}
