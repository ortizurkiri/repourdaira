﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace AppUrdaira.Converter
{
    public class ColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color returnColor = Color.Black;
            Color defaultColor = Color.Black;
            Color primaryColor = Color.Yellow;
            try
            {
                if(parameter != null)
                {
                    if (parameter is Color)
                    {
                        primaryColor = (Color)parameter;
                        defaultColor = primaryColor == Color.Green ? Color.Red : defaultColor;
                    }
                }

                if (value != null)
                {
                    if(value is int)
                        returnColor = (int)value > 0 ? primaryColor : defaultColor;
                    else if(value is double)
                        returnColor = (double)value > 0 ? primaryColor : defaultColor;
                    else
                        returnColor = defaultColor;
                }
                else
                    returnColor = defaultColor;

                return returnColor;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ColorConverter:Convert]Error: " + ex.Message);
                return returnColor;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
