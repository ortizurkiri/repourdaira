﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace AppUrdaira.Converter
{
    public class FontAttributesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FontAttributes returnFontAttributes = FontAttributes.None;
            FontAttributes defaultFontAttributes = FontAttributes.None;
            FontAttributes primaryFontAttributes = FontAttributes.Bold;
            try
            {
                if (value != null)
                {
                    if (value is int)
                        returnFontAttributes = (int)value > 0 ? primaryFontAttributes : defaultFontAttributes;
                    else if (value is double)
                        returnFontAttributes = (double)value > 0 ? primaryFontAttributes : defaultFontAttributes;
                    else
                        returnFontAttributes = defaultFontAttributes;
                }
                else
                    returnFontAttributes = defaultFontAttributes;

                return returnFontAttributes;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[FontAttributesConverter:Convert]Error: " + ex.Message);
                return returnFontAttributes;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
