﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppUrdaira.Interfaces
{
    public interface IDialogsHelper
    {
        void ShowToast(string message, bool showLongTime = true);
    }
}
