﻿using System.IO;

namespace AppUrdaira.Interfaces
{
    public interface IBitmapHelper
    {
        string GetLogo(string imageName);
        byte[] GetBLogo(string base64);
    }
}
