﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppUrdaira.Interfaces
{
    public interface IFilesHelper
    {
        string Copy(string path, string file);
    }
}
