﻿using AppUrdaira.Helpers;
using AppUrdaira.Models;
using AppUrdaira.Services;
using AppUrdaira.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace AppUrdaira
{
    public class AppConfig
    {
        #region Propiedades públicas
        public DatabaseModel Database { get; set; }
        public Globals Globals { get; set; }
        public Navigations Navigations { get; set; }
        public Dialogs Dialogs { get; set; }
        public DialogsHelper DialogsHelper { get; set; }
        public FilesHelper FileHelper { get; set; }
        public BitmapHelper BitmapHelper { get; set; }
        public SyncService SyncService { get; set; }
        #endregion
        /// <summary>
        /// Constructor
        /// </summary>
        private AppConfig()
        {
            try
            {
                Database = new DatabaseModel();
                Globals = new Globals();
                Navigations = new Navigations();
                Dialogs = new Dialogs();
                DialogsHelper = new DialogsHelper();
                FileHelper = new FilesHelper();
                BitmapHelper = new BitmapHelper();
                SyncService = new SyncService();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[AppConfig:()]"+ex.Message);
            }
        }

        #region Funciones públicas

        #endregion

        #region Instance
        private static AppConfig instance;
        private static object syncLocker = new object();
        public static AppConfig Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLocker)
                    {
                        if (instance == null)
                        {
                            instance = new AppConfig();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion
    }
}
