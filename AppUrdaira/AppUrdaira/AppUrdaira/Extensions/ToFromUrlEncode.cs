﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;

namespace AppUrdaira.Extensions
{
    public static class ToFromUrlEncode
    {
        public static string ToFormUrlEncode<T>(this T item)
        {
            string result = string.Empty;
            try
            {
                foreach (PropertyInfo propertyInfo in item.GetType().GetTypeInfo().DeclaredProperties)
                {
                    if (propertyInfo.GetValue(item, null) != null && propertyInfo.GetType() != typeof(StreamReader))
                    {
                        if (!string.IsNullOrEmpty(result))
                            result += "&";

                        result += $"{propertyInfo.Name}={HttpUtility.UrlEncode(propertyInfo.GetValue(item, null).ToString())}";
                    }
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return result;
        }
    }
}
